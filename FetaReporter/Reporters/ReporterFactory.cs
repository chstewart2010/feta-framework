﻿using System;
using Tr.Feta.Common.Enums;

namespace Tr.Feta.FetaReporter.Reporters
{
    /// <summary>
    /// Provides support for creating <see cref="IFetaReporter"/> objects
    /// </summary>
    public static class ReporterFactory
    {
        /// <summary>
        /// Returns a test reporter based on the requested <see cref="FetaReporterMode"/>
        /// </summary>
        /// <param name="mode">Requested Test Management Tool</param>
        /// <param name="product">Product being tested</param>
        public static IFetaReporter GetReporter(FetaReporterMode mode, string product)
        {
            return mode switch
            {
                FetaReporterMode.Web => new TestRailReporter(product),
                _ => throw new ArgumentOutOfRangeException(nameof(mode), mode, null)
            };
        }
    }
}
