﻿using Newtonsoft.Json.Linq;

namespace Tr.Feta.FetaReporter.Reporters
{
    /// <summary>
    /// Interface for Test Reporting Management
    /// </summary>
    public interface IFetaReporter
    {
        /// <summary>
        /// Creates a new run for a given Test Suite and returns a Run Id if successful
        /// </summary>
        /// <param name="escSuite">Suite object initialized before test execution</param>
        public ulong? CreateNewRun(JObject escSuite);

        /// <summary>
        /// Returns true if a run is found matching that is found in the json
        /// </summary>
        /// <param name="escSuite">Suite object initialized before test execution</param>
        /// <param name="runId">Run id of the previously created test run</param>
        public bool TestRunExists(JObject escSuite, ulong runId);

        /// <summary>
        /// Returns true if the suite data matches the test run
        /// </summary>
        /// <param name="escSuite">Suite object initialized before test execution</param>
        /// <param name="runId">Run id of the previously created test run</param>
        public bool ValidateTestSuiteByRunId(JObject escSuite, ulong runId);

        /// <summary>
        /// Updates a reported suite to add missing cases from supplied json
        /// </summary>
        /// <param name="escSuite">Suite object initialized before test execution</param>
        public void UpdateTestSuite(JObject escSuite);

        /// <summary>
        /// Sends a collection a test results to Test Management System
        /// </summary>
        /// <param name="runId">The run instance created at the beginning of test suite execution</param>
        /// <param name="escTestCases">The test case object in json format</param>
        public void PostTestCaseResults(ulong runId, JToken escTestCases);

        /// <summary>
        /// Closes the test run
        /// </summary>
        /// <param name="runId">The run id created at the beginning of test suite execution</param>
        public void CloseTestRun(ulong runId);

        /// <summary>
        /// Deletes all Test Run after a certain number of months, minimum is 2.
        /// </summary>
        public void DeleteExpiredTestRuns();
    }
}
