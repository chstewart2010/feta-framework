﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Exceptions;
using Tr.Feta.Common.Utilities;
using Newtonsoft.Json.Linq;
using TestRail;
using TestRail.Types;
using TestRail.Enums;

namespace Tr.Feta.FetaReporter.Reporters
{
    /// <summary>
    /// Helper class for TestRail reporting
    /// </summary>
    internal class TestRailReporter : IFetaReporter
    {
        private readonly ulong _productId;
        private readonly TestRailClient _client;

        private static readonly NameValueCollection Properties =
            (NameValueCollection) ConfigurationManager.GetSection("FetaReporter");

        private static bool ArePropertiesValid => Properties != null && !(Properties["ReporterURL"] == null &&
                                                                          Properties["ReporterUserName"] == null &&
                                                                          Properties["ReporterApiKey"] == null);

        /// <summary>
        /// Initializes the TestRail Client
        /// </summary>
        /// <param name="product">Product that is being tested</param>
        /// <exception cref="InvalidConfigurationException">Thrown when ConfigurationProperties have not been set</exception>
        /// <exception cref="ArgumentException"></exception>
        internal TestRailReporter(string product)
        {
            if (!ArePropertiesValid)
            {
                throw new InvalidConfigurationException(
                    "Failed to initialize client due to invalid properties configuration in exe.config");
            }

            _client = new TestRailClient(Properties["ReporterURL"], Properties["ReporterUserName"],
                Properties["ReporterApiKey"]);
            if (Enum.TryParse(product, out Product result))
            {
                if (result == Product.Invalid)
                {
                    Logger.Error($"Failed to initialize client due to invalid product {product}");
                    throw new ArgumentException($"Failed to initialize client due to invalid product {product}",
                        nameof(product));
                }
                _productId = (ulong)result;
            }

            Logger.Info("Client successfully initialized.");
        }

        /// <inheritdoc />
        public ulong? CreateNewRun(JObject escSuite)
        {
            Logger.Info($"Initializing {escSuite["name"]} run");
            var suite = _client.GetSuites(_productId).Payload.FirstOrDefault(testSuite =>
                string.Equals(testSuite.Name, (string) escSuite["name"], StringComparison.CurrentCultureIgnoreCase));
            ulong? testRailSuiteId;
            var newSuite = false;
            if (suite?.Id == null)
            {
                // initialize a new suite in TestRail
                newSuite = true;
                testRailSuiteId = ReportSuite(escSuite)?.Id;
            }
            else
            {
                testRailSuiteId = suite.Id;
            }

            if (testRailSuiteId == null)
            {
                Logger.Error("Failed to retrieve suite data from report server.");
                return null;
            }

            // verify test suite json matches what is in testrail
            if (!newSuite && _client.GetCases(_productId, testRailSuiteId.Value).Payload
                .Any(testCase => !SuiteContainsReportedTestCase(escSuite, testCase.Title)))
            {
                Logger.Error(
                    "Execute the UpdateTestSuite command to update the test suite in TestRail to match the json");
                return null;
            }


            Milestone milestone = GetReportedMilestone((string)escSuite["milestone"]);
            var runName = $"{escSuite["name"]}";

            // get the test cases
            List<ulong> caseIds = new List<ulong>();
            var sections = _client.GetSections(_productId, testRailSuiteId);
            foreach (var cases in sections.Payload.Select(section =>
                _client.GetCases(_productId, testRailSuiteId.Value, section.Id).Payload))
            {
                caseIds.AddRange(from testCase in cases
                    where testCase.SuiteId == testRailSuiteId
                    select testCase.Id.GetValueOrDefault());
            }

            // create the test run
            var result = _client.AddRun(_productId, testRailSuiteId.Value, runName, "", milestone.Id,
                caseIds: caseIds.ToHashSet());
            if (result.ThrownException == null)
            {
                return result.Payload.Id;
            }

            return null;
        }

        /// <inheritdoc />
        public bool TestRunExists(JObject escSuite, ulong runId)
        {
            var run = _client.GetRun(runId);
            return run != null && ValidateTestSuiteByRunId(escSuite, runId);
        }

        /// <inheritdoc />
        public bool ValidateTestSuiteByRunId(JObject escSuite, ulong runId)
        {
            Logger.Info($"Validating {escSuite["name"]} suite against test run {runId}");
            var run = _client.GetRun(runId).Payload;
            if (run.Id == null)
            {
                Logger.Error($"Invalid run id {runId} for {escSuite["name"]}");
                return false;
            }

            // ensure the test suite contains contains each test found in the test run
            foreach (var test in _client.GetTests(run.Id.Value).Payload
                .Where(test => !SuiteContainsReportedTestCase(escSuite, test.Title)))
            {
                Logger.Error($"TestRail suite {escSuite["name"]} is missing test case {test.Title}");
                return false;
            }

            return true;
        }

        /// <inheritdoc />
        public void UpdateTestSuite(JObject escSuite)
        {
            var suite = _client.GetSuites(_productId).Payload.FirstOrDefault(testSuite =>
                string.Equals(testSuite.Name, (string) escSuite["name"], StringComparison.CurrentCultureIgnoreCase));
            if (suite?.Id == null)
            {
                // return null if no suite was found
                Logger.Error($"No suite in TestRail matching {escSuite["name"]}");
                return;
            }
            foreach (var childSuite in escSuite["childSuites"])
            {
                var section = GetReportedSectionForSubSuite(childSuite, suite.Id.Value);
                if (section == null)
                {
                    // add the child suite if the section was not found
                    ReportSubSuite(childSuite, suite.Id.Value);
                }
                else
                {
                    // update the section for that child suite
                    UpdateSectionIfNecessary(childSuite, suite.Id.Value, section);
                }
            }
        }

        /// <inheritdoc />
        public void PostTestCaseResults(ulong runId, JToken escTestCases)
        {
            foreach (var testCase in escTestCases)
            {
                var test = GetReportedTest(runId, (string)testCase["name"]);
                if (test?.Id == null)
                {
                    Logger.Warn($"{testCase["name"]} was not found in current test run.");
                    continue;
                }

                // set the result status
                FinalTestState state = (FinalTestState) (int) testCase["testOutcome"];
                ResultStatus status = state switch
                {
                    FinalTestState.Failed => ResultStatus.Failed,
                    FinalTestState.Passed => ResultStatus.Passed,
                    _ => ResultStatus.Untested
                };

                // add log to the comments if a log is attached
                Logger.Info($"Adding result {status} for {testCase["name"]}.");
                var logFile = (string) testCase["logFile"];
                string log;
                if (!File.Exists(logFile))
                {
                    Logger.Warn($"{testCase["name"]} is missing a output log file");
                    log = null;
                }
                else
                {
                    log = File.ReadAllText(logFile);
                }

                // post the result
                var result = _client.AddResult(test.Id.Value, status, log,
                    elapsed: TimeSpan.FromMilliseconds((int)testCase["elapsedTicks"]));
                if (result.ThrownException != null)
                {
                    Logger.Warn($"{testCase["name"]}'s result could not be updated.");
                }
            }
        }

        /// <inheritdoc />
        public void CloseTestRun(ulong runId)
        {
            // we do not want to accidentally close a test run that isn't complete
            bool allTestRan = _client.GetTests(runId).Payload
                .All(test => test.Status != ResultStatus.Untested && test.Status != null);
            if (allTestRan)
            {
                if (_client.CloseRun(runId).ThrownException != null)
                {
                    Logger.Error("Failed to close run.");
                    return;
                }

                Logger.Info($"Test run {runId} has been closed.");
                return;
            }

            Logger.Warn("Not all tests were ran.");
        }

        /// <inheritdoc />
        public void DeleteExpiredTestRuns()
        {
            if (!int.TryParse(Properties["MaxMonthsInReporter"], out var maxMonths))
            {
                Logger.Warn("MaxMonthsInReporter was not setting. Using default minimum of 2 months.");
                maxMonths = 2;
            }

            if (maxMonths < 2)
            {
                Logger.Warn("Using default minimum of 2 months.");
                maxMonths = 2;
            }

            foreach (var run in from run in _client.GetRuns(_productId).Payload
                let daysInTestRail = DateTime.Now - run.CreatedOn.GetValueOrDefault()
                where daysInTestRail.TotalDays / 30 > maxMonths
                select run)
            {
                Logger.Info($"Deleting {run.Name} from TestRail");
                _client.DeleteRun(run.Id.GetValueOrDefault());
            }
        }

        private static bool SuiteContainsReportedTestCase(JToken escSuite, string testTitle)
        {
            if (escSuite["testCases"]?.Any(testCase => (string)testCase["name"] == testTitle) == true)
            {
                return true;
            }

            return escSuite["childSuites"] != null &&
                   escSuite["childSuites"].Any(childSuite => SuiteContainsReportedTestCase(childSuite, testTitle));
        }

        private Test GetReportedTest(ulong runId, string testName)
        {
            return _client.GetTests(runId).Payload.FirstOrDefault(test =>
                string.Equals(test.Title, testName, StringComparison.CurrentCultureIgnoreCase));
        }

        private Section GetReportedSectionForSubSuite(JToken escSubSuite, ulong testRailSuiteId)
        {
            var section = _client.GetSections(_productId, testRailSuiteId).Payload.FirstOrDefault(foundSection =>
                string.Equals(foundSection.Name, (string) escSubSuite["name"],
                    StringComparison.CurrentCultureIgnoreCase));

            return section;
        }

        private void UpdateSectionIfNecessary(JToken escSubSuite, ulong testRailSuiteId, Section section)
        {
            var cases = _client.GetCases(_productId, testRailSuiteId, section.Id).Payload;
            foreach (var testCase in escSubSuite["testCases"].Where(testCase => !cases.Any(caseInTestRail =>
                string.Equals(caseInTestRail.Title, (string) testCase["name"],
                    StringComparison.CurrentCultureIgnoreCase))))
            {
                ReportTestCase(testCase, section.Id.GetValueOrDefault());
            }

            foreach (var childSuite in escSubSuite["childSuites"])
            {
                var testSection = _client.GetSections(_productId, testRailSuiteId).Payload
                    .FirstOrDefault(existingSection => existingSection.Name == (string) childSuite["name"]);
                if (testSection == null)
                {
                    ReportNestedSubSuites(childSuite, testRailSuiteId, section.Id.GetValueOrDefault());
                }
                else
                {
                    UpdateSectionIfNecessary(childSuite, testRailSuiteId, testSection);
                }
            }
        }

        private Suite ReportSuite(JObject escSuite)
        {
            if (!escSuite["childSuites"].Any())
            {
                Logger.Error("No Sections found in test suite");
                return null;
            }
            var suiteResult = _client.AddSuite(_productId, (string)escSuite["name"]);
            if (suiteResult.ThrownException != null)
            {
                return null;
            }

            Logger.Info($"Adding new suite {escSuite["name"]}");
            Suite suite = _client.GetSuite(suiteResult.Payload.Id.Value).Payload;

            return TryAddNewSubSuitesToReport(escSuite, suiteResult.Payload.Id.Value) ? suite : null;
        }

        private bool TryAddNewSubSuitesToReport(JObject escSuite, ulong testRailSuiteId)
        {
            return escSuite["childSuites"].Select(childSuite => ReportSubSuite(childSuite, testRailSuiteId))
                .All(section => section != null);
        }

        private Section ReportSubSuite(JToken escChildSuite, ulong testRailSuiteId)
        {
            if (!EnsureTestsAreConfigured(escChildSuite))
            {
                return null;
            }

            var sectionResult = _client.AddSection(_productId, testRailSuiteId, (string) escChildSuite["name"]);
            if (sectionResult.ThrownException != null)
            {
                return null;
            }
            
            Logger.Info($"Adding new section {escChildSuite["name"]}");
            if (escChildSuite["childSuites"] != null)
            {
                foreach (var subSection in escChildSuite["childSuites"])
                {
                    ReportNestedSubSuites(subSection, testRailSuiteId, sectionResult.Payload.Id.Value);
                }
            }
            if (escChildSuite["testCases"] != null)
            {
                foreach (var test in escChildSuite["testCases"])
                {
                    ReportTestCase(test, sectionResult.Payload.Id.Value);
                }
            }

            return _client.GetSection(sectionResult.Payload.Id.Value).Payload;
        }

        private void ReportNestedSubSuites(JToken escChildSuite, ulong suiteId, ulong parentSectionId)
        {
            var sectionResult =
                _client.AddSection(_productId, suiteId, (string) escChildSuite["name"], parentSectionId);
            if (sectionResult != null)
            {
                return;
            }

            Logger.Info($"Adding new subsection {escChildSuite["name"]}");
            if (escChildSuite["childSuites"] != null)
            {
                foreach (var subsection in escChildSuite["childSuites"])
                {
                    ReportNestedSubSuites(subsection, suiteId, sectionResult.Payload.Id.Value);
                }
            }

            if (escChildSuite["testCases"] != null)
            {
                foreach (var test in escChildSuite["testCases"])
                {
                    ReportTestCase(test, sectionResult.Payload.Id.Value);
                }
            }
        }

        private void ReportTestCase(JToken escTestCase, ulong sectionId)
        {
            if (string.IsNullOrEmpty((string)escTestCase["expectedResult"]))
            {
                Logger.Error($"{escTestCase["name"]} is missing an expected result. Not adding");
                return;
            }

            var steps = new string[escTestCase["steps"].Count()];
            for (int i = 0; i < steps.Length; i++)
            {
                steps[i] = (string)escTestCase["steps"][i]["description"];
            }

            _client.AddCase(sectionId, (string) escTestCase["name"], customFields: JObject.FromObject(
                new
                {
                    custom_steps = string.Join("\n", steps),
                    custom_expected = (string) escTestCase["expectedResult"],
                    custom_automation_type = 1
                }));

            Logger.Info($"New test case: {escTestCase["name"]}");
        }

        private Milestone GetReportedMilestone(string name)
        {
            return _client.GetMilestones(_productId).Payload.FirstOrDefault(milestone =>
                       string.Equals(milestone.Name, name, StringComparison.CurrentCultureIgnoreCase)) ??
                   throw new TestingFrameworkException($"No milestone found with name {name}");
        }

        private bool EnsureTestsAreConfigured(JToken testSuite)
        {
            foreach (var test in testSuite["testCases"])
            {
                if (test["steps"].Any(step => string.IsNullOrEmpty((string)step["description"])))
                {
                    Logger.Error($"{test["name"]} is missing a step description.");
                    return false;
                }

                if (string.IsNullOrEmpty((string)test["expectedResult"]))
                {
                    Logger.Error($"{test["name"]} is missing an expected result in its header.");
                    return false;
                }
            }

            return true;
        }
    }
}
