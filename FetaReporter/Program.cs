﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;
using CommandLine;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Options;
using Tr.Feta.Common.Utilities;
using Tr.Feta.FetaReporter.Reporters;

namespace Tr.Feta.FetaReporter
{
    class Program
    {
        private static string _product;
        private static ulong? _runId;

        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<ReporterOptions>(args).WithParsed(RunOptions).WithNotParsed(HandleParseError);
        }

        static void HandleParseError(IEnumerable<Error> errs)
        {
            DisplayExampleMenu();
        }

        private static void RunOptions(ReporterOptions reporterOptions)
        {
            var jsonSaveRoot = Environment.GetEnvironmentVariable("JsonSaveRoot");

            if (!Directory.Exists(jsonSaveRoot))
            {
                Console.WriteLine("JsonSaveRoot environment variable not configured. Exiting.");
                return;
            }

            var sessionFolder = Path.Combine(jsonSaveRoot, reporterOptions.SessionId);
            if (!Directory.Exists(sessionFolder))
            {
                Console.WriteLine($"No session folder found with sessionId {reporterOptions.SessionId}");
                return;
            }

            // stop if we fail to initials the test reporter
            if (!TrySetReporter(FetaReporterMode.Web, out var reporter))
            {
                return;
            }

            // execute CloseRun command and return without initializing a json object
            var command = reporterOptions.Command;
            if (command == FetaReporterCommand.CloseRun)
            {
                reporter.CloseTestRun(reporterOptions.RunId.GetValueOrDefault());
                return;
            }

            // set the schema type
            SchemaType schema = command switch
            {
                FetaReporterCommand.PostResults => SchemaType.TestRailResults,
                _ => SchemaType.TestRailSuite
            };


            var jsonFilePath = Path.Combine(sessionFolder, $"{reporterOptions.Command}.json");
            // get the json from the json file. stop if the validation fails
            if (!TrySetJsonPath(schema, jsonFilePath, out var json))
            {
                return;
            }

            switch (command)
            {
                case FetaReporterCommand.CreateNewRun:
                    _runId = reporter.CreateNewRun(json);
                    if (_runId != null)
                    {
                        Logger.Info($"Created new test run with id {_runId}");
                    }
                    else
                    {
                        Logger.Error("Failed to create test run");
                    }
                    break;
                case FetaReporterCommand.ValidateSuite:
                    if (reporter.ValidateTestSuiteByRunId(json, _runId.GetValueOrDefault()))
                    {
                        Logger.Info($"{json["name"]} suite matches test run {_runId}.");
                    }
                    break;
                case FetaReporterCommand.PostResults:
                    reporter.PostTestCaseResults(_runId.GetValueOrDefault(), json["results"]);
                    break;
                case FetaReporterCommand.TestRunExists:
                    if (_runId != null && reporter.TestRunExists(json, _runId.Value))
                    {
                        Logger.Info($"Successfully retrieved run matching {_runId}");
                    }
                    else
                    {
                        Logger.Error("Failed to retrieve run matching the supplied json.");
                    }
                    break;
                default:
                    reporter.UpdateTestSuite(json);
                    break;
            }

            reporter.DeleteExpiredTestRuns();
        }

        private static bool TrySetJsonPath(SchemaType schema, string jsonPath, out JObject json)
        {
            if (!File.Exists(jsonPath))
            {
                Logger.Error($"No file found at {jsonPath}");
                json = null;
                return false;
            }

            if (FileManager.TryValidateJsonFile(jsonPath, FileManager.GetSchemaFile(schema), out json))
            {
                _product = (string)json["product"];
                if (json.ContainsKey("runId"))
                {
                    _runId = (ulong)json["runId"];
                }
                return true;
            }
            return false;
        }

        private static bool TrySetReporter(FetaReporterMode mode, out IFetaReporter reporter)
        {
            try
            {
                reporter = ReporterFactory.GetReporter(mode, _product);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error($"{ex.GetType()}: {ex.Message}");
                reporter = null;
                return false;
            }
        }

        private static void DisplayExampleMenu()
        {
            Console.WriteLine("Example Usage:\n" +
                              "\tTr.Feta.Reporter.exe --jsonPath suite.json --command CreateNewRun\n\n" +
                              "\tTr.Feta.Reporter.exe --jsonPath suite.json --command TestRunExists\n\n" +
                              "\tTr.Feta.Reporter.exe --jsonPath suite.json --command ValidateSuite\n\n" +
                              "\tTr.Feta.Reporter.exe --jsonPath suite.json --command UpdateSuite\n\n" +
                              "\tTr.Feta.Reporter.exe --jsonPath results.json --command PostResults\n\n" +
                              "\tTr.Feta.Reporter.exe --runId 5 --command CloseRun\n\n" +
                              "FetaReporter documentation can be found at Doc\\FetaReporter");
        }
    }
}
