Element Interaction Nodes
===

* The Element Interaction nodes perform interactions **element**s.
* Unless explicitly stated using the **throwError** attribute, an exception will be throw and test execution will stop if the framework fails to execute an Element Interaction node.
* Script execution will stop and return an failure if the **element** attribute was not defined before this node or the UIElement reference by the **element** attribute is stale or not visible.

#### Table of Contents
  * [Clear Node](#Clear-Node)
  * [Click Node](#Click-Node)
  * [RightClick Node](#RightClick-Node)
  * [DoubleClick Node](#DoubleClick-Node)
  * [DragAndDrop Node](#DragAndDrop-Node)
  * [MoveToElement Node](#MoveToElement-Node)

---

## Clear Node

The Clear node attempts to clear text from a UIElement.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "Clear": {
                "element": "_element"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## Click Node

The Click node attempts click a UIElement.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "Click": {
                "element": "_element"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## RightClick Node

The RightClick node attempts right-click a UIElement.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "RightClick": {
                "element": "_element"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## DoubleClick Node

The DoubleClick node attempts double-click a UIElement.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "DoubleClick": {
                "element": "_element"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## DragAndDrop Node

The DragAndDrop Node attempts to drop a UIElement to the location of a second UIElement.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|startingElement|The location of the starting point to drag. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|required|
|endingElement|The location of the ending point to drag. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|required|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "DragAndDrop": {
                "startingElement": "_elem1",
                "endingElement": "_elem2"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## MoveToElement Node

The MoveToElement node attempts move the mouse a UIElement.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "MoveToElement": {
                "element": "_element"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)