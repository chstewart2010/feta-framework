Switch Window Nodes
===

* The Switch Window nodes will switch the Windows Driver to the requested window using the **windowTitle**.

#### Table of Contents
  * [SwitchToWindow Node](#SwitchToWindow-Node)
  * [SwitchToTopLevelClient Node](#SwitchToTopLevelClient-Node)
  * [SwitchToDesktop Node](#SwitchToDesktop-Node)

---

## SwitchToWindow Node

The SwitchToWindow node tells the WindowsDriver to switch to the window that matches the given window title.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|windowTitle|The title of the window to automated.|string|required|
|maximize|Boolean value which determines whether to maximize the new window.|boolean|optional|

```json
{
    "actions": [
        {
            "SwitchToWindow": {
                "windowTitle": "Sample Product"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## SwitchToTopLevelClient Node

The SwitchToTopLevelClient node is an empty node that tells the WindowsDriver to return to the main product window named in the ScriptLauncher.exe.config file. This node *should* never throw an exception.

```json
{
    "actions": [
        {
            "SwitchToTopLevelClient": {}
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## SwitchToDesktop Node

The SwitchToDesktop node is an empty node that tells the WindowsDriver to switch to the desktop. This node *should* never throw never throw an exception.

```json
{
    "actions": [
        {
            "SwitchToDesktop": {}
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)