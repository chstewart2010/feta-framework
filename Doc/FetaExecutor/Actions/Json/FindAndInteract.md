Find And Interact Nodes
===

* The Find And Interact nodes find elements and perform interaction methods against them.
* Unless explicitly stated using the **throwError** attribute, an exception will be throw and test execution will stop if the framework fails to execute Find And Interact node.
* Script execution will stop and return an failure if the UIElement reference by the **parent** attribute is stale or not visible.

#### Table of Contents
  * [FindAndInteract Node](#FindAndInteract-Node)
  * [FindAndEnterText Node](#FindAndEnterText-Node)
  * [GetFromArrayAndInteract Node](#GetFromArrayAndInteract-Node)
  * [GetFromArrayAndEnterText Node](#GetFromArrayAndEnterText-Node)

---

## FindAndInteract Node

The FindAndInteract node searches for an element that matches the **by** and **value** provided, then performs an *[Element Interaction](ElementInteraction.md)* method.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|by|The UIElement's property to check against. This is optional, and the framework will use the Name *property* if not declared. This currently supports any possible property.|string|optional|
|value|The value of the UIElement's property.|string|required|
|action|The *[Element Interaction](ElementInteraction.md)* to perform against the UIElement found. This currently supports *Clear*, *Click*, *DoubleClick*, *MoveToElement*, *RightClick*.|string|required|
|parent|The parent UIElement. This attribute is optional and can be used to add specificity when searching for a UIElement. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "FindAndInteract": {
                "by": "Name",
                "value": "File",
                "action": "Click"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## FindAndEnterText Node

The FindAndEnterText node searches for an element that matches the **by** and **value** provided, then performs an *[Text Entry](TextEntry.md)* method.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|by|The UIElement's property to check against. This is optional, and the framework will use the Name *property* if not declared. This currently supports any possible property.|string|optional|
|value|The value of the UIElement's property.|string|required|
|action|The *[Text Entry](TextEntry.md)* to perform against the UIElement found. This currently supports *EnterText*, and *EnterPassword*.|string|required|
|parent|The parent UIElement. This attribute is optional and can be used to add specificity when searching for a UIElement. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|text|Text value to send to the UIElement.|string|required|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "FindAndInteract": {
                "by": "Name",
                "value": "Search",
                "action": "EnterText",
                "text": "some text"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## FindRelativeElement Node

The FindRelativeElement node searches for a reference element that matches **by** and **value** and searches for a relative element by **direction** and **controlType** provided, then performs an *[Element Interaction](ElementInteraction.md)* or *[Text Entry](TextEntry.md)* against the relative UIElement found.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|by|The Reference UIElement's property to check against. This is optional, and the framework will use the Name *property* if not declared. This currently supports any possible property.|string|optional|
|value|The value of the Reference UIElement's property.|string|required|
|controlType| The ControlType of the relative UiElements. It narrows the search to specific types of UiElements.|string|required|
|directions| The direction of eelative UiElements. It narrows the search to specific types of UIElements and choose the first UIelement.|string|required|
|action|The *[Element Interaction](ElementInteraction.md)* to perform against the UIElement found. This currently supports *Clear*, *Click*, *DoubleClick*, *MoveToElement*, *RightClick*.|string|required|
|text|If action is entertext, than Text value is required to send to the Relative UIElement.|string|optional|

```json
{
    "actions": [
        {
            "FindRelativeElement": {
                "by": "Name",
                "value": "SomeValue",
                "directions": "Above",
                "action": "Click"
            },
            "FindRelativeElement": {
                "by": "Name",
                "value": "SomeValue",
                "directions": "Right",
                "action": "EnterText",
                "text": "some text"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## GetFromArrayAndInteract Node

The GetFromArrayAndInteract node gets an element from a stored array, then performs an *[Element Interaction](ElementInteraction.md)* method.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|action|The *[Element Interaction](ElementInteraction.md)* to perform against the UIElement found. This currently supports *Clear*, *Click*, *DoubleClick*, *MoveToElement*, *RightClick*.|string|required|
|position|The position in the array to pull the element from.|string|required|
|array|The array to pull the UIElement from. This attribute is optional, and the framework will pull the UIElement from *tempArray* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "GetFromArrayAndInteract": {
                "array": "_buttons",
                "position": 0,
                "action": "Click"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## GetFromArrayAndEnterText Node

The GetFromArrayAndEnterText node gets an element from a stored array, then performs an *[Element Interaction](ElementInteraction.md)* method.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|action|The *[Text Entry](TextEntry.md)* to perform against the UIElement found. This currently supports *EnterText*, and *EnterPassword*.|string|required|
|position|The position in the array to pull the element from.|string|required|
|array|The array to pull the UIElement from. This attribute is optional, and the framework will pull the UIElement from *tempArray* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|text|Text value to send to the UIElement.|string|required|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "GetFromArrayAndInteract": {
                "array": "_boxes",
                "position": 0,
                "action": "EnterText",
                "text": "some text"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)