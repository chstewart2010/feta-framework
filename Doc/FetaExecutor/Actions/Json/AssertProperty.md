Assert Property Nodes
===

* The AssertProperty nodes perform assertion checks against an element's property.
* Script execution will stop and return an failure if the **element** attribute was not defined before this node or the UIElement reference by the **element** attribute is stale or not visible.

#### Table of Contents
  * [AssertPropertyEquals Node](#AssertPropertyEqual-Node)
  * [AssertPropertyNotEqual Node](#AssertPropertyNotEqual-Node)
  * [AssertPropertyContains Node](#AssertPropertyContains-Node)
  * [AssertPropertyNull Node](#AssertPropertyNull-Node)
  * [AssertPropertyNotNull Node](#AssertPropertyNotNull-Node)

---

## AssertPropertyEquals Node

The AssertPropertyEquals throws an exception if the provided element's property does not match the expected text value.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|------------|
|element|The UIElement to test against. This attribute is optional, and the framework will use the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric with no spaces. The first character should be an underscore.|string|optional|
|property|The UIElement's property to check against. All UIElement properties are supported. This is optional, and the framework will use the Name *property* if not declared.|string|optional|
|value|The value of the UIElement's property.|string|required|

```json
{
    "actions": [
        {
            "AssertPropertyEquals": {
                "element": "_text",
                "property": "IsOffscreen",
                "value": "False"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## AssertPropertyNotEqual Node

The AssertPropertyNotEqual Node throws an exception if the provided element's property matches the expected text value.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric with no spaces. The first character should be an underscore.|string|optional|
|property|The UIElement's property to check against. All UIElement properties are supported. This is optional, and the framework will use the Name *property* if not declared.|string|optional|
|value|The value of the UIElement's property.|string|required|

```json
{
    "actions": [
        {
            "AssertPropertyNotEqual": {
                "element": "_text",
                "property": "IsOffscreen",
                "value": "False"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## AssertPropertyContains Node

The AssertPropertyContains Node throws an exception if the provided element's property does not contain the expected text value.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric with no spaces. The first character should be an underscore.|string|optional|
|property|The UIElement's property to check against. All UIElement properties are supported. This is optional, and the framework will use the Name *property* if not declared.|string|optional|
|value|The value of the UIElement's property.|string|required|

```json
{
    "actions": [
        {
            "AssertPropertyContains": {
                "element": "_text",
                "property": "AutomationId",
                "value": "alarm"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## AssertPropertyNull Node

The AssertPropertyNull throws an exception if the provided element's property is not null.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric with no spaces. The first character should be an underscore.|string|optional|
|property|The UIElement's property to check against. All UIElement properties are supported. This is optional, and the framework will use the Name *property* if not declared.|string|optional|

```json
{
    "actions": [
        {
            "AssertPropertyNull": {
                "element": "_text",
                "property": "Name",
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## AssertPropertyNotNull Node

The AssertPropertyNotNull throws an exception if the provided element's property is null.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric with no spaces. The first character should be an underscore.|string|optional|
|property|The UIElement's property to check against. All UIElement properties are supported. This is optional, and the framework will use the Name *property* if not declared.|string|optional|

```json
{
    "actions": [
        {
            "AssertPropertyNotNull": {
                "element": "_text",
                "property": "HelpText"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)