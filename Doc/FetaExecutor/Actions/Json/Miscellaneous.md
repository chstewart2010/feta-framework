Logging and Miscellaneous Nodes
===

#### Table of Contents
  * [ClearContainers Node](#ClearContainers-Node)
  * [Section Node](#Section-Node)
  * [Log Node](#Log-Node)
  * [Record Node](#Record-Node)
  * [TakeScreenshot Node](#TakeScreenshot-Node)
  * [Wait Node](#Wait-Node)

## ClearContainers Node

The ClearContainers node clears all stored UI element containers.

```json
{
    "actions": [
        {
            "ClearContainers": {}
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## Section Node

The Section node performs a collection of Test Actions, then clears all of the UI Element containers stored afterwards.

|Attribute Name|Attribute Description|Requirement|
|--------------|---------------------|-----------|
|fileName|Name of the file to save the screenshot.|required|

```json
{
    "actions": [
        {
            "Section": [
                {
                    "FindElements": {
                        "by": "ControlType",
                        "value": "TextBox"
                    },
                    "ForEach": {
                        "actions": [
                            {
                                "Click": {}
                            }
                        ]
                    }
                }
            ]
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## Log Node

The Log node records an INFO level message in the script log. The logger will not record the message if the **logLevel** attribute on the parent Test node is set to *WARN*, *ERROR*, or *EXECEPTION*.

```json
{
    "actions": [
        {
            "Log": "Waiting 10 seconds then taking a screenshot"
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## Record Node

The Record node records a collection of test cations and saves them to an mp4 file.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|-----|----------|
|fileName|Name of the file to save the screenshot.|string|required|

```json
{
    "actions": [
        {
            "Record": {
                "fileName": "Video of current window",
                "actions": [
                    {
                        "Click": {}
                    }
                ]
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## TakeScreenshot Node

The TakeScreenshot node attempts to take a screenshot of the active window.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|fileName|Name of the file to save the screenshot.|string|required|
|element|The UIElement to screenshot. This attribute is optional, and the framework will screenshot the entire window if not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|

```json
{
    "actions": [
        {
            "TakeScreenshot": {
                "fileName": "Screenshot of current window"
            }
        },
        {
            "TakeScreenshot": {
                "fileName": "Screenshot of element",
                "element": "_element"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## Wait Node

The Wait node pauses script execution.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|seconds|Number of seconds to wait before returning to UI automation. The maximize allowed wait time is 20 seconds.|unsigned integer|required|

```json
{
    "actions": [
        {
            "Wait": {
                "seconds": 10
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)