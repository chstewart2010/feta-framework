Find Element Nodes
===

* The Find Element nodes will stored singular elements in variables and collections of elements in arrays **element**s.
* Unless explicitly stated using the **throwError** attribute, an exception will be throw and test execution will stop if the framework fails to execute an Find Element node.
* Script execution will stop and return an failure if the **element** attribute was not defined before this node or the UIElement reference by the **element** or **element** attribute is stale or not visible.

#### Table of Contents
  * [AssertElementExists Node](#AssertElementExists-Node)
  * [AssertElementNotExists Node](#AssertElementNotExists-Node)
  * [FindElement Nodes](#FindElement-Nodes)
  * [FindElements Nodes](#FindElements-Nodes)
  * [GetFromArray Node](#GetFromArray-Node)

---

## AssertElementExists Node

The AssertElementExists node will throw an error if no element is found matching the requirements.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|by|The UIElement's property or XPath to check against. This currently supports any possible property. This is optional, and the framework will use the Name *property* if not declared.|string|optional|
|value|The value of the UIElement's property.|string|required|
|parent|The parent UIElement. This attribute is optional and can be used to add specificity when searching for a UIElement. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|

```json
{
    "actions": [
        {
            "AssertElementExists": {
                "by": "ClassName",
                "value": "SecondChildStackPanel",
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## AssertElementNotExists Node

The AssertElementNotExists node will throw an error if an element is found matching the requirements.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|by|The UIElement's property or XPath to check against. This currently supports any possible property. This is optional, and the framework will use the Name *property* if not declared.|string|optional|
|value|The value of the UIElement's property.|string|required|
|parent|The parent UIElement. This attribute is optional and can be used to add specificity when searching for a UIElement. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|

```json
{
    "actions": [
        {
            "AssertElementNotExists": {
                "by": "ClassName",
                "value": "SecondChildStackPanel",
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## FindElement Nodes

The FindElement node finds the first element in the window matching the property or XPath attribute and stores that value in a variable.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|by|The UIElement's property or XPath to check against. This currently supports any possible property. This is optional, and the framework will use the Name *property* if not declared.|string|optional|
|value|The value of the UIElement's property.|string|required|
|var|The name of the variable to store the element. This attribute is optional, and the framework will store the UIElement as *temp* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|parent|The parent UIElement. This attribute is optional and can be used to add specificity when searching for a UIElement. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "FindElement": {
                "by": "ClassName",
                "value": "MainStackPanel",
                "var": "_main"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## FindElements Nodes

The FindElements node finds the all elements in the window matching the property or it XPath attribute and stores that value in an array.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|by|The UIElement's property or XPath to check against. This currently supports any possible property. This is optional, and the framework will use the Name *property* if not declared.|string|optional|
|value|The value of the UIElement's property.|string|required|
|array|The name of the array to store the elements. This attribute is optional, and the framework will store the UIElements as *temp* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|parent|The parent UIElement. This attribute is optional and can be used to add specificity when searching for a UIElement. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "FindElements": {
                "by": "ClassName",
                "value": "Row",
                "array": "_rows"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## GetFromArray Node

The GetFromArray node will find the nth element in the array and stored it in a variable.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|var|The name of the variable to store the element. This attribute is optional, and the framework will store the UIElement as *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|array|The name of the array to store the elements. This attribute is optional, and the framework will store the UIElements as *array* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|position|The position in the array to pull the element from.|unsigned integer|required|

```json
{
    "actions": [
        {
            "GetElementFromArray": {
                "array": "_rows",
                "position": 0,
                "var": "_row1"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)