Conditional Nodes
===

* The Conditional nodes perform a collection of test actions if they meet the conditions defined.

#### Table of Contents
  * [ForEach Node](#ForEach-Node)
  * [IfArrayCountEquals Node](#IfArrayCountEquals-Node)
  * [IfArrayCountLessThan Node](#IfArrayCountLessThan-Node)
  * [IfPropertyEquals Node](#IfPropertyEquals-Node)
  * [IfPropertyContains Node](#IfPropertyContains-Node)
  * [IfPropertyNull Node](#IfPropertyNull-Node)
  * [IfElementExists Node](#IfElementExists-Node)
  * [IfTextEquals Node](#IfTextEquals-Node)
  * [IfTextContains Node](#IfTextContains-Node)
  * [IfTextNull Node](#IfTextNull-Node)
  * [Else Node](#Else-Node)

## ForEach Node

The ForEach node performs a collection of actions for each UIElement in an array.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|var|The name of the variable to store the element. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|array|The array to pull the UIElement from. This attribute is optional, and the framework will pull the UIElement from *tempArray* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|

```json
{
    "actions": [
        {
            "ForEach": {
                "var": "_item",
                "array": "_items",
                "actions": [
                    {
                        "Click": {
                            "element": "_item"
                        }
                    }
                ]
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## IfArrayCountEquals Node

The IfArrayCountEquals node performs a collection of action if the number of elements in the array is equal to the expected count.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|array|The UIElements to test against. This attribute is optional, and the framework will used the UIElement stored in *tempArray* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|count|The number to test against. This can either be an integer or the name of a previously stored array.|unsigned integer or string|required|

```json
{
    "actions": [
        {
            "Conditional": {
                "ifs": [
                    {
                        "IfArrayCountEquals": {
                            "array": "_items",
                            "count": 5,
                            "actions": [
                                {
                                    "Click": {
                                        "element": "_item"
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## IfArrayCountLessThan Node

The IfArrayCountLessThan node performs a collection of action if the number of elements in the array is less than to the expected count.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|array|The UIElements to test against. This attribute is optional, and the framework will used the UIElement stored in *tempArray* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|count|The number to test against. This can either be an integer or the name of a previously stored array.|unsigned integer or string|required|

```json
{
    "actions": [
        {
            "Conditional": {
                "ifs": [
                    {
                        "IfArrayCountLessThan": {
                            "array": "_items",
                            "count": 5,
                            "actions": [
                                {
                                    "Click": {
                                        "element": "_item"
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## IfPropertyEquals Node

The IfPropertyEquals node performs a collection of action if the **element**'s **by**'s value is equal to **value**.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test the conditional against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
]property]The UIElement's property to check against. All UIElement properties are supported. This is optional, and the framework will use the Name *property* if not declared.|string|optional|
|value|The value of the UIElement's property.|string|required|

```json
{
    "actions": [
        {
            "Conditional": {
                "ifs": [
                    {
                        "IfPropertyEquals": {
                            "element": "_item",
                            "property": "Name",
                            "value": "View",
                            "actions": [
                                {
                                    "Click": {
                                        "element": "_item"
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## IfPropertyContains Node

The IfPropertyContains node performs a collection of action if the **element**'s **by**'s value contains **value**.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test the conditional against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
]property]The UIElement's property to check against. All UIElement properties are supported. This is optional, and the framework will use the Name *property* if not declared.|string|optional|
|value|The value of the UIElement's property.|string|required|

```json
{
    "actions": [
        {
            "Conditional": {
                "ifs": [
                    {
                        "IfPropertyContains": {
                            "element": "_item",
                            "property": "Name",
                            "value": "View",
                            "actions": [
                                {
                                    "Click": {
                                        "element": "_item"
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## IfPropertyNull Node

The IfPropertyNull node performs a collection of action if the **element**'s **by**'s value is null.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test the conditional against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
]property]The UIElement's property to check against. All UIElement properties are supported. This is optional, and the framework will use the Name *property* if not declared.|string|optional|

```json
{
    "actions": [
        {
            "Conditional": {
                "ifs": [
                    {
                        "IfPropertyNull": {
                            "element": "_item",
                            "property": "Name",
                            "actions": [
                                {
                                    "Click": {
                                        "element": "_item"
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## IfElementExists Node

The IfElementExists node performs a collection of action if an UI Element exists with **by** matching **value**. If the UI Element exists, it will be stored in a variable.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|var|The name of the variable to store the element. This attribute is optional, and the framework will store the UIElement as *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|by|The UIElement's property to check against. This is optional, and the framework will use the Name *property* if not declared. This current supports *AutomationId*, *ClassName*, *Name* *ControlType*, and *XPath*.|string|optional|
|value|The value of the UIElement's property.|string|required|

```json
{
    "actions": [
        {
            "Conditional": {
                "ifs": [
                    {
                        "IfElementExists": {
                            "element": "_item",
                            "property": "Name",
                            "value": "View",
                            "actions": [
                                {
                                    "Click": {
                                        "element": "_item"
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## IfTextEquals Node

The IfTextEquals node performs a collection of action if the **element**'s text is equal to **text**.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|text|Text value to send to the UIElement.|string|required|

```json
{
    "actions": [
        {
            "Conditional": {
                "ifs": [
                    {
                        "IfTextEquals": {
                            "element": "_item",
                            "text": "View",
                            "actions": [
                                {
                                    "Click": {
                                        "element": "_item"
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## IfTextContains Node

The IfTextContains node performs a collection of action if the **element**'s text contains **text**.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|text|Text value to send to the UIElement.|string|required|

```json
{
    "actions": [
        {
            "Conditional": {
                "ifs": [
                    {
                        "IfTextContains": {
                            "element": "_item",
                            "text": "View",
                            "actions": [
                                {
                                    "Click": {
                                        "element": "_item"
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## IfTextNull Node

The IfTextNull node performs a collection of action if the **element**'s text is null.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|

```json
{
    "actions": [
        {
            "Conditional": {
                "ifs": [
                    {
                        "IfTextNull": {
                            "element": "_item",
                            "actions": [
                                {
                                    "Click": {
                                        "element": "_item"
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## Else Node

The Else node performs a collection of actions if the condition from the immediately preceding **If** node did not execute.

```json
{
    "actions": [
        {
            "Conditional": {
                "ifs": [
                    {
                        "IfArrayCountLessThan": {
                            "array": "_items",
                            "count": 5,
                            "actions": [
                                {
                                    "Click": {
                                        "element": "_item"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "else": [
                    {
                        "TakeScreenshot": {
                            "fileName": "FIle Name"
                        }
                    }
                ]
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)
