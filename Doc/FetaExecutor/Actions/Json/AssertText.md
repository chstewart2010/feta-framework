Assert Text Nodes
===

* The AssertText nodes perform assertion checks against an **element**'s text.
* Script execution will stop and return an failure if the **element** attribute was not defined before this node or the UIElement reference by the **element** attribute is stale or not visible.

#### Table of Contents
  * [AssertTextEquals Node](#AssertTextEquals-Node)
  * [AssertTextNotEqual Node](#AssertTextNotEqual-Node)
  * [AssertTextContains Node](#AssertTextContains-Node)
  * [AssertTextNull Node](#AssertTextNull-Node)
  * [AssertTextNotNull Node](#AssertTextNotNull-Node)

---

## AssertTextEquals Node

The AssertTextEquals Node throws an exception if the text found in the element does not match the expected text value.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|text|Text value to test against.|string|required|

```json
{
    "actions": [
        {
            "AssertTextEquals": {
                "element": "_text",
                "text": "Alarm triggered."
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## AssertTextNotEqual Node

The AssertTextNotEqual Node throws an exception if the text found in the element matches the expected text value.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|text|Text value to test against.|string|required|

```json
{
    "actions": [
        {
            "AssertTextNotEqual": {
                "element": "_text",
                "text": "Alarm triggered."
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## AssertTextContains Node

The AssertTextContains Node throws an exception if the text found in the element does not contain the expected text value.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|text|Text value to test against.|string|required|

```json
{
    "actions": [
        {
            "AssertTextContains": {
                "element": "_text",
                "text": "Alarm triggered."
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## AssertTextNull Node

The AssertTextNull throws an exception if the provided element's text is not null.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|

```json
{
    "actions": [
        {
            "AssertTextNull": {
                "element": "_text"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## AssertTextNotNull Node

The AssertTextNotNull throws an exception if the provided element's text is null.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|

```json
{
    "actions": [
        {
            "AssertTextNotNull": {
                "element": "_text"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)