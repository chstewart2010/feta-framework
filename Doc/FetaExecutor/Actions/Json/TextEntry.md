Text Entry Nodes
===

* The Text Entry nodes perform text entry interactions **element**s.
* Unless explicitly stated using the **throwError** attribute, an exception will be throw and test execution will stop if the framework fails to execute an Element Interaction node.
* Script execution will stop and return an failure if the **element** attribute was not defined before this node or the UIElement reference by the **element** and **parent** attribute is stale or not visible.

#### Table of Contents
  * [EnterText Node](#EnterText-Node)
  * [EnterPassword Node](#EnterPassword-Node)
  * [TapKey Node](#TapKey-Node)
  * [TapKeys Node](#TapKeys-Node)
  * [Type Node](#Type-Node)

---

## EnterText Node

The EnterText node attempts to send text to an UI element.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|text|Text value to send to the UIElement.|string|required|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "EnterText": {
                "element": "_user",
                "text": "realusername"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## EnterPassword Node

The EnterPassword node attempts to send text to an UI element without logging the text entry.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|-----------|
|element|The UIElement to test against. This attribute is optional, and the framework will used the UIElement stored in *tempVar* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|text|Text value to send to the UIElement.|string|required|
|throwError|Boolean value which determines whether to stop script execution and fail the test if the task fails. This attribute is optional, and its default value is true.|boolean|optional|

```json
{
    "actions": [
        {
            "EnterPassword": {
                "element": "_pass",
                "text": "realpassword"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## TapKey Node

The TapKey node taps a non-alphanumeric key, such as the Enter key.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|-----------|
|key|Key to send to the Windows Driver. Currently supports *ArrowUp*, *ArrowDown*, *ArrowLeft*, *ArrowRight*, *Space*, *Enter*, *Tab*, *Ctrl*, *Alt*, *Del*, *Esc*, *F1*, *F2*, *F3*, *F4*,and *F5*|string|required|
|numberOfTaps|The number of times to tap the key|unsigned integer|optional|

```json
{
    "actions": [
        {
            "TapKey": {
                "key": "Enter",
                "numberOfTaps": 4
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## TapKeys Node

The TapKeys node taps two or three non-alphanumeric key, such as the Enter key.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|-----------|
|keys|Kes to send to the Windows Driver. Currently supports *ArrowUp*, *ArrowDown*, *ArrowLeft*, *ArrowRight*, *Space*, *Enter*, *Tab*, *Ctrl*, *Alt*, *Del*, *Esc*, *F1*, *F2*, *F3*, *F4*,and *F5*|array of [keys](#TapKey-Node)|required|

```json
{
    "actions": [
        {
            "TapKeys": {
                "keys": [
                    "Shift",
                    "Enter"
                ]
            }
        }
    ]
}
```
[Back to top](#table-of-contents)

[Back to XMLSchema](..\..\Schema.md#test-action-nodes)

---

## Type Node

The Type node attempts to send text to the Windows Driver. While similar to the [Enter Text node](#entertext-node), this node is not directed at a UIElement and thus never throws an error.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|-----------|
|text|Text value to send to the UIElement.|string|required|

```json
{
    "actions": [
        {
            "Type": {
                "text": "realusername"
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)