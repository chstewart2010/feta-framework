Assert Array Count Nodes
===

* The AssertArrayCount nodes perform assertion checks against the number of the UIElements in the **array**.
* Script execution will stop and return an failure if the **element** attribute was not defined before this node or the UIElement reference by the **element** attribute is stale or not visible.

#### Table of Contents
  * [AssertArrayCountEquals Node](#AssertArrayCountEqual-Node)
  * [AssertArrayCountGreaterThan Node](#AssertArrayCountEqual-Node)
  * [AssertArrayCountLessThan Node](#AssertArrayCountLessThan-Node)

---

## AssertArrayCountEquals Node

The AssertArrayCountEquals Node throws an exception if the number of elements in the array is not equal to the expected count.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|array|The UIElements to test against. This attribute is optional, and the framework will used the UIElement stored in *tempArray* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|count|The number to test against. This can either be an integer or the name of a previously stored array.|unsigned integer or array name|required|

```json
{
    "actions": [
        {
            "AssertArrayCountEquals": {
                "array": "_boxes",
                "count": 5
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## AssertArrayCountGreaterThan Node

The AssertArrayCountGreaterThan Node throws an exception if the number of elements in the array is less than or equal to the expected count.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|array|The UIElements to test against. This attribute is optional, and the framework will used the UIElement stored in *tempArray* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|count|The number to test against. This can either be an integer or the name of a previously stored array.|unsigned integer or array name|required|

```json
{
    "actions": [
        {
            "AssertArrayCountGreaterThan": {
                "array": "_boxes",
                "count": 5
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)

---

## AssertArrayCountLessThan Node

The AssertArrayCountLessThan Node throws an exception if the number of elements in the array is greater than or equal to the expected count.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|array|The UIElements to test against. This attribute is optional, and the framework will used the UIElement stored in *tempArray* if this attribute is not declared. The name should be 4 and 9 characters long and alphanumeric. The first character should be an underscore.|string|optional|
|count|The number to test against. This can either be an integer or the name of a previously stored array.|unsigned integer or array name|required|

```json
{
    "actions": [
        {
            "AssertArrayCountLessThan": {
                "array": "_boxes",
                "count": 5
            }
        }
    ]
}
```
* [Back to top](#table-of-contents)
* [Back to Schema](..\..\Schema.md#test-action-nodes)