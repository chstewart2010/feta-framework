FetaExecutor
===

The FetaExecutor is a .NET test automation framework built around Appium and WinappDriver that reads test cases from XML documents to perform scripted automation.

---

#### Table of Contents

  * [Installing Node and Appium](#installing-node-and-appium)
  * [Enabling Developer Mode](#enabling-developer-mode)
  * [Using Accessibility Insights](#using-accessibility-insights)
  * [Using Automation Spy](#using-automation-Spy)
  * [Using Feta](#using-feta)
    - [Executing Test Suites](#executing-test-suites)
    - [Interrupting Sessions](#interrupting-sessions)
    - [Listing Active Sessions](#listing-active-sessions)

---

## Installing Node.js and Appium
The Feta Framework relies on Node.js and Appium to perform test execution. The Windows download for Node.js can be [here](https://nodejs.org/en/).

After installing Node.js, Appium can be installed from the commandline using the following command

```
npm install -g appium
```
---

## Enabling Developer Mode
The WinAppDriver Find UIElement functionality relies on the machine's Developer Mode. To enable Developer Mode, open the Developer Settings from the Start Menu and select the Developer Mode radio button.

![Developer Setting Window](DeveloperMode.png)

---
## Using Automation Spy

To inspect UI Elements, an executable program is embedded in the UiAutomation\Tools folder.

For more help, please visit [here](https://ddeltasolutions.000webhostapp.com/Help/AutomationSpyHelp.html)

---

## Using Accessibility Insights
Performing TestStep actions with the FetaExecutor rely on the ability to find UIElements, which are defined in the UI Tree. The Accessibility Insights Tool provides easy access of the UI Tree, using its Inspect feature.


The Windows download for the tool can be found [here](https://accessibilityinsights.io/en/downloads).  
The documentation for Inspect feature can be found [here](https://accessibilityinsights.io/docs/en/windows/getstarted/inspect).

---

## Configuring Feta
General configuration properties can be configured in the Tr.Feta.Executor.exe.config global node.

|ConfigurationProperty Name|ConfigurationProperty Description|Default|
|--------------------------|---------------------------------|-------|
|NumberOfTestStepAttempts|The total number of times to attempt a test step.|Default value is 3.|
|TestOutputDirectory|Root path to record test output. Must be less than 90 characters.|Default value is Tr.Feta.Executor.exe directory.|
|ExecutionTimeOut|Total amount of time, in minutes, to execute a test case before failing.|Default value is 3.|
|MaxDaysInDatabase|Total amount of time, in days, to keep a session record in the local database.|Default value is 3.|
|TestLibraryDirectory|Path to TestLibrary directory.|Default path is adjacent to Tr.Feta.Executor.exe directory.|

```xml
  <global>
    <add key="NumberOfTestStepAttempts" value="3" />
    <add key="TestCaseExecutionTimeOut" value="3" />
    <add key="MaxDaysInDatabase" value="3" />
    <add key="TestOutputDirectory" value="C:\UIAutomationLogs" />
    <add key="TestLibraryDirectory" value="C:\Current\TestLibrary" />
  </global>

  <Sample>
    <add key="ServerName" value="MyWindowsServer" />
    <add key="ServerIPAddress" value="[ip-address]" />
    <add key="ServerUsername" value="myusername" />
    <add key="ServerPassword" value="mypassword" />
    <add key="LocalAppPath" value="C:\my\path\to\application.exe" />
  </Sample>

  <Windows>
    <add key="SecondaryWindow1" value="My Secondary Window Title" />
  </Windows>

  <SmtpServerCredentials>
    <add key="SendEmail" value="true" />
    <add key="SenderUsername" value="tester@escspectrum.com" />
    <add key="SenderAppPassword" value="Password123" />
    <add key="Recipients" value="recipient@xyz.com" />
  </SmtpServerCredentials>
```

## Using Feta
The FetaExecutor is triggered from the command-line, using the *--run* argument. Upon execution, an unique *`SessionId`* will be provided.

### Executing Test Suites
*`Tr.Feta.Executor.exe --command "`Run`" --suites "`suitepath1`" "`suitepath2`" ... [--sessionName "`SessionName`"] [--report] [--runId "`runId`"]`*
```
C:\Tr.Feta\FetaExecutor>Tr.Feta.Executor.exe --command Run --suites "MySuite.xml"

24/09/2020 6:05:57 PM | INFO > SessionId: My Suite_20200924180557
24/09/2020 6:06:32 PM | INFO > Test step 1: Check the Help Menu.
24/09/2020 6:06:32 PM | INFO > Saving First Screenshow screenshot.
```

### Interrupting Sessions

Feta can be stopped or interrupted from the command-line, using the *--stopsession* or *--abortsession* argument. 

**To Stop Execution**

With the *Stop* command, one or multiple sessions can be queued for interruption using *`SessionId`* to stop the current test case and move on to the next test.

*`Tr.Feta.Executor.exe --command "`Stop`" --sessionIds "`SessionId1`" "`SessionId2`" ...`*
```
C:\Tr.Feta\FetaExecutor>Tr.Feta.Executor.exe --command Stop --sessionIds "My Suite_20200924180557"

SessionId 'My Suite_20200924180557' is queued to stop execution after current test.
```
**To Abort Execution**

With the *Abort* command, one or multiple sessions can be aborted.

*`Tr.Feta.Executor.exe --command "`Abort`" -sessionIds "`SessionId1`" "`SessionId2`" ...`*
```
C:\Tr.Feta\FetaExecutor>Tr.Feta.Executor.exe --command Abort "My Suite_20200924180557"
SessionId 'My Suite_20200924180557' is aborted.
```

### Listing Active Sessions

Active Session(s) can be viewed or logged using the --list command.

**To View Active Sessions**

With the *List* command, users can view the active sessions stored in Test Session Database without logging the sessions in the output folder.

*`Tr.Feta.Executor.exe --command "`List`"`*
```
C:\Tr.Feta\FetaExecutor>Tr.Feta.Executor.exe--command List

Active Session(s):
TestID: 'My Suite_20200924115208' StartTime: '9/24/2020 11:52:08 AM' Status: 'Active'
TestID: 'My Suite_20200924115228' StartTime: '9/24/2020 11:52:28 AM' Status: 'Active'
TestID: 'My Suite_20200924115306' StartTime: '9/24/2020 11:53:06 AM' Status: 'Active'
```
