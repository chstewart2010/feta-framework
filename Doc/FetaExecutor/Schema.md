Feta Schema
===

#### Table of Contents
  * [TestSuite Schema](#testsuite-schema)
  * [TestCaseLibrary Schema](#testcaselibrary-schema)
  * [TestStep Node](#teststep-node)
  * [Test Action Nodes](#test-action-nodes)

---

## TestSuite Schema

The Feta Test Suite Node. This is the container for the individual tests referenced for test automation.

Script execution will return an instant failure if the Suite Node is not the first child node of Tr.Feta.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|name|The name of the test section. The name should be at least 3 characters long and alphanumeric with spaces allowed. The first character should be alphanumeric.|string|required|
|id|The name of the test case file|string|required
|logLevel|Sets the LogLevel for the test execution. This attribute determine what the lowest level events to be recorded. Trace < Debug < Info < Warn < Error < Exception|string|optional for TestCase, ignored for TestSuite|
|milestone|The milestone to record the test execution under in TestRail.|string|required for TestRail connection|

```json
{
    "$schema": "../../../FetaExecutor/bin/Debug/Schema/Tr.Feta.Schema.TestSuite.json",
    "name": "Sample Suite",
    "suites": [
        {
            "id": "SampleSuite.json"
        }
    ],
    "cases": [
        {
            "id": "SampleTest1.json",
            "loglevel": "Info"
        },
        {
            "id": "SampleTest3.json",
            "loglevel": "Debug"
        },
        {
            "id": "SampleTest4.json",
            "loglevel": "Debug"
        }
    ]
}
```
[Back to top](#table-of-contents)

---

## TestCaseLibrary Schema
The Feta Test Node. Each Test Node contains a collection of test steps which define the UI test.

Script execution will return an instant failure if the Suite Node does not have at least one Test Node.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|name|The name of the test. The name should be at least 3 characters long and alphanumeric with spaces allowed. The first character should be alphanumeric.|string|required|
|expected|The expected result for the test case.|string|required for TestRail connection|

```json
{
    "$schema": "../../../FetaExecutor/bin/Debug/Schema/Tr.Feta.Schema.TestCaseLibrary.json",
    "name": "Sample Test 1",
    "steps": [
        {
            "description": "Perform test step 1.",
            "actions": []
        }
    ]
}
```
[Back to top](#table-of-contents)

---

## TestStep Node

The Feta TestStep Node. Each Test Node contains a meaningful collection of [actions](#test-action-nodes) that the WindowsDriver and the Feta framework perform during test execution.

While no exceptions are thrown for empty TestStep nodes, it would not make sense for a TestStep node not to have any Action nodes.

Script execution will return an instant failure if any Test Node does not have at least one TestStep Node.

|Attribute Name|Attribute Description|Type|Requirement|
|--------------|---------------------|----|-----------|
|description|The description of the test step. The description should be a written as a complete sentence that accurately describes what the actions of its child nodes.|string|required for TestRail connection|

```json
{
    "$schema": "../../../FetaExecutor/bin/Debug/Schema/Tr.Feta.Schema.TestCaseLibrary.json",
    "name": "Sample Test 1",
    "steps": [
        {
            "description": "Perform test step 1.",
            "actions": [
                {
                    "FindAndInteract": {
                        "value": "File",
                        "action": "Click"
                    }
                }
            ]
        }
    ]
}
```
[Back to top](#table-of-contents)

---

## Test Action Nodes

The following nodes are the collection of all test actions that are supported by the Feta framework. They are grouped in relation to similar actions with shared attributes.

* [Assert Array Count Nodes](Actions\Json\AssertArrayCount.md)
* [Assert Attribute Value Nodes](Actions\Json\AssertProperty.md)
* [Assert Text Nodes](Actions\Json\AssertText.md)
* [Element Interaction Nodes](Actions\Json\ElementInteraction.md)
* [Text Entry Nodes](Actions\Json\TextEntry.md)
* [Find Element Nodes](Actions\Json\FindElement.md)
* [Find And Interact Nodes](Actions\Json\FindAndInteract.md)
* [Switch Window Nodes](Actions\Json\SwitchWindow.md)
* [Conditional Nodes](Actions\Json\Conditional.md)
* [Miscellaneous Nodes](Actions\Json\Miscellaneous.md)