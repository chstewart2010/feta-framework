FetaReporter Schema
===

#### Table of Contents
  * [Suite Schema](#suite-and-testcase-schema)
  * [Results Schema](#results-and-resultitem-schema)
  * [Close Schema](#close-schema)

---

## Suite and TestCase Schema
|Suite Property Name|Property Description|Type|Requirement|
|-------------------|--------------------|----|-----------|
|name|Name of the test suite.|string|required|
|milestone|Name of the Milestone in TestRail.|string|required|
|product|Product that the suite relates to.|string|required|
|childSuites|Collection of all, if any, child suites. Each item follows the Suite schema.|array|required|
|testCases|Collection of all test cases in the. current suite. Each item follows the TestCase Schema.|array|required|
|runId|Run ID in the TestRail. This property is optional and is only need when using the ***--Get*** switch|unsigned integer|optional|

|TestCase Property Name|Property Description|Type|Requirement|
|----------------------|--------------------|----|-----------|
|name|Name of the test case.|string|required|
|steps|Collection of test steps. Each item is an object that a *description* property which describes the test step.|array|required|
|expectResult|The expected result for the test case.|string|required|

```json
{
    "name":"Test Case 1",
    "milestone":"Sprint 0",
    "product":"Sample",
    "childSuites":[
        
    ],
    "testCases":[
       {
          "name":"Step 1",
          "step":[
             {
                "description":"Perform step 1."
             }
          ],
          "expectedResult":"Step executes successfully."
       },
       {
          "name":"Step 2",
          "step":[
             {
                "description":"Perform step 2."
             }
          ],
          "expectedResult":"Step executes successfully."
       },
       {
          "name":"Step 3",
          "step":[
             {
                "description":"Perform step 3."
             }
          ],
          "expectedResult":"Step executes successfully."
       },
       {
          "name":"Step 4",
          "step":[
             {
                "description":"Perform step 4."
             }
          ],
          "expectedResult":"Step executes successfully."
       }
    ]
 }
```

---

## Results and ResultItem Schema
|Result Property Name|Property Description|Type|Requirement|
|--------------------|--------------------|----|-----------|
|product|Product that the suite relates to.|string|required|
|results|Collection of results to pass through to TestRail. Each item follows the ResultItem Schema.|array|required|
|runId|Run ID in the TestRail.|unsigned integer|Rquired|

|ResultItem Property Name|Property Description|Type|Requirement|
|------------------------|--------------------|----|-----------|
|name|Name of the test case.|string|required|
|testOutcome|Outcome from the test case execution. Valid values are *Aborted*, *NotStarted*, *Failed*, and *Passed*.|string|required|
|log|Full log of the test case execution.|string|required|
|elapsedTicks|Total test run time in ticks.|unsigned integer|required|

```json
{
    "product": "Sample",
    "results": [
        {
            "name": "Test Case 1",
            "testOutcome": "Passed",
            "log": "Dozens of lines of output log",
            "elapsedTicks": 1800000000
        },
        {
            "name": "Test Case 2",
            "testOutcome": "Passed",
            "log": "Dozens of lines of output log",
            "elapsedTicks": 1800000000
        },
        {
            "name": "Test Case 3",
            "testOutcome": "Failed",
            "log": "Dozens of lines of output log",
            "elapsedTicks": 1800000000
        }
    ],
    "runId": 16
}
```

---

## Close Schema
|Property Name|Property Description|Type|Requirement|
|-------------|--------------------|----|-----------|
|product|Product that the suite relates to.|string|required|
|runId|Run ID in the TestRail.|unsigned integer|Rquired|

```json
{
    "product": "Sample",
    "runId": 16
}
```