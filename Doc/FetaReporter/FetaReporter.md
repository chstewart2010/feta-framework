FetaReporter
===

FetaReporter is an .Net executable that reads test suite and test case result metadata in the form of json and sends it to the TestRail instance supplied.

---

#### Table of Contents
  * [Configuring FetaReporter](#configuring-FetaReporter)
  * [Creating a new Test Run](#creating-a-new-test-run)
  * [Validating a Suite against a Test Run](#validating-a-suite-against-a-test-run)
  * [Sending Test Results](#sending-test-results)
  * [Closing a Test Run](#closing-a-test-run)

---

## Configuring FetaReporter
Tr.Feta.Reporter.exe config file.

```xml
<FetaReporter>
    <add key="MaxMonthsInTestRail" value="3" />
    <add key="TestRailUserName" value="example@company.com" />
    <add key="TestRailApiKey" value="[sample api key]" />
    <add key="TestRailURL" value="https://example.testrail.io" />
</FetaReporter>
```

---

## Creating a new Test Run
  * Creates a new Test Run in TestRail using a provided test suite and logs the Test Run id.
  * Follows the [Suite Schema](Schema.md#suite-and-testcase-schema)
  * *`Tr.Feta.Reporter.exe --sessionId "`sessionId`" --command CreateNewRun`*
```
C:\Tr.Feta\FetaReporter>Tr.Feta.Reporter.exe --sessionId My Suite_20200924180557 --command CreateNewRun

24/09/2020 6:05:57 PM | INFO > Client successfully initialized.
24/09/2020 6:05:59 PM | INFO > Initializing My Suite run.
24/09/2020 6:06:15 PM | INFO > Created new test run with id 16.
```

---

## Validating a Suite against a Test Run
  * Validates a test suite against the run id supplied.
  * Follows the [Suite Schema](Schema.md#suite-and-testcase-schema)
  * *`Tr.Feta.Reporter.exe --sessionId "`sessionId`" --command ValidateSuite`*
```
C:\Tr.Feta\FetaReporter>Tr.Feta.Reporter.exe -s-essionId My Suite_20200924180557 --command ValidateSuite

24/09/2020 6:05:57 PM | INFO > Client successfully initialized.
24/09/2020 6:05:59 PM | INFO > Validating My Suite suite against test run 16.
24/09/2020 6:06:15 PM | INFO > My Suite suite matches test run 16.
```

---

## Sending Test Results
  * Sends test results after suite executions
  * Follows the [Results Schema](Schema.md#results-and-resultitem-schema)
  * *`Tr.Feta.Reporter.exe --sessionId "`sessionId`" --command PostResults`*
```
C:\Tr.Feta\FetaReporter>Tr.Feta.Reporter.exe --sessionId My Suite_20200924180557 --command PostResults

24/09/2020 6:05:57 PM | INFO > Client successfully initialized.
24/09/2020 6:05:59 PM | INFO > Adding result Passed for Test Case 1 Filter.
24/09/2020 6:06:01 PM | INFO > Adding result Passed for Test Case 2 Filter.
24/09/2020 6:06:03 PM | INFO > Adding result Failed for Test Case 3 Filter.
```

---

## Closing a Test Run
  * Closes a Test Run if all tests have been completed.
  * Follows the [Close Schema](Schema.md#close-schema)
  * *`Tr.Feta.Reporter.exe --runId "`runid`" --command CloseRun`*
```
C:\Tr.Feta\FetaReporter>Tr.Feta.Reporter.exe -runId 16 --command CloseRun

24/09/2020 6:05:57 PM | INFO > Client successfully initialized.
24/09/2020 6:05:59 PM | INFO > Test run 16 has been closed.
```
