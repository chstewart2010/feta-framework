﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommandLine;
using CommandLine.Text;
using Tr.Feta.Common.Enums;

namespace Tr.Feta.Common.Options
{
    /// <summary>
    /// Provides data for the core executable  command line
    /// </summary>
    public class CoreOptions
    {
        /// <summary>
        /// Command that was selected to run
        /// </summary>
        [Option("command", Required = true, HelpText = "The command to run")]
        public FrameworkCommand Command { get; set; }

        /// <summary>
        /// Collection of suite files
        /// </summary>
        [Option("suites", Default = null, HelpText = "List of suite files to run")]
        public IEnumerable<string> SuiteFiles { get; set; }

        /// <summary>
        /// Name of the test session
        /// </summary>
        [Option("sessionName", Default = null, HelpText = "Name for test session")]
        public string SessionName { get; set; }

        /// <summary>
        /// Test Management System to report session results
        /// </summary>
        [Option("report", Default = false, HelpText = "Whether to send result to the TestReporter")]
        public bool Report { get; set; }

        /// <summary>
        /// The runid to track and report test session execution
        /// </summary>
        [Option("runId", Default = null, HelpText = "Run Id for test session")]
        public ulong? RunId { get; set; }

        /// <summary>
        /// Collection of session ids to abort
        /// </summary>
        [Option("sessionIds", Default = null, HelpText = "Space-separated list of active session ids to abort immediately")]
        public IEnumerable<string> SessionIds { get; set; }

        /// <summary>
        /// Whether the options are valid
        /// </summary>
        public bool AreValid => Command switch
        {
            FrameworkCommand.Run => SuiteFiles.Any() &&
                                    !(SessionName ?? string.Empty).Any(character =>
                                        Path.GetInvalidFileNameChars().Contains(character)),
            FrameworkCommand.List => true,
            FrameworkCommand.Abort => SessionIds.Any(),
            FrameworkCommand.Stop => SessionIds.Any(),
            _ => throw new ArgumentOutOfRangeException(nameof(Command), Command, "Invalid Command")
        };
    }
}
