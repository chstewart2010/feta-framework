﻿using CommandLine;
using Tr.Feta.Common.Enums;

namespace Tr.Feta.Common.Options
{
    /// <summary>
    /// Provides data for the FetaReporter executable command line
    /// </summary>
    public class ReporterOptions
    {
        /// <summary>
        /// Path to the json file
        /// </summary>
        [Option("sessionId", HelpText = "The session id to find json files")]
        public string SessionId { get; set; }

        /// <summary>
        /// The runid to track and report test session execution
        /// </summary>
        [Option("runId", Default = null, HelpText = "Run Id for test session")]
        public ulong? RunId { get; set; }

        /// <summary>
        /// Command that was selected to run
        /// </summary>
        [Option("command", Required = true, HelpText = "Command to run")]
        public FetaReporterCommand Command { get; set; }
    }
}
