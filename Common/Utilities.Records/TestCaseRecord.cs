﻿using System;
using Tr.Feta.Common.Enums;

namespace Tr.Feta.Common.Utilities.Records
{
    /// <summary>
    /// Defines the data points of the TestCase record in the local database
    /// </summary>
    public class TestCaseRecord
    {
        /// <summary>
        /// The test case id created at the initialization of the TestCase object
        /// </summary>
        public string TestCaseId { get; set; }

        /// <summary>
        /// The session id created at the initialization of the TestSuite object
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// The test case name found in the script
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The TestCase's result after execution is finished
        /// </summary>
        public FinalTestState Result { get; set; }

        /// <summary>
        /// The start time of the test case execution
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// The end time of the test case execution
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Returns the TestCase's information
        /// </summary>
        public override string ToString() =>
            $"TestCaseId: {TestCaseId} | " +
            $"SessionId: {SessionId} | " +
            $"Name: {Name} | " +
            $"Result: {Result} | " +
            $"TimeInExecution: {EndTime - StartTime} ";
    }
}
