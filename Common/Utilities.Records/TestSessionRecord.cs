﻿using System;
using Tr.Feta.Common.Enums;

namespace Tr.Feta.Common.Utilities.Records
{
    /// <summary>
    /// Defines the data points of the TestSession record in the local database
    /// </summary>
    public class TestSessionRecord
    {
        /// <summary>
        /// The session id created at the initialization of the TestSuite object
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// The start time of the script execution
        /// </summary>
        public DateTime ActiveStartTime { get; set; }

        /// <summary>
        /// The TestSession's current Status
        /// </summary>
        public TestSessionStatus ActiveStatus { get; set; }

        /// <summary>
        /// The creator of the test session
        /// </summary>
        public string Owner { get; set; }

        /// <summary>
        /// Process id for the test session
        /// </summary>
        public int ProcessId { get; set; }

        /// <summary>
        /// Returns the Session's information
        /// </summary>
        public override string ToString() =>
            $"SessionId: {SessionId} | Owner: {Owner} | Status: {ActiveStatus} | StartTime: {ActiveStartTime} | PID: {ProcessId}";
    }
}
