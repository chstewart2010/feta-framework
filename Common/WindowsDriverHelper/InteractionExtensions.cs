﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Tr.Feta.Common.Utilities;
using Keys = OpenQA.Selenium.Keys;

namespace Tr.Feta.Common.WindowsDriverHelper
{
    /// <summary>
    /// Extension class for WindowsDriver for Interacting with Elements
    /// </summary>
    public static class InteractionExtensions
    {
        /// <summary>
        /// Method to send keys to the application
        /// </summary>
        /// <param name="windowsElement">Element to send keys to</param>
        /// <param name="value">Value to send</param>
        /// <param name="throwException">Whether to throw an exception</param>
        public static void EnterText(this AppiumWebElement windowsElement, string value,
            bool throwException = false)
        {
            try
            {
                windowsElement.SendKeys(value);
                Logger.Debug($"The value {value} was sent {windowsElement}.");
            }
            catch (Exception ex)
            {
                WindowsHelper.ExceptionMessenger($"Element located at {windowsElement} could not have keys sent.",
                    throwException, ex);
            }
        }

        /// <summary>
        /// Method to send keys to the application
        /// </summary>
        /// <param name="windowsElement">Element to send keys to</param>
        /// <param name="value">Value to send</param>
        /// <param name="throwException">Whether to throw an exception</param>
        public static void EnterPassword(this AppiumWebElement windowsElement, string value,  bool throwException = false)
        {
            try
            {
                windowsElement.SendKeys(value);
                Logger.Debug($"The password was sent to {windowsElement}.");
            }
            catch (Exception ex)
            {
                WindowsHelper.ExceptionMessenger($"Element located at {windowsElement} could not have keys sent.",
                    throwException, ex);
            }
        }

        /// <summary>
        /// Method to clear an input element of text
        /// </summary>
        /// <param name="windowsElement">Element to clear</param>
        /// <param name="throwException">Whether to throw an exception</param>
        public static void ClearElement(this AppiumWebElement windowsElement, bool throwException = false)
        {
            try
            {
                windowsElement.Clear();
                Logger.Debug($"The element {windowsElement} was cleared.");
            }
            catch (Exception ex)
            {
                WindowsHelper.ExceptionMessenger($"Element located at {windowsElement} could not be cleared.",
                    throwException, ex);
            }
        }

        /// <summary>
        /// Method to click an element
        /// </summary>
        /// <param name="windowsElement">Element to click</param>
        /// <param name="throwException">Whether to throw an exception</param>
        public static void ClickElement(this AppiumWebElement windowsElement, bool throwException = false)
        {
            try
            {
                windowsElement.Click();
                Logger.Debug($"The element {windowsElement} was clicked.");
            }
            catch (Exception ex)
            {
                WindowsHelper.ExceptionMessenger(
                    $"Element located at {windowsElement.GetAttribute("ControlType")} {windowsElement.GetAttribute("Name")} could not be clicked.",
                    throwException, ex);
            }
        }

        /// <summary>
        /// Method to drop and drop an element
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="startingElement">Element to start the drag and drop action</param>
        /// <param name="endingElement">Element to end the drag and drop action</param>
        /// <param name="throwException">Whether to throw an exception</param>
        public static void DragAndDrop(this WindowsDriver<AppiumWebElement> driver, AppiumWebElement startingElement,
            AppiumWebElement endingElement, bool throwException = false)
        {
            driver.DragAndDrop(startingElement.Location, endingElement.Location, throwException);
        }

        /// <summary>
        /// Method to drop and drop an element
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="element">Element to use</param>
        /// <param name="point">Point to use</param>
        /// <param name="isStartingElement" />
        /// <param name="throwException">Whether to throw an exception</param>
        public static void DragAndDrop(this WindowsDriver<AppiumWebElement> driver, AppiumWebElement element,
            Point point, bool isStartingElement, bool throwException = false)
        {
            if (isStartingElement)
            {
                driver.DragAndDrop(element.Location, point, throwException);
            }
            else
            {
                driver.DragAndDrop(point, element.Location, throwException);
            }
        }

        /// <summary>
        /// Method to drop and drop an element
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="startingPoint">Element to start the drag and drop action</param>
        /// <param name="endingPoint">Element to end the drag and drop action</param>
        /// <param name="throwException">Whether to throw an exception</param>
        public static void DragAndDrop(this WindowsDriver<AppiumWebElement> driver, Point startingPoint,
            Point endingPoint, bool throwException = false)
        {
            try
            {
                Actions actions = new Actions(driver);
                Cursor.Position = startingPoint;
                actions.ClickAndHold();
                actions.Perform();
                Cursor.Position = endingPoint;
                actions.Release();
                actions.Perform();
                Logger.Debug($"The point {startingPoint} was dragged to {endingPoint}.");
            }
            catch (Exception ex)
            {
                WindowsHelper.ExceptionMessenger(
                    $"Element located at {startingPoint} could not be dragged to {endingPoint}.", throwException, ex);
            }
        }

        /// <summary>
        /// Method to double click on an element
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="windowsElement">Element to double click</param>
        /// <param name="throwException">Whether to throw an exception</param>
        public static void DoubleClick(this WindowsDriver<AppiumWebElement> driver, AppiumWebElement windowsElement,
            bool throwException = false)
        {
            try
            {
                Actions actions = new Actions(driver);
                actions.MoveToElement(windowsElement);
                actions.DoubleClick(windowsElement);
                actions.Perform();
                Logger.Debug($"The element {windowsElement} was double-clicked.");
            }
            catch (Exception ex)
            {
                WindowsHelper.ExceptionMessenger($"Element located at {windowsElement} could not be double-clicked.",
                    throwException, ex);
            }
        }


        /// <summary>
        /// Method to move the cursor to an element
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="windowsElement">Element to move the cursor</param>
        /// <param name="throwException">Whether to throw an exception</param>
        public static void MoveToElement(this WindowsDriver<AppiumWebElement> driver, AppiumWebElement windowsElement,
            bool throwException = false)
        {
            try
            {
                Actions actions = new Actions(driver);
                actions.MoveToElement(windowsElement);
                actions.Perform();
                Logger.Debug($"Mouse was moved to the element {windowsElement}.");
            }
            catch (Exception ex)
            {
                if (!windowsElement.Displayed)
                {
                    throwException = true;
                }
                WindowsHelper.ExceptionMessenger($"Element located at {windowsElement} not found on the screen.",
                    throwException, ex);
            }
        }

        /// <summary>
        /// Method to right click on an element
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="windowsElement">Element to right click</param>
        /// <param name="throwException">Whether to throw an exception</param>
        public static void RightClick(this WindowsDriver<AppiumWebElement> driver, AppiumWebElement windowsElement,
            bool throwException = false)
        {
            try
            {
                Actions actions = new Actions(driver);
                actions.ContextClick(windowsElement);
                actions.Perform();
                Logger.Debug($"The element {windowsElement} was right-clicked.");
            }
            catch (Exception ex)
            {
                WindowsHelper.ExceptionMessenger($"Element located at {windowsElement} could not be double-clicked.",
                    throwException, ex);
            }
        }

        /// <summary>
        /// Method to take a key
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="key">Key to tap</param>
        public static void TapKey(this WindowsDriver<AppiumWebElement> driver, string key)
        {
            Actions actions = new Actions(driver);
            string keyToSend = GetKey(key);
            actions.SendKeys(keyToSend);
            actions.Perform();
            Logger.Debug($"{key} was tapped.");
        }

        /// <summary>
        /// Method to enter text regardless of ui element
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="keys">Keys to send</param>
        public static void TapKeys(this WindowsDriver<AppiumWebElement> driver, params string[] keys)
        {
            Actions actions = new Actions(driver);
            string keysToSend = keys.Aggregate(string.Empty, (current, key) => current + GetKey(key));
            actions.SendKeys(keysToSend);
            actions.Perform();
            Logger.Debug($"{string.Join("+", keys)} was tapped.");
        }

        /// <summary>
        /// Method to enter text regardless of ui element
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="value">Keys to send</param>
        public static void Type(this WindowsDriver<AppiumWebElement> driver, string value)
        {
            Actions actions = new Actions(driver);
            actions.SendKeys(value);
            actions.Perform();
            Logger.Debug($"{value} was typed.");
        }

        private static string GetKey(string key)
        {
            return key.ToLower() switch
            {
                "arrowup" => Keys.ArrowUp,
                "arrowdown" => Keys.ArrowDown,
                "arrowleft" => Keys.ArrowLeft,
                "arrowright" => Keys.ArrowRight,
                "enter" => Keys.Enter,
                "space" => Keys.Space,
                "shift" => Keys.Shift,
                "tab" => Keys.Tab,
                "ctrl" => Keys.Control,
                "alt" => Keys.LeftAlt,
                "del" => Keys.Delete,
                "esc" => Keys.Escape,
                "f1" => Keys.F1,
                "f2" => Keys.F2,
                "f3" => Keys.F3,
                "f4" => Keys.F4,
                "f5" => Keys.F5,
                _ => string.Empty
            };
        }
    }
}
