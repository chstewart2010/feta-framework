﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Tr.Feta.Common.Utilities;

namespace Tr.Feta.Common.WindowsDriverHelper
{
    /// <summary>
    /// Helper class for WindowsDriver management
    /// </summary>
    public static class WindowsHelper
    {
        private static string _currentWindow;

        /// <summary>
        /// IP Address to server host
        /// </summary>
        public static string ServerIpAddress { private get; set; }

        /// <summary>
        /// Window Title for the Top Level Client
        /// </summary>
        public static string TopLevelClient { private get; set; }

        /// <summary>
        /// The Current WindowsDriver for the test session
        /// </summary>
        public static WindowsDriver<AppiumWebElement> CurrentDriver { get; private set; }

        /// <summary>
        /// Method to dispose (close/quit) of all existing Appium Drivers
        /// </summary>
        public static void CloseWindowsDriver()
        {
            try
            {
                if (!(_currentWindow == TopLevelClient || string.IsNullOrEmpty(_currentWindow)))
                {
                    CurrentDriver.CloseApp();
                }

                CurrentDriver = null;
                TaskKill("winappdriver.exe");
                TaskKill("node.exe");
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }

        /// <summary>
        /// Switch the Driver a new window
        /// </summary>
        /// <param name="windowTitle">The title of the window to switch</param>
        /// <param name="closeCurrentWindow">Whether to close the current window</param>
        /// <param name="maximize">Whether to maximize the new window</param>
        /// <param name="reset">Whether to forcefully re-create the driver</param>
        public static WindowsDriver<AppiumWebElement> GetWindowsDriver(
            string windowTitle,
            bool closeCurrentWindow,
            bool maximize = true,
            bool reset = false)
        {
            if (CurrentDriver != null)
            {
                if (_currentWindow != windowTitle ||
                    reset)
                {
                    CreateWindowsDriver
                    (
                        windowTitle,
                        closeCurrentWindow,
                        maximize
                    );
                }
            }
            else
            {
                CreateWindowsDriver
                (
                    windowTitle,
                    closeCurrentWindow,
                    maximize
                );
            }

            return CurrentDriver;
        }

        /// <summary>
        /// Method to take a screenshot of the application
        /// </summary>
        /// <param name="fileDestination">The directory to save the screenshot</param>
        /// <param name="fileName">Name of the file</param>
        /// <param name="element">Element to screenshot</param>
        /// <param name="skip">Skips the screenshot attempt</param>
        public static void TakeScreenshot(
            string fileDestination,
            string fileName,
            AppiumWebElement element,
            bool skip = false)
        {
            try
            {
                Logger.Info($"Saving {fileName} screenshot.");
                Screenshot screenshot = CurrentDriver?.GetScreenshot();
                string filepath = FileManager.GenerateFilePath
                (
                    fileDestination,
                    fileName,
                    "png",
                    true
                );

                if (element == null)
                {
                    screenshot?.SaveAsFile(filepath);
                }
                else
                {
                    Rectangle elementCrop = new Rectangle(element.Location, element.Size);
                    using Bitmap fullScreen = Image.FromStream
                    (
                        new MemoryStream(screenshot?.AsByteArray ?? new byte[1])
                    ) as Bitmap;
                    using Bitmap imageOfElement = fullScreen?.Clone(elementCrop, fullScreen.PixelFormat);

                    if (imageOfElement != null)
                    {
                        imageOfElement.Save(filepath);
                    }
                    else
                    {
                        Logger.Error("Failed to take screenshot.");
                    }
                }
            }
            catch
            {
                if (skip)
                {
                    Logger.Error($"Failed to save screenshot.");
                }
                else
                {
                    Logger.Warn($"Could not save screenshot. Trying again.");
                    CreateWindowsDriver
                    (
                        _currentWindow,
                        false,
                        false
                    );
                    TakeScreenshot
                    (
                        fileDestination,
                        fileName,
                        null,
                        true
                    );
                }
            }
        }

        internal static void ExceptionMessenger(
            string errorMessage,
            bool throwException,
            Exception ex)
        {
            if (throwException ||
                ex is StaleElementReferenceException ||
                ex is ElementNotVisibleException)
            {
                ex = RedefineException
                (
                    ex,
                    errorMessage
                );

                throw ex;
            }

            Logger.Debug(errorMessage);
        }

        private static T RedefineException<T>(
            T ex,
            string message) where T : Exception
        {
            return ex.GetType()
                .GetConstructor(new[] {typeof(string)})?
                .Invoke(new object[] 
                {
                    message
                }) as T;
        }

        private static void CreateWindowsDriver(
            string windowTitle,
            bool closeCurrentWindow,
            bool maximize,
            bool retry = true)
        {
            try
            {
                // close current window
                if (_currentWindow != TopLevelClient &&
                    closeCurrentWindow)
                {
                    CloseWindowsDriver();
                }
                
                // create a rootdriver
                AppiumOptions options = new AppiumOptions();
                options.AddAdditionalCapability
                (
                    "app",
                    "Root"
                );
                options.AddAdditionalCapability
                (
                    "deviceName",
                    "WindowsPC"
                );
                var rootDriver = new WindowsDriver<AppiumWebElement>(options);
                var newWindow = string.IsNullOrEmpty(windowTitle)
                    ? "Desktop"
                    : windowTitle;
                Logger.Debug(newWindow);

                // stop here to navigate the desktop
                if (string.IsNullOrEmpty(windowTitle))
                {
                    Logger.Debug("Creating new driver for the desktop.");
                    CurrentDriver = rootDriver;
                    _currentWindow = newWindow;
                    Logger.Debug("Setting implicit wait time to 30 seconds.");
                    CurrentDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
                    return;
                }

                // select new window
                Logger.Debug($"Creating new driver for {windowTitle}");
                var windowsElement = rootDriver.FindElementByName(windowTitle);
                var windowHandle = windowsElement.GetAttribute("NativeWindowHandle");
                windowHandle = int.Parse(windowHandle).ToString("x");

                // switch to new window
                options = new AppiumOptions();
                options.AddAdditionalCapability
                (
                    "deviceName",
                    "WindowsPC"
                );
                options.AddAdditionalCapability
                (
                    "appTopLevelWindow",
                    windowHandle
                );
                CurrentDriver = new WindowsDriver<AppiumWebElement>(options);
                _currentWindow = newWindow;
                Logger.Debug("Setting implicit wait time to 30 seconds.");
                CurrentDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);

                if (maximize)
                {
                    CurrentDriver.Manage().Window.Maximize();
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Original error: WinAppDriver crashed during startup.") ||
                    retry)
                {
                    ExceptionMessenger
                    (
                        "WinAppDriver crashed during startup.",
                        !retry,
                        ex
                    );
                    CreateWindowsDriver
                    (
                        windowTitle,
                        closeCurrentWindow,
                        maximize,
                        false
                    );
                }
                else
                {
                    ExceptionMessenger
                    (
                        $"{windowTitle} was not found.",
                        true,
                        ex
                    );
                }
            }
        }

        private static void TaskKill(string processName) =>
            Process.Start(new ProcessStartInfo
            {
                FileName = "taskkill.exe",
                Arguments = $"/f /im {processName}",
                CreateNoWindow = true,
                UseShellExecute = false
            })?.WaitForExit();
    }
}
