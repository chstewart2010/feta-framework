﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using System;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Utilities;

namespace Tr.Feta.Common.WindowsDriverHelper
{
    /// <summary>
    /// Extension class for WindowsDriver for Assertion
    /// </summary>
    public static class AssertionExtensions
    {
        /// <summary>
        /// Method to check for the nonexistence of a UIElement
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="by">UIElement's property or XPath to check against</param>
        /// <param name="value">value of the UIElement's property</param>
        /// <param name="parentElement">Parent element</param>
        public static void AssertElementExists(this WindowsDriver<AppiumWebElement> driver, FindBy by, string value,
            AppiumWebElement parentElement = null)
        {
            try
            {
                if (parentElement == null)
                {
                    _ = by switch
                    {
                        FindBy.ClassName => driver.FindElementByClassName(value),
                        FindBy.AutomationId => driver.FindElementByAccessibilityId(value),
                        FindBy.ControlType => driver.FindElementByTagName(value),
                        FindBy.XPath => driver.FindElementByXPath(value),
                        FindBy.Name => driver.FindElementByName(value),
                        _ => driver.FindElementByXPath($"//*[@{by}='{value}']")
                    };
                }
                else
                {
                    _ = by switch
                    {
                        FindBy.ClassName => parentElement.FindElementByClassName(value),
                        FindBy.AutomationId => parentElement.FindElementByAccessibilityId(value),
                        FindBy.ControlType => parentElement.FindElementByTagName(value),
                        FindBy.XPath => parentElement.FindElementByXPath(value),
                        FindBy.Name => parentElement.FindElementByName(value),
                        _ => driver.FindElementByXPath($"//*[@{by}='{value}']")
                    };
                }

                Logger.Debug($"Element found using {by}={value}");
            }
            catch (Exception ex)
            {
                if (ex is StaleElementReferenceException)
                {
                    WindowsHelper.ExceptionMessenger("Parent element is stale", true, ex);
                }

                if (ex is NoSuchElementException)
                {
                    throw new AssertionException($"No element found using {by}={value}.");
                }
            }
        }

        /// <summary>
        /// Method to check for the nonexistence of a UIElement
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="by">UIElement's property or XPath to check against</param>
        /// <param name="value">value of the UIElement's property</param>
        /// <param name="parentElement">Parent element</param>
        public static void AssertElementNotExists(this WindowsDriver<AppiumWebElement> driver, FindBy by, string value,
            AppiumWebElement parentElement = null)
        {
            try
            {
                if (parentElement == null)
                {
                    _ = by switch
                    {
                        FindBy.ClassName => driver.FindElementByClassName(value),
                        FindBy.AutomationId => driver.FindElementByAccessibilityId(value),
                        FindBy.ControlType => driver.FindElementByTagName(value),
                        FindBy.XPath => driver.FindElementByXPath(value),
                        FindBy.Name => driver.FindElementByName(value),
                        _ => driver.FindElementByXPath($"//*[@{by}='{value}']")
                    };
                }
                else
                {
                    _ = by switch
                    {
                        FindBy.ClassName => parentElement.FindElementByClassName(value),
                        FindBy.AutomationId => parentElement.FindElementByAccessibilityId(value),
                        FindBy.ControlType => parentElement.FindElementByTagName(value),
                        FindBy.XPath => parentElement.FindElementByXPath(value),
                        FindBy.Name => parentElement.FindElementByName(value),
                        _ => driver.FindElementByXPath($"//*[@{by}='{value}']")
                    };
                }

                throw new AssertionException($"Element found using {by}={value}.");
            }
            catch (Exception ex)
            {
                if (ex is StaleElementReferenceException || ex.Message.ToLower().Contains("no active session"))
                {
                    WindowsHelper.ExceptionMessenger("Parent element is stale", true, ex);
                }

                if (ex is NoSuchElementException)
                {
                    Logger.Debug($"No element using {by}={value}.");
                }
            }
        }
    }
}
