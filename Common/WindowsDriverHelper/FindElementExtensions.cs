﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using System;
using System.Collections.ObjectModel;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Utilities;

namespace Tr.Feta.Common.WindowsDriverHelper
{
    /// <summary>
    /// Extension class for WindowsDriver for Finding Elements
    /// </summary>
    public static class FindElementExtensions
    {
        /// <summary>
        /// Method to find the first UIElement matching the given criteria
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="by">UIElement's property or XPath to check against</param>
        /// <param name="value">value of the UIElement's property</param>
        /// <param name="parentElement">Parent element</param>
        /// <param name="throwException">Whether to throw an exception</param>
        /// <exception cref="NoSuchElementException"/>
        public static AppiumWebElement FindUiElement(this WindowsDriver<AppiumWebElement> driver, FindBy by,
            string value, AppiumWebElement parentElement = null, bool throwException = true)
        {
            try
            {
                AppiumWebElement element;
                if (parentElement == null)
                {
                    element = by switch
                    {
                        FindBy.AutomationId => driver.FindElementByAccessibilityId(value),
                        FindBy.ClassName => driver.FindElementByClassName(value),
                        FindBy.Name => driver.FindElementByName(value),
                        FindBy.ControlType => driver.FindElementByTagName(value),
                        FindBy.XPath => driver.FindElementByXPath(value),
                        _ => driver.FindElementByXPath($"//*[@{by}='{value}']")
                    };
                }
                else
                {
                    element = by switch
                    {
                        FindBy.AutomationId => parentElement.FindElementByAccessibilityId(value),
                        FindBy.ClassName => parentElement.FindElementByClassName(value),
                        FindBy.Name => parentElement.FindElementByName(value),
                        FindBy.ControlType => parentElement.FindElementByTagName(value),
                        FindBy.XPath => parentElement.FindElementByXPath(value),
                        _ => parentElement.FindElementByXPath($"//*[@{by}='{value}']")
                    };
                }

                Logger.Debug($"Found element {element} using {by}={value}.");
                return element;
            }
            catch (Exception ex)
            {
                WindowsHelper.ExceptionMessenger($"No element found using {by}={value}.", throwException, ex);
                return null;
            }
        }

        /// <summary>
        /// Method to find all UIElements matching the given criteria
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="by">UIElement's property or XPath to check against</param>
        /// <param name="value">value of the UIElement's property</param>
        /// <param name="parentElement">Parent element</param>
        /// <param name="throwException">Whether to throw an exception</param>
        /// <exception cref="NoSuchElementException"/>
        public static ReadOnlyCollection<AppiumWebElement> FindUiElements(this WindowsDriver<AppiumWebElement> driver,
            FindBy by, string value, AppiumWebElement parentElement = null, bool throwException = true)
        {
            try
            {
                ReadOnlyCollection<AppiumWebElement> collection;
                if (parentElement == null)
                {
                    collection = by switch
                    {
                        FindBy.AutomationId => driver.FindElementsByAccessibilityId(value) as
                            ReadOnlyCollection<AppiumWebElement>,
                        FindBy.ClassName => driver.FindElementsByClassName(value),
                        FindBy.Name => driver.FindElementsByName(value),
                        FindBy.ControlType => driver.FindElementsByTagName(value),
                        FindBy.XPath => driver.FindElementsByXPath(value),
                        _ => driver.FindElementsByXPath($"//*[@{by}='{value}']")
                    };
                }
                else
                {
                    collection = by switch
                    {
                        FindBy.AutomationId => parentElement.FindElementsByAccessibilityId(value) as
                            ReadOnlyCollection<AppiumWebElement>,
                        FindBy.ClassName => parentElement.FindElementsByClassName(value),
                        FindBy.Name => parentElement.FindElementsByName(value),
                        FindBy.ControlType => parentElement.FindElementsByTagName(value),
                        FindBy.XPath => parentElement.FindElementsByXPath(value),
                        _ => parentElement.FindElementsByXPath($"//*[@{by}='{value}']")
                    };
                }

                Logger.Debug($"Found {collection?.Count ?? 0} elements using {by}={value}");
                return collection;
            }
            catch (Exception ex)
            {
                WindowsHelper.ExceptionMessenger($"No elements found {by}={value}.", throwException, ex);
                return null;
            }
        }
    }
}
