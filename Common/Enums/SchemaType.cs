﻿using Tr.Feta.Common.Attributes;

namespace Tr.Feta.Common.Enums
{
    /// <summary>
    /// Container for schemas
    /// </summary>
    public enum SchemaType
    {
        /// <summary>
        /// Path to Suite Json schema
        /// </summary>
        [SchemaPath("Tr.Feta.Schema.TestSuite.json")]
        JsonSuite = 1,

        /// <summary>
        /// Path to test case json schema
        /// </summary>
        [SchemaPath("Tr.Feta.Schema.TestCaseLibrary.json")]
        JsonTestCase,

        /// <summary>
        /// Path to Results Json schema
        /// </summary>
        [SchemaPath("Tr.Feta.Schema.SendResult.json")]
        TestRailResults,

        /// <summary>
        /// Path to TestRail Suite schema
        /// </summary>
        [SchemaPath("Tr.Feta.Schema.CreateSuite.json")]
        TestRailSuite
    }
}
