﻿namespace Tr.Feta.Common.Enums
{
    /// <summary>
    /// Container for test execution status
    /// </summary>
    public enum TestExecutionStatus
    {
        /// <summary>
        /// Test is initializing from script file
        /// </summary>
        PreRun,

        /// <summary>
        /// Test is running
        /// </summary>
        Running,

        /// <summary>
        /// Test is being torn down and <see cref="FinalTestState"/> is being assigned
        /// </summary>
        PostRun,

        /// <summary>
        /// Test run has concluded
        /// </summary>
        Finished
    }
}
