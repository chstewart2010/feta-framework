﻿namespace Tr.Feta.Common.Enums
{
    /// <summary>
    /// Container for Complex type nodes
    /// </summary>
    public enum ComplexNode
    {
        /// <summary>
        /// Executes ForEach loop
        /// </summary>
        ForEach = 1,

        /// <summary>
        /// Records child actions
        /// </summary>
        Record,

        /// <summary>
        /// Perform child actions if conditional is met
        /// </summary>
        IfArrayCountEquals,

        /// <summary>
        /// Perform child actions if conditional is met
        /// </summary>
        IfArrayCountLessThan,

        /// <summary>
        /// Perform child actions if conditional is met
        /// </summary>
        IfPropertyEquals,

        /// <summary>
        /// Perform child actions if conditional is met
        /// </summary>
        IfPropertyContains,

        /// <summary>
        /// Perform child actions if conditional is met
        /// </summary>
        IfPropertyNull,

        /// <summary>
        /// Perform child actions if conditional is met
        /// </summary>
        IfElementExists,

        /// <summary>
        /// Perform child actions if conditional is met
        /// </summary>
        IfTextEquals,

        /// <summary>
        /// Perform child actions if conditional is met
        /// </summary>
        IfTextContains,

        /// <summary>
        /// Perform child actions if conditional is met
        /// </summary>
        IfTextNull,

        /// <summary>
        /// Perform child actions if prior conditional was false
        /// </summary>
        Else,

        /// <summary>
        /// Perform one of several collections of child actions, depending on which case is met
        /// </summary>
        SwitchOnProperty,

        /// <summary>
        /// Perform one of several collections of child actions, depending on which case is met
        /// </summary>
        SwitchOnArrayCount,

        /// <summary>
        /// Perform one of several collections of child actions, depending on which case is met
        /// </summary>
        SwitchOnText,

        /// <summary>
        /// Perform child actions if value is met
        /// </summary>
        Case,

        /// <summary>
        /// Perform child actions if value is null
        /// </summary>
        CaseNull,

        /// <summary>
        /// Perform child actions if value is not met
        /// </summary>
        Default,

        /// <summary>
        /// Fail test if value is not met
        /// </summary>
        DefaultFail,

        /// <summary>
        /// Clear element containers
        /// </summary>
        ClearContainers,

        /// <summary>
        /// Perform child actions, then clear element containers
        /// </summary>
        Section,

        /// <summary>
        /// Switch WindowsDriver to Desktop
        /// </summary>
        SwitchToDesktop,

        /// <summary>
        /// Switch WindowsDriver to requested window
        /// </summary>
        SwitchToWindow,

        /// <summary>
        /// Switch WindowsDriver to top level client
        /// </summary>
        SwitchToTopLevelClient,

        /// <summary>
        /// Launches the top level client
        /// </summary>
        LaunchTopLevel
    }
}