﻿namespace Tr.Feta.Common.Enums
{
    /// <summary>
    /// Container for Valid types of Test Automation
    /// </summary>
    public enum TestAutomationType
    {
        /// <summary>
        /// Represents Ui test automation
        /// </summary>
        UiAutomation = 1,

        /// <summary>
        /// Represents Script automation
        /// </summary>
        ScriptAutomation = 2
    }
}
