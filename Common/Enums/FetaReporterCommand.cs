﻿namespace Tr.Feta.Common.Enums
{
    /// <summary>
    /// Container for value TestReporter Commandds
    /// </summary>
    public enum FetaReporterCommand
    {
        /// <summary>
        /// Represents the --CreateNewRun command
        /// </summary>
        CreateNewRun = 1,

        /// <summary>
        /// Represents the --TestRunExists command
        /// </summary>
        TestRunExists,

        /// <summary>
        /// Represents the --ValidateSuite command
        /// </summary>
        ValidateSuite,

        /// <summary>
        /// Represents the --PostResult command
        /// </summary>
        PostResults,

        /// <summary>
        /// Represents the --UpdateSuite command
        /// </summary>
        UpdateSuite,

        /// <summary>
        /// Represents the --CloseRun
        /// </summary>
        CloseRun
    }
}
