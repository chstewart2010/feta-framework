﻿namespace Tr.Feta.Common.Enums
{
    /// <summary>
    /// Container for common methods for finding elements
    /// </summary>
    public enum FindBy
    {
        #region Common Bys

        /// <summary>
        /// Indicates to find element using the Name Property
        /// </summary>
        Name = 1,

        /// <summary>
        /// Indicates to find element using the ClassName Property
        /// </summary>
        ClassName,

        /// <summary>
        /// Indicates to find element using the ControlType Property
        /// </summary>
        ControlType,

        /// <summary>
        /// Indicates to find element using the AutomationId Property
        /// </summary>
        AutomationId,

        /// <summary>
        /// Indicates to find element using the element's XPath
        /// </summary>
        XPath,

        #endregion

        #region Uncommon Bys
        
        /// <summary>
        /// Indicates to find element using the AcceleratorKey Property
        /// </summary>
        AcceleratorKey,

        /// <summary>
        /// Indicates to find element using the AccessKey Property
        /// </summary>
        AccessKey,

        /// <summary>
        /// Indicates to find element using the AriaProperties Property
        /// </summary>
        AriaProperties,

        /// <summary>
        /// Indicates to find element using the AriaRole Property
        /// </summary>
        AriaRole,

        /// <summary>
        /// Indicates to find element using the BoundingRectangle Property
        /// </summary>
        BoundingRectangle,

        /// <summary>
        /// Indicates to find element using the Culture Property
        /// </summary>
        Culture,

        /// <summary>
        /// Indicates to find element using the FillType Property
        /// </summary>
        FillType,

        /// <summary>
        /// Indicates to find element using the FrameworkId Property
        /// </summary>
        FrameworkId,

        /// <summary>
        /// Indicates to find element using the HasKeyboardFocus Property
        /// </summary>
        HasKeyboardFocus,

        /// <summary>
        /// Indicates to find element using the HeadingLevel Property
        /// </summary>
        HeadingLevel,

        /// <summary>
        /// Indicates to find element using the HelpText Property
        /// </summary>
        HelpText,

        /// <summary>
        /// Indicates to find element using the IsContentElement Property
        /// </summary>
        IsContentElement,

        /// <summary>
        /// Indicates to find element using the IsControlElement Property
        /// </summary>
        IsControlElement,

        /// <summary>
        /// Indicates to find element using the IsDataValidForForm Property
        /// </summary>
        IsDataValidForForm,

        /// <summary>
        /// Indicates to find element using the IsDialog Property
        /// </summary>
        IsDialog,

        /// <summary>
        /// Indicates to find element using the IsEnabled Property
        /// </summary>
        IsEnabled,

        /// <summary>
        /// Indicates to find element using the IsKeyboardFocusable Property
        /// </summary>
        IsKeyboardFocusable,

        /// <summary>
        /// Indicates to find element using the IsOffscreen Property
        /// </summary>
        IsOffscreen,

        /// <summary>
        /// Indicates to find element using the IsPassword Property
        /// </summary>
        IsPassword,

        /// <summary>
        /// Indicates to find element using the IsPeripheral Property
        /// </summary>
        IsPeripheral,

        /// <summary>
        /// Indicates to find element using the IsRequiredForForm Property
        /// </summary>
        IsRequiredForForm,

        /// <summary>
        /// Indicates to find element using the LiveSetting Property
        /// </summary>
        LiveSetting,

        /// <summary>
        /// Indicates to find element using the LocalizedControlType Property
        /// </summary>
        LocalizedControlType,

        /// <summary>
        /// Indicates to find element using the NativeWindowHandle Property
        /// </summary>
        NativeWindowHandle,

        /// <summary>
        /// Indicates to find element using the OptimizeForVisualContent Property
        /// </summary>
        OptimizeForVisualContent,

        /// <summary>
        /// Indicates to find element using the Orientation Property
        /// </summary>
        Orientation,

        /// <summary>
        /// Indicates to find element using the ProcessId Property
        /// </summary>
        ProcessId,

        /// <summary>
        /// Indicates to find element using the VisualEffect Property
        /// </summary>
        VisualEffect

        #endregion
    }
}
