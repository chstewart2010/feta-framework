﻿namespace Tr.Feta.Common.Enums
{
    /// <summary>
    /// Container for TestSession State
    /// </summary>
    public enum TestSessionStatus
    {
        /// <summary>
        /// Active TestSession, Interruptable, Abortable, Completable
        /// </summary>
        Active = 1,

        /// <summary>
        /// Finished TestSession, Not Interruptable, Not Abortable
        /// </summary>
        Finished = 2,
        
        /// <summary>
        /// Ready to be Interrupted, Abortable, not Completable
        /// </summary>
        InterruptRequested = 3,

        /// <summary>
        /// Interrupted, not Abortable, not Completable
        /// </summary>
        Interrupted = 4,

        /// <summary>
        /// Aborted TestSession, not Interruptable, not Completable
        /// </summary>
        Aborted = 5
    }
}
