﻿namespace Tr.Feta.Common.Enums
{
    /// <summary>
    /// Collection of supported Test Reporter Modes
    /// </summary>
    public enum FetaReporterMode
    {
        /// <summary>
        /// Represents the TestRail Reporting System
        /// </summary>
        Web = 1
    }
}
