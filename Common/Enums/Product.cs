﻿namespace Tr.Feta.Common.Enums
{
    /// <summary>
    /// Container for list of supported Products
    /// </summary>
    public enum Product : ulong
    {
        /// <summary>
        /// Error value set when an invalid Product is add to the app.config
        /// </summary>
        Invalid,

        /// <summary>
        /// Sample product
        /// </summary>
        Sample
    }
}
