﻿namespace Tr.Feta.Common.Enums
{
    /// <summary>
    /// Container for valid core commands
    /// </summary>
    public enum FrameworkCommand
    {
        /// <summary>
        /// Represents the --run command
        /// </summary>
        Run = 1,

        /// <summary>
        /// Represents the --list command
        /// </summary>
        List,

        /// <summary>
        /// Represents the --abort command
        /// </summary>
        Abort,

        /// <summary>
        /// Represent the --stop command
        /// </summary>
        Stop
    }
}
