﻿namespace Tr.Feta.Common.Enums
{
    /// <summary>
    /// Specifies constants which represents tables found in the local database
    /// </summary>
    internal enum DatabaseTableName
    {
        /// <summary>
        /// The TestSession table in the database
        /// </summary>
        TestSession = 1,

        /// <summary>
        /// The TestCase table in the database
        /// </summary>
        TestCase
    }
}
