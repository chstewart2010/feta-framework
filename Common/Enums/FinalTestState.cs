﻿namespace Tr.Feta.Common.Enums
{
    /// <summary>
    /// Test State container
    /// </summary>
    public enum FinalTestState
    {
        /// <summary>
        /// The executor has lost track of this test
        /// </summary>
        Unknown,

        /// <summary>
        /// The test has passed execution
        /// </summary>
        Passed,

        /// <summary>
        /// IThe test could not finish due to an error.
        /// </summary>
        Error,

        /// <summary>
        /// The test has failed due to an assertion
        /// </summary>
        Failed,

        /// <summary>
        /// The test ended early due to an abort request
        /// </summary>
        Aborted
    }
}
