﻿using System;

namespace Tr.Feta.Common.Attributes
{
    internal class SchemaPathAttribute : Attribute
    {
        /// <summary>
        /// Path to Schema file
        /// </summary>
        internal string SchemaPath { get; }

        /// <summary>
        /// Constructor for SchemaPath
        /// </summary>
        /// <param name="schemaPath">Path to schema file</param>
        internal SchemaPathAttribute(string schemaPath)
        {
            SchemaPath = schemaPath;
        }
    }
}
