﻿namespace Tr.Feta.Common.Exceptions
{
    /// <summary>
    /// The Exception that is thrown when an invalid Test Action is referenced
    /// </summary>
    public class InvalidTestActionException : TestingFrameworkException
    {
        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="message">Message to add to the exception</param>
        public InvalidTestActionException(string message) : base(message) {}
    }
}
