﻿namespace Tr.Feta.Common.Exceptions
{
    /// <summary>
    /// The Exception that is thrown when bad data is used to build the DataModel
    /// </summary>
     public class DataModelException : TestingFrameworkException
    {
        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="message">Message to add to the exception</param>
        public DataModelException(string message) : base(message) {}
    }
}
