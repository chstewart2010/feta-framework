﻿namespace Tr.Feta.Common.Exceptions
{
    /// <summary>
    /// The Exception that is thrown when a TestCase is aborted
    /// </summary>
    public class AbortedTestCaseException : TestingFrameworkException
    {
        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="message">Message to add to the exception</param>
        public AbortedTestCaseException(string message) : base(message) {}
    }
}
