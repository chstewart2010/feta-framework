﻿namespace Tr.Feta.Common.Exceptions
{
    /// <summary>
    /// Exception that is thrown when the ConfigurationManager is invalid
    /// </summary>
    public class InvalidConfigurationException : TestingFrameworkException
    {
        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="message">Message to add to the exception</param>
        public InvalidConfigurationException(string message) : base(message){}
    }
}
