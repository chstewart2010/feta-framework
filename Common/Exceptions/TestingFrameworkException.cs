﻿using System;

namespace Tr.Feta.Common.Exceptions
{
    /// <summary>
    /// Represents Exceptions for projects using the CommonLibrary
    /// </summary>
    public class TestingFrameworkException : Exception
    {
        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="message">Message to add to the exception</param>
        public TestingFrameworkException(string message) : base(message){}
    }
}
