﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using log4net;
using log4net.Appender;
using log4net.Config;
using Tr.Feta.Common.Exceptions;

namespace Tr.Feta.Common.Utilities
{
    /// <summary>
    /// Class to log events from test case
    /// </summary>
    public static class Logger
    {
        private static readonly Dictionary<string,ILog> Loggers = new Dictionary<string, ILog>();
        private static ILog _currentLogger;
        private static readonly string LoggerConfig;
        private const string GlobalLogger = "global";
        private static string _currentLoggerName;

        /// <summary>
        /// Initializes a new Logger object for logging events
        /// </summary>
        static Logger()
        {
            var executableBase = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? ".";
            LoggerConfig = Path.Combine
            (
                executableBase,
                "Tr.Feta.Common.log4net.config"
            );
            AddNewLogger
            (
                GlobalLogger,
                GlobalLogger
            );

            SwitchLogger(GlobalLogger);
        }

        /// <summary>
        /// Path to the log file
        /// </summary>
        public static string LogFile => _currentLogger.Logger.Repository.GetAppenders()
            .Where(appender => appender is FileAppender)                            // only pay attention to FileAppenders
            .Select(appender => ((FileAppender)appender).File).FirstOrDefault();    // there should only be one FileAppender, so get the first

        /// <summary>
        /// Closes the logger after session is complete
        /// </summary>
        public static void CloseCurrentLogger()
        {
            if (_currentLoggerName == GlobalLogger &&
                Loggers.Count > 1)
            {
                throw new TestingFrameworkException("Cannot manually close global logger until all other loggers are closed");
            }

            _currentLogger.Logger.Repository.Shutdown();
            Loggers.Remove(_currentLoggerName);
            _currentLoggerName = GlobalLogger;
        }

        /// <summary>
        /// Logs information regarding the latest caught exception
        /// </summary>
        public static void ReportException(Exception ex)
        {
            _currentLogger.Fatal
            (
                ex.Message,
                ex
            );
        }

        /// <summary>
        /// Create a unique logger for the name given
        /// </summary>
        /// <param name="uniqueId">A unique value to assign to the created log</param>
        /// <param name="logName">The name with which to associated the created logger</param>
        public static void AddNewLogger(string uniqueId, string logName)
        {
            if (Loggers.ContainsKey(logName))
            {
                throw new TestingFrameworkException($"Logger already contains a log with associated with {logName}");
            }

            var repository = LogManager.CreateRepository(uniqueId);
            XmlConfigurator.Configure(repository, new FileInfo(LoggerConfig));
            var log = LogManager.GetLogger
            (
                uniqueId,
                logName
            );
            Loggers.Add
            (
                logName,
                log
            );
        }

        /// <summary>
        /// Switches to the default global logger
        /// </summary>
        public static void SwitchToGlobalLogger() => SwitchLogger(GlobalLogger);

        /// <summary>
        /// Switches to a previously created logger
        /// </summary>
        /// <param name="logName">The name of the previously created logger to switch</param>
        public static void SwitchLogger(string logName)
        {
            if (Loggers.Count == 0)
            {
                throw new TestingFrameworkException("All loggers have shut down.");
            }

            if (!Loggers.TryGetValue(logName, out var log))
            {
                throw new TestingFrameworkException($"Unknown logger associated with name {logName}");
            }

            _currentLoggerName = logName;
            _currentLogger = log;
        }

        /// <summary>
        /// Method for logging DEBUG-level events
        /// </summary>
        /// <param name="value">Message to log</param>
        public static void Debug(object value) => _currentLogger.Debug(value);

        /// <summary>
        /// Method for logging INFO-level events
        /// </summary>
        /// <param name="value">Message to log</param>
        public static void Info(object value) => _currentLogger.Info(value);

        /// <summary>
        /// Method for logging WARN-level events
        /// </summary>
        /// <param name="value">Message to log</param>
        public static void Warn(object value) => _currentLogger.Warn(value);

        /// <summary>
        /// Method for logging ERROR-level events
        /// </summary>
        /// <param name="value">Message to log</param>
        public static void Error(object value) => _currentLogger.Error(value);
    }
}
