﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Tr.Feta.Common.Attributes;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Exceptions;

namespace Tr.Feta.Common.Utilities
{
    /// <summary>
    /// Exposes static methods for file system management
    /// </summary>
    public static class FileManager
    {
        /// <summary>
        /// Copies all files, and subdirectories if applicable, to a new directory
        /// </summary>
        /// <param name="sourceDirName">Source directory to copy from</param>
        /// <param name="destDirName">Destination directory to copy to</param>
        /// <param name="extensionsToExclude">Collection of file extensions to exclude from directory copy</param>
        /// <param name="recursive">Whether to copy files in subdirectories</param>
        public static void CopyDirectory(
            string sourceDirName,
            string destDirName,
            ReadOnlyCollection<string> extensionsToExclude,
            bool recursive = true)
        {
            DirectoryInfo directory = new DirectoryInfo(sourceDirName);

            if (!directory.Exists)
            {
                Logger.Error($"Source directory does not exist or could not be found: {sourceDirName}");
            }

            Directory.CreateDirectory(destDirName);

            foreach (var file in directory.GetFiles())
            {
                if (extensionsToExclude?.Contains(Path.GetExtension(file.Name)) == true)
                {
                    continue;
                }

                string tempPath = Path.Combine
                (
                    destDirName,
                    file.Name
                );
                file.CopyTo
                (
                    tempPath,
                    false
                );
            }

            if (recursive)
            {
                foreach (var subDirectory in directory.GetDirectories())
                {
                    string tempPath = Path.Combine
                    (
                        destDirName,
                        subDirectory.Name
                    );
                    CopyDirectory
                    (
                        subDirectory.FullName,
                        tempPath,
                        extensionsToExclude
                    );
                }
            }
        }

        /// <summary>
        /// Creates a fully qualified file path with no invalid path characters
        /// </summary>
        /// <param name="outputFolder">Directory to save the file</param>
        /// <param name="fileNameBase">THe file's name</param>
        /// <param name="extension">the file extension</param>
        /// <param name="isUnique">
        /// Whether to create a unique file. If set to false and th filepath already exists, this function throws a <see cref="TestingFrameworkException"/>
        /// </param>
        public static string GenerateFilePath(
            string outputFolder,
            string fileNameBase,
            string extension,
            bool isUnique)
        {
            string filePath = Path.Combine
            (
                outputFolder,
                $"{fileNameBase}.{extension}"
            );

            if (!File.Exists(filePath))
            {
                return GetValidFilePath(filePath);
            }

            if (!isUnique)
            {
                throw new TestingFrameworkException($"{filePath} already exists");
            }


            var uniqueId = 1;
            while (true)
            {
                filePath = Path.Combine
                (
                    outputFolder,
                    $"{fileNameBase}-{uniqueId}.{extension}"
                );

                if (File.Exists(filePath))
                {
                    uniqueId++;
                    continue;
                }

                return GetValidFilePath(filePath);
            }
        }

        private static string GetValidFilePath(string filePath)
        {
            filePath = Path.GetInvalidPathChars().Aggregate(filePath, (current, invalid) => current.Replace(invalid, '_'));
            return filePath.Trim('_');
        }

        /// <summary>
        /// Searches for a directory, and its sub directories, for a file with the name specified
        /// </summary>
        /// <param name="directory">Starting directory for the file search</param>
        /// <param name="fileName">The name of the file to search for</param>
        /// <param name="isRecursive">Whether to search the top directory or all child directories</param>
        /// <returns></returns>
        public static string FindFile(
            string directory,
            string fileName,
            bool isRecursive)
        {
            return Directory.GetFiles
            (
                directory,
                $"{fileName}*",
                isRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly
            ).FirstOrDefault();
        }

        /// <summary>
        /// Parses the json from the path and validates against the schema
        /// </summary>
        /// <param name="jsonPath">Path to the json file</param>
        /// <param name="schema">Path to the json schema file</param>
        /// <param name="json">The parsed json object. This object returns null if validation fails.</param>
        public static bool TryValidateJsonFile(
            string jsonPath,
            string schema,
            out JObject json)
        {
            var jsonString = File.ReadAllText(jsonPath);
            json = JObject.Parse(jsonString);
            var jsonSchema = File.ReadAllText(schema);

            if (json.IsValid(JSchema.Parse(jsonSchema), out IList<string> errors))
            {
                return true;
            }

            Logger.Error($"Errors found in json {jsonPath}");
            Logger.Error(string.Join("\n", errors));
            json = null;
            return false;
        }

        /// <summary>
        /// Returns the file path of the associated schema
        /// </summary>
        /// <param name="schema">The schema file to retrieve</param>
        public static string GetSchemaFile(SchemaType schema)
        {
            SchemaPathAttribute attribute = typeof(SchemaType)
                .GetField(schema.ToString())
                .GetCustomAttribute<SchemaPathAttribute>();
            return attribute.SchemaPath;
        }
    }
}
