﻿using System;
using System.Collections.Specialized;

namespace Tr.Feta.Common.Utilities
{
    /// <summary>
    /// Provides a collection of methods for extracting values from an App.config
    /// </summary>
    public static class ConfigurationHelper
    {

        /// <summary>
        /// Returns a property from a NameValueCollection
        /// </summary>
        /// <param name="collection">The collection to inspect</param>
        /// <param name="property">The property to search for</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static string GetProperty(
            NameValueCollection collection,
            string property)
        {
            if (collection == null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            if (string.IsNullOrEmpty(property))
            {
                throw new ArgumentNullException(nameof(property));
            }

            return string.IsNullOrEmpty(collection[property])
                ? throw new NullReferenceException($"{property} property is missing")
                : collection[property];
        }
    }
}
