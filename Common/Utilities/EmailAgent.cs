﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Net.Mail;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.TestResultsData;

namespace Tr.Feta.Common.Utilities
{
    /// <summary>
    /// Provides a method for send a Feta-tailored email with all post test-execution data supplied.
    /// </summary>
    public static class EmailAgent
    {
        /// <summary>
        /// Provides an object for sending a collection of properties for the <see cref="EmailAgent"/>
        /// </summary>
        public class EmailClientProperties
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="EmailClientProperties"/> class.
            /// </summary>
            /// <param name="server">The email server to send the mail message</param>
            /// <param name="senderUsername">The username to use for sending the email</param>
            /// <param name="senderPassword">The password associated with the username</param>
            /// <param name="recipients">Collection of email recipients</param>
            /// <param name="port">The port on the server to send to email</param>
            /// <param name="enableSsl">Whether to enable SSL</param>
            public EmailClientProperties(
                string server,
                string senderUsername,
                string senderPassword,
                IEnumerable<string> recipients,
                int port,
                bool enableSsl)
            {
                if (string.IsNullOrEmpty(senderUsername))
                {
                    throw new ArgumentNullException(nameof(senderUsername));
                }
                if (string.IsNullOrEmpty(senderPassword))
                {
                    throw new ArgumentNullException(nameof(senderUsername));
                }
                if (port < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(port));
                }

                Server = server;
                SenderUsername = senderUsername;
                SenderPassword = senderPassword;
                Recipients = recipients ?? throw new ArgumentNullException(nameof(senderUsername));
                Port = port;
                EnableSsl = enableSsl;
            }

            /// <summary>
            /// The Smtp Client Server for sending the email
            /// </summary>
            internal string Server { get; }

            /// <summary>
            /// The senders email address
            /// </summary>
            internal string SenderUsername { get; }

            /// <summary>
            /// The senders email address password
            /// </summary>
            internal string SenderPassword { get; }

            /// <summary>
            /// The recipients for the email
            /// </summary>
            internal IEnumerable<string> Recipients { get; }

            /// <summary>
            /// The server port to send the email message
            /// </summary>
            internal int Port { get; }

            /// <summary>
            /// Whether to enable SSL
            /// </summary>
            internal bool EnableSsl { get; }
        }

        private const string SessionSummaryTemplate =
            "<p>UI Automation for {0} has completed on server {1} <br/><br/>Run time:<br/>&emsp;Start time: {2}<br/>&emsp;End time: {3}<br/><br/>Summary:<br/>Total number of tests ran: {4}<ul><li>Successful cases: {5}, {6}% passed</li><li>Failed cases: {7}, {8}% failed</li></ul><li>Errored cases: {9}, {10}% failed</li></ul></p>";

        /// <summary>
        /// Sends an email with post-run summary data from Feta
        /// </summary>
        /// <param name="emailClientProperties">A collection of user-specific properties for configuring the MailMessage and SmtpClient objects</param>
        /// <param name="subjectLine">Subject line of the email to send</param>
        /// <param name="sessionResult">Summary data to build the email body from</param>
        /// <param name="attachmentFilePaths">Path to attachment files</param>
        public static void SendPostRunEmail(
            EmailClientProperties emailClientProperties,
            string subjectLine,
            SessionResult sessionResult,
            params string[] attachmentFilePaths)
        {
            using var message = GenerateMailMessage
            (
                emailClientProperties.SenderUsername,
                emailClientProperties.Recipients,
                subjectLine,
                sessionResult,
                attachmentFilePaths
            );

            using SmtpClient client = new SmtpClient(emailClientProperties.Server)
            {
                Credentials = new System.Net.NetworkCredential
                (
                    emailClientProperties.SenderUsername,
                    emailClientProperties.SenderPassword
                ),
                Port = emailClientProperties.Port,
                EnableSsl = emailClientProperties.EnableSsl
            };

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                Logger.Error($"Unexpected exception {ex.GetType().Name}: {ex.Message}");
            }
        }

        /// <summary>
        /// Returns a formatted <see cref="MailMessage"/>
        /// </summary>
        /// <param name="senderUsername">The username to use for sending the email</param>
        /// <param name="recipients">Collection of email recipients</param>
        /// <param name="subjectLine">Subject line of the email to send</param>
        /// <param name="sessionResult">Summary data to build the email body from</param>
        /// <param name="attachmentFilePaths">Path to attachment files</param>
        private static MailMessage GenerateMailMessage(
            string senderUsername,
            IEnumerable<string> recipients,
            string subjectLine,
            SessionResult sessionResult,
            string[] attachmentFilePaths)
        {
            var message = new MailMessage()
            {
                From = new MailAddress(senderUsername),
                IsBodyHtml = true,
                Subject = subjectLine,
                Body = GenerateEmailBody(sessionResult),
            };

            foreach (var recipient in recipients)
            {
                message.To.Add(recipient);
            }

            if (attachmentFilePaths != null)
            {
                foreach (var attachment in attachmentFilePaths)
                {
                    message.Attachments.Add(new Attachment
                    (
                        attachment,
                        MediaTypeNames.Application.Octet
                    ));
                }
            }

            return message;
        }

        /// <summary>
        /// Build the email body from our suite results
        /// </summary>
        /// <param name="sessionResult">Results from session execution</param>
        private static string GenerateEmailBody(SessionResult sessionResult)
        {
            var failedCases = $"<p>{GenerateFailedCasesHtml(sessionResult.Results)}</p>";
            var sharedFolderWarning = sessionResult.FailRuns + sessionResult.ErrorRuns > 0
                ? $@"<p>Request access to \\{Environment.MachineName} if you cannot access this file.</p>"
                : string.Empty;
            return $"{GetHtmlFormattedSummary(sessionResult)}{failedCases}{sharedFolderWarning}";
        }

        /// <summary>
        /// Returns html test based on the post-run summary data
        /// </summary>
        /// <param name="sessionResult"></param>
        private static string GetHtmlFormattedSummary(SessionResult sessionResult)
        {
            double totalRuns = sessionResult.SuccessfulRuns + sessionResult.FailRuns + sessionResult.ErrorRuns;
            var passPercentage = 100 * sessionResult.SuccessfulRuns / totalRuns;
            var failPercentage = 100 * sessionResult.FailRuns / totalRuns;
            var errorPercentage = 100 * sessionResult.ErrorRuns / totalRuns;

            return string.Format
            (
                SessionSummaryTemplate,
                sessionResult.SessionName,
                Environment.MachineName,
                sessionResult.StartTime.ToString("F"),
                sessionResult.EndTime.ToString("F"),
                totalRuns,
                sessionResult.SuccessfulRuns,
                Math.Round(passPercentage, 2),
                sessionResult.FailRuns,
                Math.Round(failPercentage, 2),
                sessionResult.ErrorRuns,
                Math.Round(errorPercentage, 2)
            );
        }

        /// <summary>
        /// Returns a the failed cases section of the email body
        /// </summary>
        /// <param name="results">Collection of SuiteResults to build failed cases section</param>
        private static string GenerateFailedCasesHtml(IEnumerable<SuiteResult> results)
        {
            var totalFailedCases = string.Empty;

            foreach (var result in results)
            {
                var failedCases = result.CaseResults
                    .Where(testResult => testResult.FinalTestState != FinalTestState.Passed)
                    .Aggregate
                    (
                        string.Empty,
                        (current, testResult) =>
                            current +
                            $"<li>{testResult.Name}: <a href=\"{testResult.TestCaseLogFileUri}\">{testResult.FinalTestState}</a></li>"
                    );

                failedCases += GenerateFailedCasesHtml(result.ChildSuiteResults);
                totalFailedCases += (string.IsNullOrEmpty(failedCases)
                    ? failedCases
                    : $"Suite name: {result.Name}:<ul>{failedCases}</ul>");
            }

            return totalFailedCases;
        }
    }
}
