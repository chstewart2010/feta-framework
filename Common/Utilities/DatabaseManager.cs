﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Utilities.Records;

namespace Tr.Feta.Common.Utilities
{
    /// <summary>
    /// Provides a collection of functions for manipulating the mdf file
    /// </summary>
    public static class DatabaseManager
    {
        private static readonly string DatabaseFilePath = Path.Combine
        (
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
            "..",
            "Database",
            "Tr.Feta.TestExecution.mdf"
        );

        public static void InsertTestResult(TestResultsData.CaseResult caseResult, string sessionId)
        {

        }

        #region TestSessionTable

        /// <summary>
        /// Insert test session record into TestSession Table
        /// </summary>
        /// <param name="sessionId">SessionId of Current Test</param>
        public static void InsertTestSession(string sessionId)
        {
            var query = "INSERT TestSession VALUES " +
                        "(" +
                        $"'{sessionId}'," +
                        $"'{Environment.MachineName}'," +
                        $"'{Process.GetCurrentProcess().Id}'," +
                        $"'{TestSessionStatus.Active}'," +
                        $"'{DateTime.Now}'" +
                        ")";

            using var connection = GetDatabaseConnection();

            if (connection == null)
            {
                return;
            }

            connection.Open();
            using var command = new SqlCommand(query) { Connection = connection };
            command.ExecuteNonQuery();
            connection.Close();
        }

        /// <summary>
        /// Lists all Active Sessions
        /// </summary>
        public static IEnumerable<TestSessionRecord> GetActiveTestSessions()
        {
            var query = "Select * " +
                        "FROM TestSession " +
                        $"WHERE Status='{TestSessionStatus.Active}'";
            var records = new List<TestSessionRecord>();

            using SqlConnection connection = GetDatabaseConnection();

            if (connection == null)
            {
                return new List<TestSessionRecord>();
            }

            connection.Open();
            using var command = new SqlCommand(query) { Connection = connection };
            using var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    var record = new TestSessionRecord
                    {
                        SessionId = (string)reader["SessionId"],
                        ActiveStartTime = (DateTime)reader["StartTime"],
                        Owner = (string)reader["Owner"],
                        ProcessId = (int)reader["PID"]
                    };

                    if (Enum.TryParse(reader["Status"].ToString(), out TestSessionStatus status))
                    {
                        record.ActiveStatus = status;
                    }

                    records.Add(record);
                }
            }

            connection.Close();
            return records;
        }

        /// <summary>
        /// Get Status by SessionId
        /// </summary>
        /// <param name="sessionId">SessionId of a test</param>
        public static TestSessionStatus GetSessionStatus(string sessionId) => GetStatuses<TestSessionStatus>
        (
            "Status",
            DatabaseTableName.TestSession,
            $"SessionId='{sessionId}'"
        ).FirstOrDefault();

        /// <summary>
        /// Update test status in database
        /// </summary>
        /// <param name="sessionId">SessionId of a test</param>
        /// <param name="status">Status of the current test</param>
        public static int UpdateSessionStatus(
            string sessionId,
            TestSessionStatus status)
        {
            var query =
                "UPDATE TestSession " +
                $"SET Status = '{status}' " +
                $"WHERE SessionId = '{sessionId}' AND Status in ('Active', 'InterruptRequested', 'Interrupted')";
            using var connection = GetDatabaseConnection();

            if (connection == null)
            {
                return 0;
            }

            connection.Open();
            using var command = new SqlCommand(query) { Connection = connection };
            using var reader = command.ExecuteReader();
            connection.Close();
            return reader.RecordsAffected;
        }

        /// <summary>
        /// Clean up data from database based on given time by the user
        /// </summary>
        /// <param name="maxDaysInDatabase">The maximum allow time in days to keep a record.</param>
        public static void CleanupExpiredTestSessions(int maxDaysInDatabase) => RemoveExpiredRecords
        (
            maxDaysInDatabase,
            DatabaseTableName.TestSession
        );

        #endregion

        /// <summary>
        /// Clean up data from database based on given time by the user
        /// </summary>
        /// <param name="maxDaysInDatabase">The maximum allow time in days to keep a record.</param>
        /// <param name="table"></param>
        private static void RemoveExpiredRecords(
            int maxDaysInDatabase,
            DatabaseTableName table)
        {
            DateTime earliestAllowedDate = DateTime.Now.AddDays(maxDaysInDatabase * -1);
            var query = $"Delete FROM {table} WHERE StartTime < '{earliestAllowedDate}'";
            using SqlConnection connection = GetDatabaseConnection();

            if (connection == null)
            {
                return;
            }

            connection.Open();
            using var command = new SqlCommand(query) { Connection = connection };
            command.ExecuteNonQuery();
            connection.Close();
        }

        /// <summary>
        /// Get Status from one of the DatabaseStatusColumns using a specified condition
        /// </summary>
        /// <param name="column">The database column to select from</param>
        /// <param name="table">The table to pull the status value from</param>
        /// <param name="condition">Conditions</param>
        private static IEnumerable<TEnum> GetStatuses<TEnum>(
            string column,
            DatabaseTableName table,
            string condition) where TEnum : struct, Enum
        {
            var query = $"SELECT {column} " +
                        $"FROM {table} " +
                        $"WHERE {condition}";
            using var connection = GetDatabaseConnection();
            var results = new List<TEnum>();

            if (connection == null)
            {
                return results;
            }

            connection.Open();
            using var command = new SqlCommand(query) { Connection = connection };
            using var reader = command.ExecuteReader();

            while (reader.Read())
            {
                if (Enum.TryParse(reader[0].ToString(), out TEnum status))
                {
                    results.Add(status);
                }
            }

            return results;
        }

        /// <summary>
        /// Creates a connection with TestDatabase.mdf
        /// </summary>
        private static SqlConnection GetDatabaseConnection()
        {
            try
            {
                return new SqlConnection
                (
                    @"Data Source=(LocalDB)\MSSQLLocalDB;" +
                    $"AttachDbFilename={DatabaseFilePath};" +
                    "Integrated Security = False;" +
                    "Asynchronous Processing = True;" +
                    "Replication = True;" +
                    "Connect Timeout=1;" +
                    "User Instance = false;"
                );
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to access database.");
                Console.WriteLine($"{ex.GetType().Name}: {ex.Message}");
                return null;
            }
        }
    }
}
