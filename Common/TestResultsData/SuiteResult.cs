﻿using System.Collections.Generic;
using Tr.Feta.Common.Enums;

namespace Tr.Feta.Common.TestResultsData
{
    /// <summary>
    /// Container for test suite results
    /// </summary>
    public class SuiteResult
    {
        /// <summary>
        /// Suite Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Feature or section of the product that the cases relate to
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// Run id created at the beginning of the test session
        /// </summary>
        public ulong? RunId { get; set; }

        /// <summary>
        /// Collection of Test Case Results for the suite
        /// </summary>
        public IEnumerable<CaseResult> CaseResults { get; set; }

        /// <summary>
        /// Collection of Child Suite Results for the suite
        /// </summary>
        public IEnumerable<SuiteResult> ChildSuiteResults { get; set; }

        /// <summary>
        /// Number of test cases that passed test execution
        /// </summary>
        public int SuccessfulRuns { get; set; }

        /// <summary>
        /// NUmber of test cases that failed test execution
        /// </summary>
        public int FailedRuns { get; set; }

        /// <summary>
        /// Number of test cases that ended in an unexpected error
        /// </summary>
        public int ErrorRuns { get; set; }
    }
}
