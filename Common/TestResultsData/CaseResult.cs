﻿using System;
using Tr.Feta.Common.Enums;

namespace Tr.Feta.Common.TestResultsData
{
    /// <summary>
    /// Container for test case results
    /// </summary>
    public class CaseResult
    {
        /// <summary>
        /// Test Case Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Test Case Outcome from test execution
        /// </summary>
        public FinalTestState FinalTestState { get; set; }

        /// <summary>
        /// Path to the test case log file
        /// </summary>
        public string TestCaseLogFile { get; set; }

        /// <summary>
        /// Path to test case log file for email
        /// </summary>
        public string TestCaseLogFileUri { get; set; }

        /// <summary>
        /// The time it took to test case to execute
        /// </summary>
        public TimeSpan ElapsedTime { get; set; }
    }
}