﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Tr.Feta.Common.TestResultsData
{
    /// <summary>
    /// Represents a post-run session summary for the UI Testing Framework.
    /// </summary>
    public class SessionResult
    {
        /// <summary>
        /// Initializes a new instance of <see cref="SessionResult"/>
        /// </summary>
        /// <param name="results"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="sessionName"></param>
        public SessionResult(
            ReadOnlyCollection<SuiteResult> results,
            DateTime startTime,
            DateTime endTime,
            string sessionName)
        {
            Results = results;
            StartTime = startTime;
            EndTime = endTime;
            SessionName = sessionName;
            AddResults(Results);
        }

        internal ReadOnlyCollection<SuiteResult> Results { get; }

        internal DateTime StartTime { get; }

        internal DateTime EndTime { get; }

        internal string SessionName { get; }

        internal int SuccessfulRuns { get; private set; }

        internal int FailRuns { get; private set; }

        internal int ErrorRuns { get; private set; }

        private void AddResults(IEnumerable<SuiteResult> results)
        {
            if (results == null)
            {
                return;
            }

            foreach (var result in results)
            {
                SuccessfulRuns += result.SuccessfulRuns;
                FailRuns += result.FailedRuns;
                ErrorRuns += result.ErrorRuns;
                AddResults(result.ChildSuiteResults);
            }
        }
    }
}
