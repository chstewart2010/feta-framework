﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Exceptions;
using Tr.Feta.Common.Utilities;
using Tr.Feta.Core.Configurations;
using Tr.Feta.Core.TestSession;

namespace Tr.Feta.Core.DataModel
{
    /// <summary>
    /// Contains methods to build the Data Model from json
    /// </summary>
    internal static class ModelBuilderFromJson
    {
        private static readonly string JsonTestCaseSchemaPath = Path.Combine
        (
            ConfigurationProperties.SchemaDirectory,
            FileManager.GetSchemaFile(SchemaType.JsonTestCase)
        );

        private static readonly string JsonSuiteSchemaPath = Path.Combine
        (
            ConfigurationProperties.SchemaDirectory,
            FileManager.GetSchemaFile(SchemaType.JsonSuite)
        );

        private enum JValueType
        {
            ClearContainers = 1,
            Log,
            SwitchToTopLevelClient,
            SwitchToDesktop
        }

        /// <summary>
        /// Returns null if suiteJsonPath or testCaseLibraryDirectory is not properly configured
        /// </summary>
        /// <param name="suiteJsonPath">Path to the suite json</param>
        /// <param name="productName">Product that is being tested</param>
        /// <param name="familyTree"></param>
        /// <exception cref="DataModelException"></exception>
        internal static TestSuite BuildTestSuite(
            string suiteJsonPath,
            string productName,
            string[] familyTree = null)
        {
            if (!File.Exists(suiteJsonPath))
            {
                Logger.Error($"No file found at {suiteJsonPath}");
                return null;
            }

            var tryResult = FileManager.TryValidateJsonFile
            (
                suiteJsonPath,
                JsonSuiteSchemaPath,
                out var suiteJson
            );

            if (!tryResult)
            {
                return null;
            }

            var name = suiteJson["name"].GetString();

            if (familyTree?.Contains(name) == true)
            {
                Logger.Error
                (
                    "Attempted to add an already existing parent suite as a new child suite"
                );

                for (int i = 0; i < familyTree.Length; i++)
                {
                    Logger.Error($"Suite {i}: {familyTree[i]}");
                }

                Logger.Error($"Suite {familyTree.Length}: {name}");
                throw new DataModelException
                (
                    "Attempted to add an already existing parent suite as a new child suite"
                );
            }

            var testSuite = new TestSuite
            (
                name,
                familyTree
            )
            {
                Milestone = suiteJson["milestone"]?.GetString(),
                Product = Enum.TryParse(productName, true, out Product product) ? product : default
            };

            foreach (var child in suiteJson["cases"])
            {
                string testCaseId = child["id"].GetString();
                string caseFile = FileManager.FindFile
                (
                    ConfigurationProperties.TestCaseLibrary,
                    testCaseId,
                    true
                );

                if (caseFile == null)
                {
                    Logger.Error
                    (
                        $"No test case found named id {testCaseId} in {ConfigurationProperties.TestCaseLibrary}."
                    );
                    continue;
                }

                testSuite.TestCases.Add
                (
                    BuildTestCase(testSuite, caseFile) ??
                    throw new DataModelException($"Invalid test case json {caseFile}")
                );
            }

            foreach (var child in suiteJson["suites"])
            {
                string suiteId = child["id"].GetString();
                string suiteFile = FileManager.FindFile
                (
                    ConfigurationProperties.TestSuiteLibrary,
                    suiteId,
                    true
                );

                if (suiteFile == null)
                {
                    Logger.Error
                    (
                        $"No test suite found named id {suiteId} in {ConfigurationProperties.TestSuiteLibrary}."
                    );
                    continue;
                }

                testSuite.ChildSuites.Add(BuildTestSuite
                (
                    suiteFile,
                    productName,
                    testSuite.OutputSuitePath.Split('\\')
                ) ?? throw new DataModelException($"Invalid suite json {suiteFile}"));
            }

            // The top level test suite should only contain child suites
            // when reporting test cases to a Test Management Tool (e.g. TestRail)
            if (ConfigurationProperties.TestReporterMode != default &&
                testSuite.IsTopLevel &&
                testSuite.TestCases.Any())
            {
                Logger.Error($"Top Level Suite {testSuite} should not have any test cases.");
                return null;
            }

            return testSuite;
        }

        /// <summary>
        /// Returns null if testCaseFilePath is not properly configured
        /// </summary>
        /// <param name="suite">Suite name if Applicable</param>
        /// <param name="testCaseFilePath">path to test case json</param>
        internal static TestCase BuildTestCase(
            TestSuite suite,
            string testCaseFilePath)
        {
            if (!File.Exists(testCaseFilePath))
            {
                Logger.Error($"No file found at {testCaseFilePath}");
                return null;
            }

            var tryResult = FileManager.TryValidateJsonFile
            (
                testCaseFilePath,
                JsonTestCaseSchemaPath,
                out var testCaseJson
            );

            if (!tryResult)
            {
                return null;
            }

            var testCase = new TestCase(suite.OutputSuitePath)
            {
                Name = testCaseJson["name"].GetString(),
                ExpectedResult = testCaseJson["expectedResult"]?.GetString(),
                Product = suite.Product,
                FilePath = Path.GetFullPath(testCaseFilePath),
                Steps = testCaseJson["steps"].Select
                (
                    property =>
                        GetTestStep(property.GetObject()) ??
                        throw new NullReferenceException("Null TestStep")
                ).ToArray()
            };

            return testCase;
        }

        private static TestStep GetTestStep(JObject testStepNode)
        {
            var testStep = new TestStep
            {
                Description = testStepNode["description"]?.GetString()
            };

            var actions = GetTestActions(testStepNode["actions"].GetArray());

            if (actions == null)
            {
                return null;
            }

            testStep.Actions = actions;
            return testStep;
        }

        private static TestAction GetTestAction(JProperty testActionNode)
        {
            var jsonValue = testActionNode.Value;
            var testAction = new TestAction
            {
                Name = testActionNode.Name,
                Maximize = jsonValue["maximize"]?.GetBool() == true,
                Action = jsonValue["action"]?.GetString(),
                Element = jsonValue["element"]?.GetString() ?? "tempVar",
                Value = jsonValue["value"]?.GetString(),
                Position = jsonValue["position"]?.GetInt() ?? 0,
                Seconds = jsonValue["seconds"]?.GetInt() ?? 0,
                StartingElement = jsonValue["startingElement"]?.GetString() ?? "tempVar",
                EndingElement = jsonValue["endingElement"]?.GetString() ?? "tempVar",
                StartingPoint = GetPoint(jsonValue["startingPoint"]),
                EndingPoint = GetPoint(jsonValue["endingPoint"]),
                FileName = jsonValue["fileName"]?.GetString() ?? "Screenshot",
                VarName = jsonValue["var"]?.GetString() ?? "tempVar",
                Array = jsonValue["array"]?.GetString() ?? "tempArray",
                Message = jsonValue.GetString(),
                PropertyValue = jsonValue["value"]?.GetString() ?? string.Empty,
                Key = jsonValue["key"]?.GetString(),
                NumberOfTaps = jsonValue["numberOfTaps"]?.GetInt() ?? 1,
                Text = jsonValue["text"]?.GetString()?? jsonValue["stringFormat"]?.GetString() ?? string.Empty,
                Reason = jsonValue["reason"]?.GetString(),
                Keys = jsonValue.Select(key => key.GetString()).ToArray(),
                Parent = jsonValue["parent"]?.GetString(),
                ThrowException = jsonValue["throwError"]?.GetBool() != false
            };

            if (jsonValue["count"] != null)
            {
                testAction.Count = jsonValue["count"]["array"] != null
                    ? jsonValue["count"]["array"].GetString()
                    : jsonValue["count"]["value"].GetString();
            }

            var window = jsonValue["windowTitle"]?.GetString();

            if (!string.IsNullOrEmpty(window))
            {
                var tryResult = ConfigurationProperties.CommonWindows.TryGetValue
                (
                    window,
                    out var windowTitle
                );

                if (!tryResult)
                {
                    throw new FormatException($"No Saved Window with name {window} in Windows.config");
                }

                testAction.WindowTitle = windowTitle;
            }

            string by = jsonValue["by"]?.GetString() ?? jsonValue["property"]?.GetString();

            if (by == null)
            {
                testAction.By = FindBy.Name;
            }
            else if (Enum.TryParse(by, out FindBy byAsEnum))
            {
                testAction.By = byAsEnum;
            }
            else
            {
                Logger.Error
                (
                    $"Invalid property {by}. Please use one of the following properties"
                );
                Logger.Error(string.Join
                (
                    ", ",
                    Enum.GetNames(typeof(FindBy))
                ));
                return null;
            }

            if (Enum.TryParse<ComplexNode>(testActionNode.Name, out _))
            {
                testAction.ChildActions = GetTestActions(jsonValue["actions"]?.GetArray());
            }

            return testAction;
        }

        private static TestAction[] GetTestActions(JArray json)
        {
            if (json == null)
            {
                return null;
            }

            var actions = new List<TestAction>();

            foreach (var childObject in json)
            {
                JProperty childProperty = (JProperty) childObject.First;

                var tryResult = Enum.TryParse
                (
                    childProperty.Name,
                    out JValueType valueType
                );

                if (tryResult)
                {
                    actions.Add(new TestAction
                    {
                        Name = childProperty.Name,
                        Message = valueType switch
                        {
                            JValueType.Log => childProperty.Value.ToString(),
                            _ => null
                        }
                    });
                }
                else if (childProperty.Name == "TapKeys")
                {
                    actions.Add(new TestAction
                    {
                        Name = "TapKeys",
                        Keys = childProperty.Value.Select(key => key.GetString()).ToArray(),
                    });
                }
                else
                {
                    var propertyAsObject = childProperty.Value.GetObject();
                    if (propertyAsObject.ContainsKey("ifs"))
                    {
                        actions.AddRange
                        (
                            propertyAsObject["ifs"]
                                .GetObject()
                                .Properties()
                                .Select(GetTestAction)
                                .Where(action => action != null)
                        );

                        if (propertyAsObject.ContainsKey("else"))
                        {
                            var elseProperty = propertyAsObject.Property("else");
                            var action = GetTestAction(elseProperty);

                            if (action == null)
                            {
                                return null;
                            }

                            actions.Add(action);
                        }
                    }
                    else
                    {
                        var action = GetTestAction(childProperty);

                        if (action == null)
                        {
                            return null;
                        }

                        actions.Add(action);
                    }
                }
            }

            return actions.ToArray();
        }

        private static Point GetPoint(JToken point)
        {
            if (point == null)
            {
                return Point.Empty;
            }

            var values = point.GetString().Trim('(', ')').Split(',');
            return new Point
            (
                int.Parse(values[0]),
                int.Parse(values[1])
            );
        }

        private static string GetString(this JToken json)
        {
            return json.ToString();
        }

        private static int GetInt(this JToken json)
        {
            return (int) json;
        }

        private static bool GetBool(this JToken json)
        {
            return (bool) json;
        }

        private static JObject GetObject(this JToken json)
        {
            return (JObject) json;
        }

        private static JArray GetArray(this JToken json)
        {
            return (JArray) json;
        }
    }
}
