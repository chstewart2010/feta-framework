﻿using System.Collections.ObjectModel;
using System.Linq;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.WindowsDriverHelper;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;

namespace Tr.Feta.Core.DataModel
{
    /// <summary>
    /// Container for UI Element(s)
    /// </summary>
    internal class ElementContainer
    {
        private readonly ElementContainer _parentContainer;
        private readonly FindBy _by;
        private readonly string _value;
        private int _selectedElement;

        /// <summary>
        /// Constructor for UI Element container
        /// </summary>
        /// <param name="driver">Windows Driver to find the UI element</param>
        /// <param name="by">Method to find the UI Element</param>
        /// <param name="value">Value to test against</param>
        /// <param name="parentContainer">Parent element</param>
        public ElementContainer(WindowsDriver<AppiumWebElement> driver, FindBy by, string value, ElementContainer parentContainer)
        {
            _by = by;
            _value = value;
            _parentContainer = parentContainer;
            Elements = driver.FindUiElements(by, value, parentContainer?.SelectedElement);
        }

        /// <summary>
        /// Position of the current selected UI element, default is 0
        /// </summary>
        public int SelectedElementPosition
        {
            get => _selectedElement;
            set
            {
                if (value < 0)
                {
                    _selectedElement = 0;
                }
                else if (value >= Elements.Count)
                {
                    _selectedElement = Elements.Count - 1;
                }
                else
                {
                    _selectedElement = value;
                }
            }
        }

        /// <summary>
        /// Collection of UI elements found using the after initialization of the container
        /// </summary>
        public ReadOnlyCollection<AppiumWebElement> Elements { get; private set; }

        /// <summary>
        /// The selected singular UI element to act on
        /// </summary>
        public AppiumWebElement SelectedElement => Elements?[_selectedElement];

        /// <summary>
        /// Returns true if at least one UI element was found
        /// </summary>
        public bool ContainerNotEmpty => Elements?.Any() ?? false ;

        /// <summary>
        /// Resets the container if there's been a change in the WindowsDriver
        /// </summary>
        /// <param name="driver"></param>
        public void ResetContainer(WindowsDriver<AppiumWebElement> driver)
        {
            Elements = driver.FindUiElements(_by, _value, _parentContainer?.SelectedElement);
        }
    }
}
