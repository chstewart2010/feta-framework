﻿using System.IO;
using System.Linq;
using Tr.Feta.Common.Exceptions;
using Tr.Feta.Core.TestSession;

namespace Tr.Feta.Core.DataModel
{
    /// <summary>
    /// Provides support for creating <see cref="TestSuite"/> objects.
    /// </summary>
    internal static class TestSuiteImportFactory
    {
        /// <summary>
        /// Returns a test suite object based on the test suite file passed
        /// </summary>
        /// <param name="filePath">path to the test suite file</param>
        /// <param name="sessionName">The name of the Session</param>
        /// <param name="databaseId">The session id</param>
        /// <param name="product">The product being tests</param>
        internal static TestSuite GetTestSuite(
            string filePath,
            string sessionName,
            string databaseId,
            string product)
        {
            var testSuite = Path.GetExtension(filePath).ToLower() switch
            {
                ".json" => ModelBuilderFromJson.BuildTestSuite
                (
                    filePath,
                    product
                ),
                _ => throw new DataModelException($"Invalid file format on {filePath}")
            } ?? throw new DataModelException($"Invalid suite found at {filePath}");

            // ensure each test suite calls the LaunchTopLevel TestAction at some point during execution
            CheckForLaunchAction(testSuite);
            testSuite.SessionName = sessionName;
            testSuite.DatabaseId = databaseId;
            return testSuite;
        }

        private static void CheckForLaunchAction(TestSuite testSuite)
        {
            foreach (var testCase in testSuite.TestCases.Where(HasLaunchAction))
            {
                throw new DataModelException($"Missing \"LaunchTopLevel\" Test Action in {testCase} under {testSuite}");
            }

            foreach (var childSuite in testSuite.ChildSuites)
            {
                CheckForLaunchAction(childSuite);
            }
        }

        private static bool HasLaunchAction(TestCase testCase)
        {
            return testCase.Steps.Any(testStep => testStep.Actions.Any(IsLaunchAction));
        }

        private static bool IsLaunchAction(TestAction testAction)
        {
            return testAction.Name == "LaunchTopLevel" || testAction.ChildActions?.Any(IsLaunchAction) == true;
        }
    }
}
