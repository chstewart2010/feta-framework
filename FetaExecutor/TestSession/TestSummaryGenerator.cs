﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Tr.Feta.Common.TestResultsData;
using Tr.Feta.Common.Utilities;
using Tr.Feta.Core.Configurations;

namespace Tr.Feta.Core.TestSession
{
    internal static class TestSummaryGenerator
    {

        /// <summary>
        /// Generates the [summary] folder and sends a post session email
        /// </summary>
        /// <param name="results">Collection of test suite results from which ot generate the summary</param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="sessionName"></param>
        internal static void GeneratePostSessionSummaryAndSendEmail(
            ReadOnlyCollection<SuiteResult> results,
            DateTime startTime,
            DateTime endTime,
            string sessionName)
        {
            if (!results.Any())
            {
                Logger.Info("No Results to generate [summary] folder from.");
                return;
            }

            // save summary files
            var summaryData = new SessionResult
            (
                results,
                startTime,
                endTime,
                sessionName
            );
            var summaryDirectory = Path.Combine
            (
                ConfigurationProperties.TestOutputDirectory,
                sessionName,
                "[summary]"
            );
            var summaryJson = CreateTestSummary
            (
                summaryDirectory,
                results
            );
            var summaryLog = GenerateSummaryLog(summaryDirectory);

            // send email if EmailClientProperties is configured
            var emailClientProperties = ConfigurationProperties.GetEcpFromAppConfig();

            if (emailClientProperties == null)
            {
                return;
            }

            EmailAgent.SendPostRunEmail
            (
                emailClientProperties,
                $"UI Automation Test Results for Session: {sessionName}",
                summaryData,
                summaryJson,
                summaryLog
            );
        }

        private static string CreateTestSummary(
            string summaryDirectory,
            IEnumerable<SuiteResult> results)
        {
            var summaryJson = Path.Combine
            (
                summaryDirectory,
                "summary.json"
            );

            if (!Directory.Exists(summaryDirectory))
            {
                Directory.CreateDirectory(summaryDirectory);
            }

            using var jsonWriter = new StreamWriter(summaryJson);
            var json = JsonConvert.SerializeObject
            (
                results.Select(GenerateJsonSummary),
                Formatting.Indented
            );
            jsonWriter.WriteLine(json);
            return summaryJson;
        }

        /// <summary>
        /// Creates an anonymous object to build the json object from
        /// </summary>
        /// <param name="result">The result object to parse the summary from</param>
        internal static object GenerateJsonSummary(SuiteResult result)
        {
            if (result == null)
            {
                return null;
            }

            var testResults = new List<object>();

            foreach (var caseResult in result.CaseResults)
            {
                testResults.Add(new
                {
                    name = caseResult.Name,
                    testOutcome = caseResult.FinalTestState,
                    logFile = caseResult.TestCaseLogFile,
                    elapsedTime = caseResult.ElapsedTime,
                    elapsedTicks = caseResult.ElapsedTime.Ticks
                });
            }

            testResults.AddRange(result.ChildSuiteResults.Select(GenerateJsonSummary));
            return new
            {
                product = result.Product,
                runId = result.RunId,
                results = testResults
            };
        }

        /// <summary>
        /// Parses the Logger into individual test.logs and a summary.log
        /// </summary>
        /// <param name="summaryDirectory">Path to the [summary] folder</param>
        private static string GenerateSummaryLog(string summaryDirectory)
        {
            Logger.SwitchToGlobalLogger();
            var loggerFile = Logger.LogFile;
            Logger.CloseCurrentLogger();
            var summaryLogPath = Path.Combine
            (
                summaryDirectory,
                "summary.log"
            );

            try
            {
                File.Copy(loggerFile, summaryLogPath);
                return summaryLogPath;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }
    }
}
