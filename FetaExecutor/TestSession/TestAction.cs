﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using Tr.Feta.Common.Exceptions;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Utilities;
using Tr.Feta.Common.WindowsDriverHelper;
using Tr.Feta.Core.Configurations;
using Tr.Feta.Core.Events;
using Tr.Feta.Core.DataModel;

namespace Tr.Feta.Core.TestSession
{
    /// <summary>
    /// Container for each Test Action node
    /// </summary>
    internal class TestAction
    {
        private enum Interaction
        {
            Clear = 1,
            Click,
            DoubleClick,
            EnterCurrentDateTimeString,
            EnterText,
            EnterPassword,
            MoveToElement,
            RightClick
        }

        private AppiumWebElement _windowsElement;
        private string _actual;
        private Dictionary<string, ElementContainer> _containers;
        private Dictionary<string, int> _counts;
        private ElementContainer _container;
        private ElementContainer _parentContainer;
        private ReadOnlyCollection<AppiumWebElement> _uiElements;
        private WindowsDriver<AppiumWebElement> _driver;
        private int _count;

        #region Properties
        
        /// <summary>
        /// Test Action Name
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Window title to change the WindowsDriver
        /// </summary>
        public string WindowTitle { get; internal set; }

        /// <summary>
        /// Whether to maximize the Window
        /// </summary>
        public bool Maximize { get; internal set; }

        /// <summary>
        /// Applicable Test Action
        /// </summary>
        public string Action { get; internal set; }

        /// <summary>
        /// Windows element to interact with
        /// </summary>
        public string Element { get; internal set; }

        /// <summary>
        /// Cartesian point on primary screen
        /// </summary>
        public Point Point { get; internal set; }

        /// <summary>
        /// Position in Array
        /// </summary>
        public int Position { get; internal set; }

        /// <summary>
        /// Number of seconds to wait
        /// </summary>
        public int Seconds { get; internal set; }

        /// <summary>
        /// Starting Element to drag and drop
        /// </summary>
        public string StartingElement { get; internal set; }

        /// <summary>
        /// Ending Element to drag and drop
        /// </summary>
        public string EndingElement { get; internal set; }

        /// <summary>
        /// Starting point to drag and drop
        /// </summary>
        public Point StartingPoint { get; internal set; }

        /// <summary>
        /// Ending point to drag and drop
        /// </summary>
        public Point EndingPoint { get; internal set; }

        /// <summary>
        /// File name to save the screenshot
        /// </summary>
        public string FileName { get; internal set; }

        /// <summary>
        /// What to name the windows element found
        /// </summary>
        public string VarName { get; internal set; }

        /// <summary>
        /// Name of an array
        /// </summary>
        public string Array { get; internal set; }

        /// <summary>
        /// Message to log
        /// </summary>
        public string Message { get; internal set; }

        /// <summary>
        /// Name of Windows Element property
        /// </summary>
        public FindBy By { get; internal set; }

        /// <summary>
        /// Value of Windows Element property
        /// </summary>
        public string PropertyValue { get; internal set; }

        /// <summary>
        /// Key to tap
        /// </summary>
        public string Key { get; internal set; }

        /// <summary>
        /// Number of times to tap the key
        /// </summary>
        public int NumberOfTaps { get; internal set; }

        /// <summary>
        /// Text to send
        /// </summary>
        public string Text { get; internal set; }

        /// <summary>
        /// Reason to fail the test
        /// </summary>
        public string Reason { get; internal set; }

        /// <summary>
        /// Keys to tap
        /// </summary>
        public string[] Keys { get; internal set; }

        /// <summary>
        /// Number of elements in the Array
        /// </summary>
        public string Count { get; internal set; }

        /// <summary>
        /// Name of the parent element
        /// </summary>
        public string Parent { get; internal set; }

        /// <summary>
        /// Whether to throw an exception
        /// </summary>
        public bool ThrowException { get; internal set; }

        /// <summary>
        /// Collection of child TestActions
        /// </summary>
        public IEnumerable<TestAction> ChildActions { get; internal set; }

        /// <summary>
        /// Value node
        /// </summary>
        public string Value { get; internal set; }

        #endregion

        /// <summary>
        /// Total time for test action to execute
        /// </summary>
        public TimeSpan TotalExecutionTime { get; set; }

        internal string TestCaseOutputFolder { private get; set; }

        /// <summary>
        /// Occurs whenever the action does not finish before the test case time out threshold is reached
        /// </summary>
        internal event EventHandler<ExecutionTimeOutEventArgs> TimeOutReached;

        /// <summary>
        /// Occurs at the end of the test action execution
        /// </summary>
        internal event EventHandler<EventArgs> TestActionEnded;

        /// <summary>
        /// Performs the appropriate test action
        /// </summary>
        /// <param name="driver">Windows Driver</param>
        /// <param name="containers">Element containers</param>
        /// <param name="counts">Collection of array counts to be used for validation</param>
        /// <exception cref="InvalidTestActionException">Thrown when using an invalid Test Action</exception>
        public void Execute(WindowsDriver<AppiumWebElement> driver, Dictionary<string, ElementContainer> containers,
            Dictionary<string, int> counts)
        {
            try
            {
                var actionStart = DateTime.Now;
                var method = GetType().GetMethod
                (
                    Name,
                    BindingFlags.NonPublic | BindingFlags.Instance
                );

                if (method == null)
                {
                    throw new InvalidTestActionException($"Invalid Test Action: {Name}");
                }

                _driver = driver;
                _containers = containers;
                _counts = counts;

                if (Parent != null)
                {
                    _parentContainer = _containers[Parent];
                }

                _count = int.TryParse(Count ?? "", out int result)
                    ? result
                    : _counts.TryGetValue(Count ?? "", out result)
                        ? result
                        : 0;
                method.Invoke
                (
                    this,
                    null
                );
                TotalExecutionTime += (DateTime.Now - actionStart);

                if (TotalExecutionTime > ConfigurationProperties.TestCaseExecutionTimeOut)
                {
                    var args = new ExecutionTimeOutEventArgs
                    {
                        TotalExecutionTime = TotalExecutionTime,
                        TimeOutThreshold = ConfigurationProperties.TestCaseExecutionTimeOut
                    };

                    TimeOutReached?.Invoke
                    (
                        this,
                        args
                    );
                }
            }
            finally
            {
                TestActionEnded?.Invoke
                (
                    this,
                    EventArgs.Empty
                );
            }
        }

        /// <summary>
        /// Returns the Action's name
        /// </summary>
        public override string ToString() => Name;

        #region List of Test Actions

        private void AssertArrayCountEquals()
        {
            _uiElements = TryGetValue
            (
                Array,
                _containers
            ).Elements;

            if (int.TryParse(Count, out _count))
            {
                Assert.AreEqual
                (
                    _count,
                    _uiElements.Count
                );
            }
            else if (_counts.TryGetValue(Count, out _count))
            {
                Assert.AreEqual
                (
                    _count,
                    _uiElements.Count
                );
            }
            else
            {
                Assert.Fail($"Unknown variable {Count}");
            }

            Logger.Info($"Assertion Passed: {Array}'s has {_uiElements.Count} elements.");
        }

        private void AssertArrayCountGreaterThan()
        {
            _uiElements = TryGetValue
            (
                Array,
                _containers
            ).Elements;

            if (int.TryParse(Count, out _count))
            {
                Assert.Greater
                (
                    _uiElements.Count,
                    _count
                );
            }
            else if (_counts.TryGetValue(Count, out _count))
            {
                Assert.Greater
                (
                    _uiElements.Count,
                    _count
                );
            }
            else
            {
                Assert.Fail($"Unknown variable {Count}");
            }

            Logger.Info($"Assertion Passed: {Array}'s has more than {_count} elements with {_uiElements.Count} found.");
        }

        private void AssertArrayCountLessThan()
        {
            _uiElements = TryGetValue
            (
                Array,
                _containers
            ).Elements;

            if (int.TryParse(Count, out _count))
            {
                Assert.Less
                (
                    _uiElements.Count,
                    _count
                );
            }
            else if (_counts.TryGetValue(Count, out _count))
            {
                Assert.Less
                (
                    _uiElements.Count,
                    _count
                );
            }
            else
            {
                Assert.Fail($"Unknown variable {Count}");
            }

            Assert.Less
            (
                _uiElements.Count,
                _count
            );
            Logger.Info($"Assertion Passed: {Array}'s has fewer than {_count} elements with {_uiElements.Count} found.");
        }

        private void AssertPropertyEquals()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            _actual = _windowsElement.GetAttribute(By.ToString());
            Assert.NotNull
            (
                _actual,
                $"{Element}'s {By} attribute was null."
            );
            Assert.AreEqual
            (
                PropertyValue,
                _actual
            );
            Logger.Info($"Assertion Passed: {By}'s value is {PropertyValue}.");
        }

        private void AssertPropertyNotEqual()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            _actual = _windowsElement.GetAttribute(By.ToString());
            Assert.NotNull(_actual, $"{Element}'s {By} attribute was null.");
            Assert.AreNotEqual
            (
                PropertyValue,
                _actual
            );
            Logger.Info($"Assertion Passed: {By}'s value is not {PropertyValue}.");
        }

        private void AssertPropertyContains()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            _actual = _windowsElement.GetAttribute(By.ToString());
            Assert.NotNull(_actual, $"{Element}'s {By} attribute was null.");
            Assert.True
            (
                _actual.Contains(PropertyValue),
                $"{By}: \"{_actual}\"\nExpected: \"{PropertyValue}\""
            );
            Logger.Info($"Assertion Passed: {By}'s value contains {PropertyValue}.");
        }

        private void AssertPropertyNull()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            string propertyValue = _windowsElement.GetAttribute(By.ToString());
            Assert.Null(propertyValue);
            Logger.Info($"Assertion Passed: {By}'s value is null.");
        }

        private void AssertPropertyNotNull()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            string propertyValue = _windowsElement.GetAttribute(By.ToString());
            Assert.NotNull(propertyValue, $"{Element}'s {By} attribute was null.");
            Logger.Info($"Assertion Passed: {By}'s value is {propertyValue}.");
        }

        private void AssertElementExists()
        {
            _driver.AssertElementExists
            (
                By,
                PropertyValue,
                _parentContainer?.SelectedElement
            );
            Logger.Info($"Assertion Passed: UI Element found with {By} {PropertyValue}.");
        }

        private void AssertElementNotExist()
        {
            _driver.AssertElementNotExists
            (
                By,
                PropertyValue,
                _parentContainer?.SelectedElement
            );
            Logger.Info($"Assertion Passed: No UI Element found with {By} {PropertyValue}.");
        }

        private void AssertTextEquals()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            Assert.NotNull(_windowsElement.Text, $"No Text found in {Element}");
            Assert.AreEqual
            (
                Text,
                _windowsElement.Text
            );
            Logger.Info($"Assertion Passed: {Element}'s Text is {_windowsElement.Text}.");
        }

        private void AssertTextNotEqual()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            Assert.NotNull(_windowsElement.Text, $"No Text found in {Element}");
            Assert.AreNotEqual
            (
                Text,
                _windowsElement.Text
            );
            Logger.Info($"Assertion Passed: {Element}'s Text is {_windowsElement.Text}, and not {Text}.");
        }

        private void AssertTextContains()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            Assert.NotNull(_windowsElement.Text, $"No Text found in {Element}");
            Assert.IsTrue
            (
                _windowsElement.Text.Contains(Text),
                $"Text: \"{_windowsElement.Text}\"\nExpected: \"{Text}\""
            );
            Logger.Info($"Assertion Passed: {Element}'s Text is {_windowsElement.Text}, containing {Text}.");
        }

        private void AssertTextNull()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            Assert.Null(_windowsElement.Text);
            Logger.Info($"Assertion Passed: {Element} Text value is null.");
        }

        private void AssertTextNotNull()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            Assert.NotNull
            (
                _windowsElement.Text,
                $"No Text found in {Element}"
            );
            Logger.Info($"Assertion Passed: {Element}'s value is {_windowsElement.Text}.");
        }

        private void Clear()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            _windowsElement.ClearElement(ThrowException);
        }

        private void Click()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            _windowsElement.ClickElement(ThrowException);
        }

        private void DoubleClick()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            _driver.DoubleClick(_windowsElement, ThrowException);
        }

        private void RightClick()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            _driver.RightClick(_windowsElement, ThrowException);
        }

        private void EnterPassword()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            _windowsElement.EnterPassword(Text, ThrowException);
        }

        private void EnterText()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            _windowsElement.EnterText(Text, ThrowException);
        }

        private void EnterCurrentDateTimeString()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            _windowsElement.EnterText(DateTime.Now.ToString(Text), ThrowException);
        }

        private void DragAndDrop()
        {
            AppiumWebElement element1 = TryGetValue
            (
                StartingElement,
                _containers
            ).SelectedElement;
            AppiumWebElement element2 = TryGetValue
            (
                EndingElement,
                _containers
            ).SelectedElement;
            _driver.DragAndDrop
            (
                element1,
                element2,
                ThrowException
            );
        }

        private void DragAndDropStartingPoint()
        {
            _windowsElement = TryGetValue
            (
                EndingElement,
                _containers
            ).SelectedElement;
            _driver.DragAndDrop
            (
                _windowsElement,
                StartingPoint,
                false,
                ThrowException
            );
        }

        private void DragAndDropEndingPoint()
        {
            _windowsElement = TryGetValue
            (
                StartingElement,
                _containers
            ).SelectedElement;
            _driver.DragAndDrop
            (
                _windowsElement,
                EndingPoint,
                true,
                ThrowException
            );
        }

        private void DragAndDropTwoPoints()
        {
            _driver.DragAndDrop
            (
                StartingPoint,
                EndingPoint,
                ThrowException
            );
        }

        private void FindAndInteract()
        {
            _windowsElement = _driver.FindUiElement
            (
                By,
                PropertyValue,
                _parentContainer?.SelectedElement,
                ThrowException
            );
            PerformInteraction
            (
                _windowsElement,
                Action
            );
        }

        private void InteractAtPoint()
        {
            Enum.TryParse
            (
                Action,
                out Interaction action
            );
            Cursor.Position = new Point(0, 0);
            Actions actions = new Actions(_driver);
            Cursor.Position = Point;

            switch (action)
            {
                case Interaction.Click:
                    actions.Click();
                    break;
                case Interaction.DoubleClick:
                    actions.DoubleClick();
                    break;
                case Interaction.MoveToElement:
                    break;
                case Interaction.RightClick:
                    actions.ContextClick();
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"{Action}");
            }

            actions.Perform();
        }

        private void FindAndEnterText()
        {
            _windowsElement = _driver.FindUiElement
            (
                By,
                PropertyValue,
                _parentContainer?.SelectedElement,
                ThrowException
            );
            PerformInteraction
            (
                _windowsElement,
                Action,
                Text
            );
        }

        private void FindElement()
        {
            _containers[VarName] = new ElementContainer
            (
                _driver,
                By,
                PropertyValue,
                _parentContainer
            );
        }

        private void FindElements()
        {
            _containers[Array] = new ElementContainer
            (
                _driver,
                By,
                PropertyValue,
                _parentContainer
            );
            _counts[Array] = _containers[Array].Elements.Count;
        }

        private void GetFromArray()
        {
            _container = TryGetValue
            (
                Array,
                _containers
            );
            _container.SelectedElementPosition = Position;
            _containers[VarName] = _container;
        }

        private void GetFromArrayAndInteract()
        {
            _container = TryGetValue
            (
                Array,
                _containers
            );
            _container.SelectedElementPosition = Position;
            PerformInteraction(_container.SelectedElement, Action);
        }

        private void GetFromArrayAndEnterText()
        {
            _container = TryGetValue
            (
                Array,
                _containers
            );
            _container.SelectedElementPosition = Position;
            PerformInteraction(_container.SelectedElement, Action);
        }

        private void Log()
        {
            Logger.Info(Message);
        }

        private void MoveToElement()
        {
            _windowsElement = TryGetValue
            (
                Element,
                _containers
            ).SelectedElement;
            _driver.MoveToElement
            (
                _windowsElement,
                ThrowException
            );
        }

        private void TakeScreenshot()
        {
            _windowsElement = Element != "tempVar"
                ? TryGetValue(Element, _containers).SelectedElement
                : null;
            WindowsHelper.TakeScreenshot
            (
                TestCaseOutputFolder,
                FileName,
                _windowsElement
            );
        }

        private void TapKey()
        {
            for (int i = 0; i < NumberOfTaps; i++)
            {
                Logger.Debug($"{Key} tap {i}");
                _driver.TapKey(Key);
                Thread.Sleep(500);
            }
        }

        private void TapKeys()
        {
            _driver.TapKeys(Keys);
        }

        private void Type()
        {
            _driver.Type(Text);
        }

        private void Wait()
        {
            Thread.Sleep(TimeSpan.FromSeconds(Seconds));
        }

        private void FailTest()
        {
            throw new AssertionException(Reason);
        }

        #endregion

        private static T TryGetValue<T>(
            string name,
            IReadOnlyDictionary<string, T> dictionary)
        {
            if (dictionary.TryGetValue(name, out T result))
            {
                return result;
            }

            throw new NotFoundException($"No Array saved with name {name}.");
        }

        private void PerformInteraction(
            AppiumWebElement uiElement,
            string interaction,
            string value = null)
        {
            Enum.TryParse
            (
                interaction,
                out Interaction action
            );

            switch (action)
            {
                case Interaction.Clear:
                    uiElement.ClearElement(ThrowException);
                    break;
                case Interaction.Click:
                    uiElement.ClickElement(ThrowException);
                    break;
                case Interaction.DoubleClick:
                    _driver.DoubleClick
                    (
                        uiElement,
                        ThrowException
                    );
                    break;
                case Interaction.EnterText:
                    uiElement.EnterText
                    (
                        value,
                        ThrowException
                    );
                    break;
                case Interaction.EnterPassword:
                    uiElement.EnterPassword
                    (
                        value,
                        ThrowException
                    );
                    break;
                case Interaction.MoveToElement:
                    _driver.MoveToElement
                    (
                        uiElement,
                        ThrowException
                    );
                    break;
                case Interaction.RightClick:
                    _driver.RightClick
                    (
                        uiElement,
                        ThrowException
                    );
                    break;
                case Interaction.EnterCurrentDateTimeString:
                    uiElement.EnterText
                    (
                        DateTime.Now.ToString(value),
                        ThrowException
                    );
                    break;
                default:
                    throw new InvalidTestActionException($"Invalid interaction {interaction}");
            }
        }
    }
}
