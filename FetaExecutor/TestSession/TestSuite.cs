﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Exceptions;
using Tr.Feta.Common.TestResultsData;
using Tr.Feta.Common.Utilities;
using Tr.Feta.Core.Configurations;
using Tr.Feta.FetaReporter.Reporters;

namespace Tr.Feta.Core.TestSession
{
    /// <summary>
    /// Container for test suite node
    /// </summary>
    internal class TestSuite
    {
        private ulong? _runId;
        private string _databaseId;
        private IFetaReporter _reporter;

        /// <summary>
        /// Initializes a new <see cref="TestSuite"/> instance
        /// </summary>
        /// <param name="name">The test suite's name</param>
        /// <param name="ancestors">List of suite names ordered from root to closest parent</param>
        public TestSuite(
            string name,
            IEnumerable<string> ancestors)
        {
            Name = name;
            OutputSuitePath = ancestors == null
                ? name
                : Path.Combine(string.Join("\\", ancestors), name);
        }

        /// <summary>
        /// Suite Name
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Sets name of the test session
        /// </summary>
        internal string SessionName
        {
            set
            {
                foreach (var testCase in TestCases)
                {
                    testCase.SessionName = value;
                }

                foreach (var testSuite in ChildSuites)
                {
                    testSuite.SessionName = value;
                }
            }
        }

        internal string DatabaseId
        {
            set
            {
                _databaseId = value;
                foreach (var testSuite in ChildSuites)
                {
                    testSuite.DatabaseId = value;
                }

                foreach (var testCase in TestCases)
                {
                    testCase.DatabaseSessionId = value;
                }
            }
        }

        /// <summary>
        /// Name of the Milestone in TestRail
        /// </summary>
        public string Milestone { get; internal set; }

        /// <summary>
        /// Feature or section of the product that the cases relate to
        /// </summary>
        public Product Product { get; internal set; }

        /// <summary>
        /// Output suite path from the root suite to this suite in DataModel hierarchy. Returns null if there is no parent
        /// </summary>
        public string OutputSuitePath { get; }

        /// <summary>
        /// Collection of all, if any, child suites
        /// </summary>
        public List<TestSuite> ChildSuites { get; } = new List<TestSuite>();

        /// <summary>
        /// Collection of all test cases in the current suite
        /// </summary>
        internal List<TestCase> TestCases { get; } = new List<TestCase>();

        /// <summary>
        /// Whether this test suite is the top level suite
        /// </summary>
        public bool IsTopLevel => OutputSuitePath.Split('\\').Length == 1;

        /// <summary>
        /// THe final suite result from test suite execution
        /// </summary>
        internal SuiteResult SuiteResult { get; } = new SuiteResult();

        private bool IsInterrupted { get; set; }

        private List<CaseResult> TestResults { get; } = new List<CaseResult>();

        /// <summary>
        /// Executes all applicable tests 
        /// </summary>
        /// <param name="runId">Run id to record test results</param>
        public void Execute(ulong? runId)
        {
            if (!ArePropertiesValid(runId) || !(TestCases.Any() || ChildSuites.Any()))
            {
                return;
            }

            if (runId != null && _runId == null)
            {
                throw new DataModelException($"Invalid run id {runId}. ScriptLauncher will now terminate.");
            }

            ConfigurationProperties.ConfigureProductProperties(Product);
            if (ConfigurationProperties.ProductProperties == null)
            {
                throw new DataModelException($"Failed to configure {Product} properties");
            }

            Execute();
        }

        /// <summary>
        /// Report results to Test Management tool, if applicable
        /// </summary>
        public void Report()
        {
            if (_runId != null)
            {
                JObject json = JObject.FromObject(TestSummaryGenerator.GenerateJsonSummary(SuiteResult));
                _reporter.PostTestCaseResults
                (
                    _runId.Value,
                    json
                );
            }
        }

        /// <summary>
        /// Returns the Suite's name
        /// </summary>
        public override string ToString() => Name;

        private void Execute()
        {
            foreach (var testCase in TestCases.TakeWhile(testCase => !IsInterrupted))
            {
                testCase.TestCaseEnded += TestCase_OnTestCaseEnded;
                ExecuteTestCase(testCase);
                switch (testCase.TestState)
                {
                    case FinalTestState.Passed:
                        SuiteResult.SuccessfulRuns++;
                        break;
                    case FinalTestState.Failed:
                        SuiteResult.FailedRuns++;
                        break;
                    default:
                        SuiteResult.ErrorRuns++;
                        break;
                }
            }

            var childSuiteResults = new List<SuiteResult>();
            foreach (var testSuite in ChildSuites.TakeWhile(testSuite => !IsInterrupted))
            {
                testSuite.Execute();
                childSuiteResults.Add(testSuite.SuiteResult);
                if (testSuite.IsInterrupted)
                {
                    IsInterrupted = testSuite.IsInterrupted;
                }
            }

            SuiteResult.Name = Name;
            SuiteResult.Product = Product;
            SuiteResult.RunId = _runId;
            SuiteResult.CaseResults = TestResults;
            SuiteResult.ChildSuiteResults = childSuiteResults;
        }

        private void ExecuteTestCase(TestCase testCase)
        {
            testCase.Execute();
            if (testCase.TestState == FinalTestState.Aborted)
            {
                throw new AbortedTestCaseException($"{testCase}");
            }

            var testCaseUri = testCase.LogFile;
            var computerName = Environment.MachineName;
            if (!testCaseUri.StartsWith($@"\\{computerName}", StringComparison.CurrentCultureIgnoreCase))
            {
                var startIndex = testCaseUri.StartsWith(@"\\") ? 3 : 0;
                var path = testCaseUri.Remove
                (
                    0,
                    testCaseUri.IndexOf('\\', startIndex)
                );
                testCaseUri = $@"\\{computerName}{path}";
            }

            var result = new CaseResult
            {
                Name = testCase.Name,
                FinalTestState = testCase.TestState,
                TestCaseLogFile = testCase.LogFile,
                TestCaseLogFileUri = $"file:///{testCaseUri}",
                ElapsedTime = testCase.Elapsed
            };
            TestResults.Add(result);
        }

        private void TestCase_OnTestCaseEnded(
            object sender,
            EventArgs e)
        {
            switch (DatabaseManager.GetSessionStatus(_databaseId))
            {
                case TestSessionStatus.InterruptRequested:
                    if (!IsInterrupted)
                    {
                        Logger.Info("End test session execution due to after an interrupt request.");
                        IsInterrupted = true;
                        DatabaseManager.UpdateSessionStatus
                        (
                            _databaseId,
                            TestSessionStatus.Interrupted
                        );
                    }
                    break;
                case TestSessionStatus.Aborted:
                    if (!IsInterrupted)
                    {
                        Logger.Info("End test session execution due to after an abort request.");
                        IsInterrupted = true;
                    }
                    break;
            }
        }

        private bool ArePropertiesValid(ulong? runId)
        {
            if (ConfigurationProperties.TestReporterMode != default && IsTopLevel)
            {
                // we cannot use TestReporter without a milestone
                if (string.IsNullOrEmpty(Milestone))
                {
                   Logger.Error($"No MileStone found for {Name}");
                    return false;
                }

                try
                {
                    // get run id for the test reporter
                    _reporter = ReporterFactory.GetReporter
                    (
                        ConfigurationProperties.TestReporterMode,
                        Product.ToString()
                    );
                    JObject json = JObject.FromObject(ToObject());
                    json["childSuites"] ??= new JArray();
                    if (runId == null)
                    {
                        _runId = _reporter.CreateNewRun(json);
                    }
                    else if (_reporter.ValidateTestSuiteByRunId(json, runId.Value))
                    {
                        _runId = runId.Value;
                    }
                    else
                    {
                        _runId = null;
                    }

                    // return false if we failed to get a run id
                    if (_runId == null)
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                   Logger.Error($"{ex.GetType().Name}: {ex.Message}");
                    return false;
                }
            }

            return true;
        }

        private object ToObject()
        {
            var childSuites = new object[ChildSuites.Count];
            var testCases = new object[TestCases.Count];
            for (int i = 0; i < ChildSuites.Count; i++)
            {
                childSuites[i] = ChildSuites[i].ToObject();
            }
            for (int i = 0; i < TestCases.Count; i++)
            {
                var steps = new object[TestCases[i].Steps.Length];
                for (int j = 0; j < steps.Length; j++)
                {
                    steps[j] = new
                    {
                        description = TestCases[i].Steps[j].Description
                    };
                }

                testCases[i] = new
                {
                    name = TestCases[i].Name,
                    steps,
                    expectedResult = TestCases[i].ExpectedResult,
                    testAutomationType = TestAutomationType.UiAutomation.ToString()
                };
            }

            if (IsTopLevel)
            {
                return new
                {
                    name = Name,
                    milestone = Milestone,
                    product = Product,
                    childSuites
                };
            }

            return new
            {
                name = Name,
                milestone = Milestone,
                product = Product,
                childSuites,
                testCases
            };
        }
    }
}