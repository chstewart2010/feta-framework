﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Exceptions;
using Tr.Feta.Common.TestResultsData;
using Tr.Feta.Common.Utilities;
using Tr.Feta.Core.Configurations;
using Tr.Feta.Core.DataModel;

namespace Tr.Feta.Core.TestSession
{
    internal static class TestSessionManager
    {
        private static readonly TestSessionStatus[] StoppedStatuses =
        {
            TestSessionStatus.Interrupted,
            TestSessionStatus.Aborted
        };

        /// <summary>
        /// Execute test session from CoreOptions
        /// </summary>
        /// <param name="sessionName1">CoreOptions object created from passed arguments</param>
        /// <param name="suiteFiles">CoreOptions object created from passed arguments</param>
        /// <param name="runId">CoreOptions object created from passed arguments</param>
        internal static void ExecuteSessionFromOptions(string sessionName1, IEnumerable<string> suiteFiles, ulong? runId)
        {
            DatabaseManager.CleanupExpiredTestSessions(ConfigurationProperties.MaxDaysInDatabase);

            if (!SetUp())
            {
                return;
            }

            // generate session id and session name
            var startTimestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            var sessionId = $"Session_{startTimestamp}";
            Logger.Info($"SessionId: {sessionId}");
            DatabaseManager.InsertTestSession(sessionId);
            var sessionName = !string.IsNullOrEmpty(sessionName1)
                ? $"{sessionName1}_{startTimestamp}"
                : $"Completed_{sessionId}";

            // execute test suites
            var startTime = DateTime.Now;
            var results = ExecuteSuites(
                suiteFiles,
                runId,
                sessionId,
                sessionName,
                out var finished
            );
            var endTime = DateTime.Now;

            // If the Session state is not Interrupted or Aborted, set the status to Finished
            // TODO: Add functionality to continue a Test Session from the Interrupted state
            // TODO: Create TestCaseManager class in Database namespace
            if (finished)
            {
                DatabaseManager.UpdateSessionStatus
                (
                    sessionId,
                    TestSessionStatus.Finished
                );
                TestSummaryGenerator.GeneratePostSessionSummaryAndSendEmail
                (
                    results,
                    startTime,
                    endTime,
                    sessionName
                );
            }
        }

        private static ReadOnlyCollection<SuiteResult> ExecuteSuites(
            IEnumerable<string> suiteFiles,
            ulong? runId,
            string sessionId,
            string sessionName,
            out bool finished)
        {
            var results = new List<SuiteResult>();

            // assume that the test session has finished by default
            finished = true;

            foreach (var suiteFile in suiteFiles)
            {
                // Stop executing new suite files if TestSessionStatus is Interrupted or Aborted
                if (StoppedStatuses.Contains(DatabaseManager.GetSessionStatus(sessionId)))
                {
                    finished = false;
                    break;
                }

                if (string.IsNullOrWhiteSpace(suiteFile))
                {
                    continue;
                }

                var filePath = FileManager.FindFile
                (
                    Path.Combine(ConfigurationProperties.TestSuiteLibrary),
                    suiteFile.Trim(),
                    true
                );

                if (filePath == null)
                {
                    Logger.Warn
                    (
                        $"Could not find {suiteFile} in {ConfigurationProperties.TestSuiteLibrary}. Skipping..."
                    );
                    continue;
                }

                var product = Path.GetFileNameWithoutExtension(Path.GetDirectoryName(filePath));

                try
                {
                    var suiteResult = ExecuteSuite
                    (
                        runId,
                        filePath,
                        product,
                        sessionName,
                        sessionId
                    );

                    if (suiteResult != null)
                    {
                        results.Add(suiteResult);
                    }
                }
                catch (AbortedTestCaseException ex)
                {
                    // do not add abort test runs to results
                    // as we will not need to create a summary json or send an email
                    Logger.Error($"Suite was aborted at TestCase {ex.Message}.");
                }
                catch (Exception ex)
                {
                    Logger.Error($"{ex.GetType().Name}: {ex.Message}");
                    Logger.Error(ex.StackTrace);
                }
            }

            return results.AsReadOnly();
        }

        private static SuiteResult ExecuteSuite(
            ulong? runId,
            string filePath,
            string product,
            string sessionName,
            string sessionId)
        {
            var testSuite = TestSuiteImportFactory.GetTestSuite
            (
                filePath,
                sessionName,
                sessionId,
                product
            );

            // execute test suite and report results to reporter
            testSuite.Execute(runId);
            testSuite.Report();
            Logger.SwitchToGlobalLogger();
            SummarizeTestSuiteExecution(testSuite);

            return testSuite.SuiteResult;
        }

        private static void SummarizeTestSuiteExecution(TestSuite testSuite)
        {
            foreach (var testCase in testSuite.TestCases
                .Where(testCase => testCase.ExecutionStatus == TestExecutionStatus.Finished))
            {
                Logger.Info($"{testCase} - {testCase.TestState}");
            }

            foreach (var childSuite in testSuite.ChildSuites)
            {
                SummarizeTestSuiteExecution(childSuite);
            }
        }

        private static bool SetUp()
        {
            if (!ConfigurationProperties.ArePropertiesValid)
            {
                Logger.Error("App Settings are not properly configured.");
                return false;
            }

            return true;
        }
    }
}
