﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Utilities;
using Tr.Feta.Common.WindowsDriverHelper;
using Tr.Feta.Core.Configurations;
using Tr.Feta.Core.Events;

namespace Tr.Feta.Core.TestSession
{
    /// <summary>
    /// Container for Test Case
    /// </summary>
    internal class TestCase
    {
        private readonly string _databaseCaseId = $"TestCase_{DateTime.Now:yyyyMMddHHmmssfff}";
        private readonly string _suiteName;
        private string _topLevelClient;
        private WindowsDriver<AppiumWebElement> _driver;
        private Exception _caughtException;
        private DateTime _startTime;
        private string _inputFolder;
        private string _inputFile;
        private string _outputFolder;
        private string _parentFolder;

        /// <summary>
        /// Initializes a new <see cref="TestCase"/> instance
        /// </summary>
        /// <param name="suiteName">Test suite name, if applicable</param>
        public TestCase(string suiteName)
        {
            _suiteName = suiteName;
        }

        /// <summary>
        /// Final Test Outcome
        /// </summary>
        public FinalTestState TestState { get; private set; } = FinalTestState.Unknown;

        /// <summary>
        /// Time that it took execute the test
        /// </summary>
        public TimeSpan Elapsed { get; private set; } = TimeSpan.Zero;

        /// <summary>
        /// Text from log file
        /// </summary>
        public string LogFile { get; private set; }

        /// <summary>
        /// Test Case Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Name of the Test Session
        /// </summary>
        internal string SessionName { private get; set; }

        /// <summary>
        /// Collection of Test Steps
        /// </summary>
        internal TestStep[] Steps { get; set; }

        /// <summary>
        /// Occurs at the end of the test case execution
        /// </summary>
        internal event EventHandler<EventArgs> TestCaseEnded; 

        /// <summary>
        /// The expected result for the test case
        /// </summary>
        internal string ExpectedResult { get; set; }

        /// <summary>
        /// Path to the original test case file
        /// </summary>
        internal string FilePath { get; set; }

        /// <summary>
        /// Returns the test case's current state of execution
        /// </summary>
        internal TestExecutionStatus ExecutionStatus { get; private set; } = TestExecutionStatus.PreRun;

        /// <summary>
        /// The product being tested
        /// </summary>
        internal Product Product { private get; set; }

        /// <summary>
        /// Sets the DatabaseSessionId for the test case and each test step
        /// </summary>
        internal string DatabaseSessionId
        {
            set
            {
                foreach (var step in Steps)
                {
                    step.DatabaseId = value;
                }
            }
        }

        private string TestCaseFolder => Path.Combine
        (
            ConfigurationProperties.TestOutputDirectory,
            TestCaseLogName
        );

        private string TestCaseLogName => Path.Combine
        (
            SessionName,
            Product.ToString(),
            _suiteName,
            Name
        );

        /// <summary>
        /// Executes the test case
        /// </summary>
        public void Execute()
        {
            string currentTestStep = null;
            try
            {
                Setup();

                _startTime = DateTime.Now;
                int step = 1;

                //TODO: Insert new TestCaseRecord to database here
                foreach (var testStep in Steps)
                {
                    // subscribe to TestStep events
                    ExecutionStatus = TestExecutionStatus.Running;
                    testStep.TestCaseException += TestStep_TestCaseException;
                    testStep.LaunchTopLevel += TestStep_LaunchTopLevel;
                    testStep.TestCaseOutputFolder = _outputFolder;
                    currentTestStep = testStep.Description;
                    string testStepMessage = $"Test step {step}" + 
                                             (currentTestStep == null ? string.Empty : $": {currentTestStep}");
                    Logger.Info(testStepMessage);

                    // attempt to execute test step
                    testStep.AttemptTestStepExecute
                    (
                        _driver,
                        _topLevelClient
                    );
                    if (testStep.TestState == FinalTestState.Aborted)
                    {
                        TestState = FinalTestState.Aborted;
                        break;
                    }

                    string successMessage = "Success: " + (currentTestStep ?? $"Test step {step}");
                    Logger.Info(successMessage);
                    step++;  
                }

                ExecutionStatus = TestExecutionStatus.PostRun;
                if (TestState != FinalTestState.Aborted)
                {
                    TestState = FinalTestState.Passed;
                }
            }
            catch (Exception ex)
            {
                // add exception to Logger
                _caughtException ??= ex;
                TestState = ex is TimeoutException
                    ? FinalTestState.Error
                    : FinalTestState.Failed;
            }
            finally
            {
                // execute tear down regardless of what happened during test case execution
                TearDown(currentTestStep);
                TestCaseEnded?.Invoke
                (
                    this,
                    EventArgs.Empty
                );
                ExecutionStatus = TestExecutionStatus.Finished;
            }
        }

        /// <summary>
        /// Returns the Case's name
        /// </summary>
        public override string ToString() => Name;

        private void Setup()
        {
            // initialize logger for test class
            Logger.AddNewLogger
            (
                _databaseCaseId,
                TestCaseLogName
            );
            Logger.SwitchLogger(TestCaseLogName);
            Logger.Info($"Preparing for UI test automation on {Name}");

            // define input and output folders
            _inputFolder = Path.Combine
            (
                TestCaseFolder,
                "Input"
            );
            _inputFile = Path.Combine
            (
                _inputFolder,
                Path.GetFileName(FilePath)
            );
            _outputFolder = Path.Combine
            (
                TestCaseFolder,
                "Output"
            );
            _parentFolder = Path.GetDirectoryName(TestCaseFolder) ?? TestCaseFolder;

            if (!Directory.Exists(_parentFolder))
            {
                Directory.CreateDirectory(_parentFolder);
            }

            // create input and output folders
            Directory.CreateDirectory(TestCaseFolder);
            Directory.CreateDirectory(_inputFolder);
            Directory.CreateDirectory(_outputFolder);

            // In the current state of the framework, the FilePath property is always set.
            // This may change in the future
            if (!string.IsNullOrWhiteSpace(FilePath))
            {
                if (File.Exists(FilePath))
                {
                    File.Copy(FilePath, _inputFile);
                }
                else
                {
                    File.Create(_inputFile);
                }
            }
            else
            {
                File.Create(Path.Combine
                (
                    _inputFolder,
                    "missing file.txt"
                ));
            }
        }

        private void TearDown(string currentTestStep)
        {
            // report all caught assertions during test run
            if (_caughtException != null)
            {
                var fileName = string.IsNullOrEmpty(currentTestStep)
                    ? "Execution stopped"
                    : $"Error at {currentTestStep.Trim('.')}";
                WindowsHelper.TakeScreenshot
                (
                    _outputFolder,
                    fileName,
                    null
                );
                Logger.ReportException(_caughtException);
            }

            WindowsHelper.CloseWindowsDriver();

            // log each step outcome
            int stepNumber = 1;
            foreach (var step in Steps)
            {
                Logger.Info(
                    $"{step.Description ?? $"Step {stepNumber}"} : {step.TestState}");
                stepNumber++;
            }

            // log final test state
            Logger.Info($"Test status: {TestState}");
            Elapsed = _startTime == default
                ? TimeSpan.Zero
                : DateTime.Now - _startTime;
            Logger.Info($"Test time - {Elapsed:g}");
            var tempLogFile = Logger.LogFile;
            Logger.CloseCurrentLogger();

            // copy log file to output folder and mark the end of test case execution
            LogFile = Path.Combine
            (
                _outputFolder,
                "test.log"
            );
            try
            {
                File.Copy
                (
                    tempLogFile,
                    LogFile
                );
            }
            catch (Exception ex)
            {
                Logger.Error($"Failed to copy log file: {ex}");
            }
        }

        private void TestStep_TestCaseException(object sender, TestCaseExceptionEventArgs e)
        {
            var verbConjugation = e.TestStepAttempts == 1
                ? "attempt"
                : "attempts";
            ReportExecutionStatus();
            Logger.Error($"Stopping test step attempt after {e.TestStepAttempts} {verbConjugation} due to error");
            throw e.Exception;
        }

        private void ReportExecutionStatus()
        {
            Logger.Info($"Test Case {Name} is in {ExecutionStatus}");
        }

        private void TestStep_LaunchTopLevel(object sender, EventArgs e)
        {
            switch (Product)
            {
                case Product.Sample:
                    string hostName = ConfigurationProperties.ProductProperties.ServerName;
                    string userName = ConfigurationProperties.ProductProperties.ServerUsername;
                    string password = ConfigurationProperties.ProductProperties.ServerPassword;
                    string app = ConfigurationProperties.ProductProperties.LocalAppPath;
                    if (!File.Exists(app))
                    {
                        throw new FileNotFoundException($"File \"{app}\" not found");
                    }

                    //Close existing Sample Process
                    Process.Start
                    (
                        "taskkill.exe",
                        "/f /im Sample.exe"
                    )?.WaitForExit();

                    //Launch Sample
                    Process.Start(app);

                    //Sign in
                    _driver = WindowsHelper.GetWindowsDriver
                    (
                        "",
                        false,
                        false,
                        true
                    );
                    var usernameBox = _driver.FindUiElement
                    (
                        FindBy.AutomationId,
                        "UsernameTextBox_EmbeddableTextBox"
                    );
                    var passwordBox = _driver.FindUiElement
                    (
                        FindBy.AutomationId,
                        "PasswordTextBox_EmbeddableTextBox"
                    );
                    var loginBtn = _driver.FindUiElement
                    (
                        FindBy.AutomationId,
                        "LoginButton"
                    );
                    usernameBox.Clear();
                    usernameBox.SendKeys(userName);
                    passwordBox.SendKeys(password);
                    loginBtn.Click();
                    _driver.TapKey("enter");

                    if (Process.GetProcessesByName("Sample").Length != 1)
                    {
                        throw new NotFoundException("Sample process is not apparently running");
                    }

                    _topLevelClient = "Sample Product";
                    WindowsHelper.TopLevelClient = _topLevelClient;
                    WindowsHelper.ServerIpAddress = ConfigurationProperties.ProductProperties.ServerIpAddress;
                    Thread.Sleep(TimeSpan.FromSeconds(10));
                    break;
                default:
                    throw new ArgumentOutOfRangeException
                    (
                        nameof(Product),
                        Product,
                        $"Invalid Product in {_suiteName}: Only Sample is supported."
                    );
            }

            _driver = WindowsHelper.GetWindowsDriver
            (
                _topLevelClient,
                true
            );
        }
    }
}
