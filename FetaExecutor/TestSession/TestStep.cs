﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using ScreenRecorderLib;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Utilities;
using Tr.Feta.Common.WindowsDriverHelper;
using Tr.Feta.Core.Configurations;
using Tr.Feta.Core.DataModel;
using Tr.Feta.Core.Events;

namespace Tr.Feta.Core.TestSession
{
    /// <summary>
    /// Container for test step node
    /// </summary>
    internal class TestStep
    {
        private string _topLevelClient;
        private Dictionary<string, ElementContainer> _storedVars = new Dictionary<string, ElementContainer>();
        private readonly Dictionary<string, int> _storedArrayCounts = new Dictionary<string, int>();
        private WindowsDriver<AppiumWebElement> _driver;
        private string _currentWindow;
        private bool _performConditional;
        private TimeSpan _currentElapsedTime = TimeSpan.Zero;

        /// <summary>
        /// Test Step's description. Returns null if no description was added.
        /// </summary>
        public string Description { get; internal set; }

        /// <summary>
        /// Collection of test actions to perform during the <see cref="TestStep"/>
        /// </summary>
        internal TestAction[] Actions { get; set; }

        /// <summary>
        /// Current Test State for the test step
        /// </summary>
        public FinalTestState TestState { get; private set; } = FinalTestState.Unknown;

        /// <summary>
        /// Occurs when a test case end in an exception
        /// </summary>
        internal event EventHandler<TestCaseExceptionEventArgs> TestCaseException;

        /// <summary>
        /// Occurs when a test case wants to launch the top level client
        /// </summary>
        internal event EventHandler<EventArgs> LaunchTopLevel;

        /// <summary>
        /// DatabaseId for current test session
        /// </summary>
        internal string DatabaseId { private get; set; }

        internal string TestCaseOutputFolder { private get; set; }

        /// <summary>
        /// Executes test step
        /// </summary>
        /// <param name="driver">Window Driver to execute against</param>
        /// <param name="topLevelClient">Top Level Client as defined by the Test Case</param>
        public void AttemptTestStepExecute(
            WindowsDriver<AppiumWebElement> driver,
            string topLevelClient)
        {
            _driver = driver;
            _topLevelClient = topLevelClient;
            AttemptTestStepExecute(null);
            if (TestState != FinalTestState.Aborted)
            {
                TestState = FinalTestState.Passed;
            }
        }

        /// <summary>
        /// Returns the Step's Description
        /// </summary>
        public override string ToString() => Description;

        private void AttemptTestStepExecute(IEnumerable<TestAction> actions)
        {
            _currentWindow ??= _topLevelClient;

            foreach (var action in actions ?? Actions)
            {
                for (int attempt = 1; attempt <= ConfigurationProperties.NumberOfTestStepAttempts; attempt++)
                {
                    if (TestState == FinalTestState.Aborted)
                    {
                        return;
                    }

                    // update the WindowsDriver if it was previously disposed
                    if (WindowsHelper.CurrentDriver != null)
                    {
                        _driver ??= WindowsHelper.CurrentDriver;
                        _topLevelClient ??= WindowsHelper.CurrentDriver.Title;
                        _currentWindow ??= _topLevelClient;
                    }

                    try
                    {
                        // attempt test case execution
                        TestActionExecute(action);
                        Logger.Debug(_currentElapsedTime);
                        break;
                    }
                    catch (Exception ex)
                    {
                        var baseException = ex.GetBaseException();
                        TestCaseExceptionEventArgs args = null;

                        // create TestCaseExceptionEventArgs
                        if (attempt == ConfigurationProperties.NumberOfTestStepAttempts - 1 ||
                            baseException is AssertionException || baseException is FormatException)
                        {
                            args = new TestCaseExceptionEventArgs
                            {
                                TestStepAttempts = attempt,
                                Exception = new AssertionException(baseException.Message)
                            };
                        }

                        if (baseException is TimeoutException)
                        {
                            args = new TestCaseExceptionEventArgs
                            {
                                TestStepAttempts = attempt,
                                Exception = baseException
                            };
                        }

                        // raise TestCaseException 
                        if (args != null)
                        {
                            TestState = FinalTestState.Failed;
                            OnTestCaseException(args);
                        }

                        // wait 2 seconds and attempt test actions again
                        Logger.Warn($"Ran into an issue at {action}. Retrying after 2 seconds");
                        Thread.Sleep(2000);

                        if (baseException is NullReferenceException ||
                            baseException is WebDriverException)
                        {
                            ResetDriver();
                        }
                    }
                }
            }

            TestState = FinalTestState.Passed;
        }

        private void TestActionExecute(TestAction action)
        {
            if (Enum.TryParse<ComplexNode>(action.Name, out var node))
            {
                ComplexNodeExecute(node, action);

            }
            else
            {
                action.TimeOutReached += TestAction_TimeOutReached;
                action.TestActionEnded += TestAction_OnTestActionEnded;
                action.TotalExecutionTime = _currentElapsedTime;
                if (string.Equals(action.Name, "takescreenshot", StringComparison.CurrentCultureIgnoreCase))
                {
                    action.TestCaseOutputFolder = TestCaseOutputFolder;
                }

                action.Execute
                (
                    _driver,
                    _storedVars,
                    _storedArrayCounts
                );
                _currentElapsedTime = action.TotalExecutionTime;
            }
        }

        private void ComplexNodeExecute(
            ComplexNode node,
            TestAction action)
        {
            AppiumWebElement windowsElement;
            ReadOnlyCollection<AppiumWebElement> uiElements;
            ElementContainer parentContainer = null;
            if (action.Parent != null)
            {
                parentContainer = _storedVars[action.Parent];
            }

            int count = int.TryParse(action.Count ?? "", out int result)
                ? result
                : _storedArrayCounts.TryGetValue(action.Count ?? "", out result)
                    ? result
                    : 0;

            switch (node)
            {
                case ComplexNode.ForEach:
                    var container = TryGetValue
                    (
                        action.Array,
                        _storedVars
                    );
                    uiElements = container.Elements;
                    for (var selectedElement = 0; selectedElement < uiElements.Count; selectedElement++)
                    {
                        container.SelectedElementPosition = selectedElement;
                        _storedVars[action.VarName] = container;
                        AttemptTestStepExecute(action.ChildActions);
                    }

                    return;
                case ComplexNode.Record:
                    string filePath = FileManager.GenerateFilePath
                    (
                        TestCaseOutputFolder,
                        action.FileName ?? "Video",
                        "mp4",
                        true
                    );
                    Recorder recorder = PrepareRecording();
                    Logger.Info("Beginning recording");
                    recorder.Record(filePath);
                    AttemptTestStepExecute(action.ChildActions);
                    recorder.Stop();
                    recorder.Dispose();
                    break;
                case ComplexNode.ClearContainers:
                    _storedVars = new Dictionary<string, ElementContainer>();
                    break;
                case ComplexNode.IfArrayCountEquals:
                    uiElements = TryGetValue
                    (
                        action.Array,
                        _storedVars
                    ).Elements;
                    _performConditional = uiElements.Count == count;
                    if (_performConditional)
                    {
                        AttemptTestStepExecute(action.ChildActions);
                        _performConditional = false;
                    }
                    break;
                case ComplexNode.IfArrayCountLessThan:
                    uiElements = TryGetValue
                    (
                        action.Array,
                        _storedVars
                    ).Elements;
                    _performConditional = uiElements.Count < count;
                    if (_performConditional)
                    {
                        AttemptTestStepExecute(action.ChildActions);
                        _performConditional = false;
                    }
                    break;
                case ComplexNode.IfPropertyEquals:
                    windowsElement = TryGetValue
                    (
                        action.Element,
                        _storedVars
                    ).SelectedElement;
                    _performConditional = action.PropertyValue == windowsElement.GetAttribute(action.By.ToString());
                    if (_performConditional)
                    {
                        AttemptTestStepExecute(action.ChildActions);
                        _performConditional = false;
                    }
                    break;
                case ComplexNode.IfPropertyContains:
                    windowsElement = TryGetValue
                    (
                        action.Element,
                        _storedVars
                    ).SelectedElement;
                    _performConditional =
                        (windowsElement.GetAttribute(action.By.ToString())?.Contains(action.PropertyValue))
                        .GetValueOrDefault();
                    if (_performConditional)
                    {
                        AttemptTestStepExecute(action.ChildActions);
                        _performConditional = false;
                    }
                    break;
                case ComplexNode.IfPropertyNull:
                    windowsElement = TryGetValue
                    (
                        action.Element,
                        _storedVars
                    ).SelectedElement;
                    _performConditional = string.IsNullOrEmpty(windowsElement.GetAttribute(action.By.ToString()));
                    if (_performConditional)
                    {
                        AttemptTestStepExecute(action.ChildActions);
                        _performConditional = false;
                    }
                    break;
                case ComplexNode.IfElementExists:
                    _storedVars[action.VarName] = new ElementContainer
                    (
                        _driver,
                        action.By,
                        action.PropertyValue,
                        parentContainer
                    );
                    _performConditional = _storedVars[action.VarName].ContainerNotEmpty;
                    if (_performConditional)
                    {
                        AttemptTestStepExecute(action.ChildActions);
                        _performConditional = false;
                    }
                    break;
                case ComplexNode.IfTextEquals:
                    windowsElement = TryGetValue
                    (
                        action.Element,
                        _storedVars
                    ).SelectedElement;
                    _performConditional = action.Text == windowsElement.Text;
                    break;
                case ComplexNode.IfTextContains:
                    windowsElement = TryGetValue
                    (
                        action.Element,
                        _storedVars
                    ).SelectedElement;
                    _performConditional = (windowsElement.Text?.Contains(action.PropertyValue)).GetValueOrDefault();
                    if (_performConditional)
                    {
                        AttemptTestStepExecute(action.ChildActions);
                        _performConditional = false;
                    }
                    break;
                case ComplexNode.IfTextNull:
                    windowsElement = TryGetValue
                    (
                        action.Element,
                        _storedVars
                    ).SelectedElement;
                    _performConditional = string.IsNullOrEmpty(windowsElement.Text);
                    if (_performConditional)
                    {
                        AttemptTestStepExecute(action.ChildActions);
                        _performConditional = false;
                    }
                    break;
                case ComplexNode.Else:
                    if (!_performConditional)
                    {
                        AttemptTestStepExecute(action.ChildActions);
                        _performConditional = true;
                    }
                    break;
                case ComplexNode.Section:
                    AttemptTestStepExecute(action.ChildActions);
                    ClearContainers();
                    break;
                case ComplexNode.SwitchOnProperty:
                    windowsElement = TryGetValue
                    (
                        action.Element,
                        _storedVars
                    ).SelectedElement;
                    var propertyValue = windowsElement.GetAttribute(action.By.ToString());
                    Logger.Info($"{action.Element}'s {action.By} is {propertyValue}");
                    ExecuteSwitch
                    (
                        action.ChildActions,
                        propertyValue
                    );
                    return;
                case ComplexNode.SwitchOnArrayCount:
                    uiElements = TryGetValue
                    (
                        action.Array,
                        _storedVars
                    ).Elements;
                    count = uiElements.Count;
                    Logger.Info($"{action.Array} has {count} matching UI elements");
                    ExecuteSwitch
                    (
                        action.ChildActions,
                        count
                    );
                    return;
                case ComplexNode.SwitchOnText:
                    windowsElement = TryGetValue
                    (
                        action.Element,
                        _storedVars
                    ).SelectedElement;
                    var text = windowsElement.Text;
                    Logger.Info($"{action.Element}'s Text is {text}");
                    ExecuteSwitch
                    (
                        action.ChildActions,
                        text
                    );
                    return;
                case ComplexNode.SwitchToDesktop:
                    SwitchWindow
                    (
                        "",
                        false
                    );
                    break;
                case ComplexNode.SwitchToWindow:
                    SwitchWindow
                    (
                        action.WindowTitle,
                        action.Maximize
                    );
                    break;
                case ComplexNode.SwitchToTopLevelClient:
                    SwitchWindow
                    (
                        _topLevelClient,
                        true
                    );
                    break;
                case ComplexNode.LaunchTopLevel:
                    LaunchTopLevel?.Invoke
                    (
                        this,
                        EventArgs.Empty
                    );
                    _driver = WindowsHelper.CurrentDriver;
                    _topLevelClient = _driver.Title;
                    _currentWindow = _topLevelClient;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(node), node, null);
            }
        }

        private Recorder PrepareRecording()
        {
            RecorderOptions options;
            if (!string.IsNullOrEmpty(_currentWindow))
            {
                _driver = WindowsHelper.GetWindowsDriver
                (
                    "",
                    false,
                    false
                );
                var windowElement = _driver.FindElementByName(_currentWindow);
                Rectangle window = new Rectangle
                (
                    windowElement.Location,
                    windowElement.Size
                );
                DisplayOptions displayOptions = new DisplayOptions
                (
                    "DISPLAY1",
                    window.Left,
                    window.Top,
                    window.Right,
                    window.Bottom
                );
                options = new RecorderOptions
                {
                    RecorderMode = RecorderMode.Video,
                    IsLowLatencyEnabled = true,
                    IsMp4FastStartEnabled = false,
                    DisplayOptions = displayOptions
                };
                SwitchWindow
                (
                    _currentWindow,
                    false
                );
            }
            else
            {
                options = new RecorderOptions
                {
                    RecorderMode = RecorderMode.Video,
                    IsLowLatencyEnabled = true,
                    IsMp4FastStartEnabled = false,
                };
            }

            var recorder = Recorder.CreateRecorder(options);
            recorder.OnRecordingComplete += OnRecordingComplete;
            recorder.OnRecordingFailed += OnRecordingFailed;
            return recorder;
        }

        private static void OnRecordingComplete(
            object sender,
            RecordingCompleteEventArgs e)
        {
            Logger.Info("Recording saved");
        }

        private static void OnRecordingFailed(
            object sender,
            RecordingFailedEventArgs e)
        {
            Logger.Error($"Recording failed due to {e.Error}");
        }

        private void OnTestCaseException(TestCaseExceptionEventArgs e)
        {
            TestCaseException?.Invoke
            (
                this,
                e
            );
        }

        private void TestAction_TimeOutReached(
            object sender,
            ExecutionTimeOutEventArgs e)
        {
            throw new TimeoutException
            (
                $"Test Script exceeded execution time of {e.TimeOutThreshold.Minutes} minutes with a total of {e.TotalExecutionTime.TotalMinutes} minutes."
            );
        }

        private void TestAction_OnTestActionEnded(
            object sender,
            EventArgs e)
        {
            if (DatabaseManager.GetSessionStatus(DatabaseId) == TestSessionStatus.Aborted)
            {
                TestState = FinalTestState.Aborted;
            }
        }

        private void SwitchWindow(
            string windowTitle,
            bool maximize)
        {
            _driver = WindowsHelper.GetWindowsDriver
            (
                windowTitle,
                !string.IsNullOrEmpty(windowTitle),
                maximize
            );
            _currentWindow = windowTitle;
            ClearContainers();
        }

        private void ClearContainers()
        {
            _storedVars = new Dictionary<string, ElementContainer>();
        }

        private static T TryGetValue<T>(
            string name,
            Dictionary<string, T> dictionary)
        {
            if (dictionary.TryGetValue(name, out T result))
            {
                return result;
            }

            throw new NotFoundException($"No array saved with name {name}.");
        }

        private void ExecuteSwitch(
            IEnumerable<TestAction> testActions,
            object switchValue)
        {
            bool executeDefault = true;
            var caseNodes = testActions as TestAction[] ?? testActions.ToArray();

            foreach (var caseNode in caseNodes)
            {
                if (caseNode.Value == Convert.ToString(switchValue) ||
                    caseNode.Name == "CaseNull" &&
                    switchValue == null)
                {
                    executeDefault = false;

                    foreach (var childAction in caseNode.ChildActions)
                    {
                        TestActionExecute(childAction);
                    }
                    break;
                }
            }

            if (executeDefault)
            {
                var defaultNode = caseNodes.Last();
                var failReason = defaultNode.Reason;

                if (failReason != null)
                {
                    throw new AssertionException(failReason);
                }
            }
        }

        private void ResetDriver()
        {
            Logger.Warn
            (
                "It looks like there was a webdriver connection issue. Resetting the webdriver"
            );
            _driver = WindowsHelper.GetWindowsDriver
            (
                _currentWindow,
                false,
                false,
                true
            );

            foreach (var container in _storedVars.Values)
            {
                try
                {
                    container.ResetContainer(_driver);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }
    }
}
