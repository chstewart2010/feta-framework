﻿using System;

namespace Tr.Feta.Core.Events
{
    /// <summary>
    /// Provides TimeSpan data for TimeOutReached event
    /// </summary>
    internal class ExecutionTimeOutEventArgs : EventArgs
    {
        internal TimeSpan TotalExecutionTime { get; set; }
        internal TimeSpan TimeOutThreshold { get; set; }
    }
}
