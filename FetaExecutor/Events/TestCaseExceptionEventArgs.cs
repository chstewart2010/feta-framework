﻿using System;

namespace Tr.Feta.Core.Events
{
    /// <summary>
    /// Provides data for TestCaseException event
    /// </summary>
    internal class TestCaseExceptionEventArgs : EventArgs
    {
        internal int TestStepAttempts { get; set; }
        internal Exception Exception { get; set; }
    }
}
