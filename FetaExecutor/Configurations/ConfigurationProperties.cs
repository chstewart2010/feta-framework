﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Reflection;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Utilities;
using EmailClientProperties = Tr.Feta.Common.Utilities.EmailAgent.EmailClientProperties;

namespace Tr.Feta.Core.Configurations
{
    /// <summary>
    /// Struct that defines a collection of properties defined in the app.config
    /// </summary>
    internal static class ConfigurationProperties
    {
        private static readonly string TestLibraryDirectory;

        /// <summary>
        /// Constructor method for defining the ExecutorProperties from the app.config
        /// </summary>
        static ConfigurationProperties()
        {
            // set default properties
            var globalConfig = (NameValueCollection) ConfigurationManager.GetSection("global");
            NumberOfTestStepAttempts = int.TryParse(globalConfig["NumberOfTestStepAttempts"], out var testNum)
                ? testNum
                : 3;
            TestCaseExecutionTimeOut = uint.TryParse(globalConfig["TestCaseExecutionTimeOut"], out var minutes)
                ? TimeSpan.FromMinutes(minutes)
                : TimeSpan.FromMinutes(3);
            MaxDaysInDatabase = int.TryParse(globalConfig["MaxDaysInDatabase"], out int days)
                ? days
                : 3;

            // set path properties
            TestOutputDirectory = globalConfig["TestOutputDirectory"];

            if (string.IsNullOrEmpty(TestOutputDirectory))
            {
                Logger.Info("TestOutputDirectory directory is missing. Please set this property.");
                ArePropertiesValid = false;
            }

            TestLibraryDirectory = globalConfig["TestLibraryDirectory"];
            var executableBase = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? ".";

            if (string.IsNullOrEmpty(TestLibraryDirectory))
            {
                Logger.Info("UiLibraryDirectory configuration property is missing. Using default directory");
                TestLibraryDirectory = Path.Combine(executableBase, "..", "TestLibrary");
            }

            if (TestOutputDirectory.Length > 90)
            {
                Logger.Info("TestLibraryDirectory directory is too long. Ensure directory is fewer than 90 characters");
                ArePropertiesValid = false;
            }

            if (!(Directory.Exists(TestSuiteLibrary) ||
                  Directory.Exists(TestCaseLibrary)))
            {
                Logger.Info($"{TestLibraryDirectory} must contain both CaseLibrary and SuiteLibrary directories");
                ArePropertiesValid = false;
            }

            Logger.Info("Using default SchemaDirectory");
            SchemaDirectory = Path.Combine(executableBase, "Schema");

            foreach (SchemaType value in Enum.GetValues(typeof(SchemaType)))
            {
                var file = Path.GetFullPath(Path.Combine
                (
                    SchemaDirectory,
                    FileManager.GetSchemaFile(value)
                ));

                if (!File.Exists(file))
                {
                    Logger.Error($"Schema file {file} is missing in {SchemaDirectory}");
                    ArePropertiesValid = false;
                }
            }

            // perform final validation on global properties
            if (NumberOfTestStepAttempts < 1 ||
                NumberOfTestStepAttempts > 10)
            {
                Console.WriteLine($"NumberOfTestStepAttempts is currently {NumberOfTestStepAttempts}.");
                Console.WriteLine($"Please set this value between 1 and 10 attempts.");
                ArePropertiesValid = false;
            }

            if (TestCaseExecutionTimeOut.Minutes <= 0)
            {
                Console.WriteLine($"TestCaseExecutionTimeOut is currently {TestCaseExecutionTimeOut.Minutes}.");
                Console.WriteLine($"Please set this value to at least 1 minute.");
                ArePropertiesValid = false;
            }

            if (MaxDaysInDatabase <= 0)
            {
                Console.WriteLine($"MaxDaysInDatabase is currently {MaxDaysInDatabase}.");
                Console.WriteLine($"Please set this value to at least 1 day.");
                ArePropertiesValid = false;
            }

            try
            {
                if (!Directory.Exists(TestOutputDirectory))
                {
                    Directory.CreateDirectory(TestOutputDirectory);
                }
            }
            catch
            {
                Console.WriteLine($"{TestOutputDirectory} is not a valid directory.");
                ArePropertiesValid = false;
            }

            if (!ArePropertiesValid)
            {
                return;
            }

            // set CommonWindows property from Windows section
            var windowCollection = (NameValueCollection)ConfigurationManager.GetSection("Windows");
            var windowDictionary = new Dictionary<string, string>();

            for (int i = 0; i < windowCollection.Count; i++)
            {
                windowDictionary.Add
                (
                    windowCollection.GetKey(i),
                    windowCollection[i]
                );
            }

            CommonWindows = windowDictionary;
        }

        /// <summary>
        /// Maximum Days of test data in Database
        /// </summary>
        internal static int MaxDaysInDatabase { get; }

        /// <summary>
        /// Sample Properties
        /// </summary>
        internal static IProductProperties ProductProperties { get; private set; }

        /// <summary>
        /// Number of times to attempt a test step
        /// </summary>
        internal static int NumberOfTestStepAttempts { get; }

        /// <summary>
        /// Test case timeout
        /// </summary>
        internal static TimeSpan TestCaseExecutionTimeOut { get; }

        /// <summary>
        /// Path to the output folder
        /// </summary>
        internal static string TestOutputDirectory { get; }

        /// <summary>
        /// Path to Schema Directory
        /// </summary>
        internal static string SchemaDirectory { get; }

        /// <summary>
        /// Collection of common Window titles
        /// </summary>
        internal static IReadOnlyDictionary<string, string> CommonWindows { get; }

        /// <summary>
        /// The correct test management tool to report results
        /// </summary>
        internal static FetaReporterMode TestReporterMode { get; set; }

        /// <summary>
        /// Path to CaseLibrary
        /// </summary>
        internal static string TestCaseLibrary => Path.Combine
        (
            TestLibraryDirectory,
            "CaseLibrary"
        );

        /// <summary>
        /// Path to SuiteLibrary
        /// </summary>
        internal static string TestSuiteLibrary => Path.Combine
        (
            TestLibraryDirectory,
            "SuiteLibrary"
        );

        /// <summary>
        /// Returns true if the Properties object is correctly configured
        /// </summary>
        internal static bool ArePropertiesValid { get; } = true;

        /// <summary>
        /// Configures the appropriate product properties
        /// </summary>
        /// <param name="product">Support product</param>
        internal static void ConfigureProductProperties(Product product)
        {
            ProductProperties = ProductFactory.GetProductPropertiesFromConfig(product);
        }

        internal static EmailClientProperties GetEcpFromAppConfig()
        {
            try
            {
                var collection = (NameValueCollection)ConfigurationManager.GetSection("SmtpServerCredentials");
                return new EmailClientProperties
                (
                    ConfigurationHelper.GetProperty(collection, "Server"),
                    ConfigurationHelper.GetProperty(collection, "SenderUsername"),
                    ConfigurationHelper.GetProperty(collection, "SenderPassword"),
                    ConfigurationHelper.GetProperty(collection, "Recipients").Split(','),
                    int.Parse(ConfigurationHelper.GetProperty(collection, "Port")),
                    ConfigurationHelper.GetProperty(collection, "EnableSsl").ToLower() == "true"
                );
            }
            catch
            {
                Console.WriteLine("Failed to retrieve email client properties. No email will be sent");
                return null;
            }
        }
    }
}
