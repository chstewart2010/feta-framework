﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Utilities;

namespace Tr.Feta.Core.Configurations
{
    /// <summary>
    /// Provides support for creating <see cref="IProductProperties"/> objects
    /// </summary>
    public static class ProductFactory
    {
        /// <summary>
        /// Returns an <see cref="IProductProperties"/> object based on the product supplied
        /// </summary>
        /// <param name="product">The product that is being tested</param>
        public static IProductProperties GetProductPropertiesFromConfig(Product product)
        {
            IProductProperties properties;
            var productConfig = (NameValueCollection) ConfigurationManager.GetSection(product.ToString());

            try
            {
                properties = product switch
                {
                    Product.Sample => new SampleProperties(productConfig),
                    _ => throw new ArgumentOutOfRangeException(nameof(product), product, $"Invalid product: {product}")
                };
            }
            catch (Exception ex)
            {
                Logger.Error($"{ex.GetType().Name}: {ex.Message}");
                properties = null;
            }

            return properties;
        }
    }
}
