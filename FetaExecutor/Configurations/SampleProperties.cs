﻿using System.Collections.Specialized;
using Tr.Feta.Common.Utilities;

namespace Tr.Feta.Core.Configurations
{
    /// <summary>
    /// Container for Sample Product Properties
    /// </summary>
    internal class SampleProperties : IProductProperties
    {
        /// <summary>
        /// Constructor method for defining the ConfigurationProperties from the app.config
        /// </summary>
        /// <param name="collection">AppSettings in Configuration Manager</param>
        public SampleProperties(NameValueCollection collection)
        {
            ServerName = ConfigurationHelper.GetProperty(collection, "ServerName");
            ServerIpAddress = ConfigurationHelper.GetProperty(collection, "ServerIPAddress");
            ServerUsername = ConfigurationHelper.GetProperty(collection, "ServerUsername");
            ServerPassword = ConfigurationHelper.GetProperty(collection, "ServerPassword");
            LocalAppPath = ConfigurationHelper.GetProperty(collection, "LocalAppPath");
        }

        /// <inheritdoc />
        public string ServerName { get; }

        /// <inheritdoc />
        public string ServerIpAddress { get; }

        /// <inheritdoc />
        public string ServerUsername { get; }

        /// <inheritdoc />
        public string ServerPassword { get; }

        /// <inheritdoc />
        public string LocalAppPath { get; }
    }
}
