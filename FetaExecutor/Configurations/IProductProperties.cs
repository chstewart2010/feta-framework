﻿namespace Tr.Feta.Core.Configurations
{
    /// <summary>
    /// Interface for storing Product data
    /// </summary>
    public interface IProductProperties
    {
        /// <summary>
        /// Name of the Host Server
        /// </summary>
        public string ServerName { get; }

        /// <summary>
        /// Host Server IPAddress
        /// </summary>
        public string ServerIpAddress { get; }

        /// <summary>
        /// Host Server Username
        /// </summary>
        public string ServerUsername { get; }

        /// <summary>
        /// User's password
        /// </summary>
        public string ServerPassword { get; }

        /// <summary>
        /// Local Path to the Application
        /// </summary>
        public string LocalAppPath { get; }
    }
}
