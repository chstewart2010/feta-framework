﻿using System;
using System.Collections.Generic;
using CommandLine;
using Tr.Feta.Common.Enums;
using Tr.Feta.Common.Options;
using Tr.Feta.Common.Utilities;
using Tr.Feta.Core.Configurations;

namespace Tr.Feta.Core
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<CoreOptions>(args)
                .WithParsed(RunOptions)             // run FetaExecutor with valid options
                .WithNotParsed(HandleParseError);   // display example menu
        }

        private static void HandleParseError(IEnumerable<Error> errs)
        {
            DisplayExampleMenu();
        }

        private static void RunOptions(CoreOptions coreOptions)
        {
            if (!coreOptions.AreValid)
            {
                DisplayExampleMenu();
                return;
            }

            ConfigurationProperties.TestReporterMode = coreOptions.Report
                ? FetaReporterMode.Web
                : default;

            switch (coreOptions.Command)
            {
                case FrameworkCommand.Run:
                    TestSession.TestSessionManager.ExecuteSessionFromOptions(coreOptions.SessionName, coreOptions.SuiteFiles, coreOptions.RunId);
                    break;
                case FrameworkCommand.List:
                    var records = DatabaseManager.GetActiveTestSessions();
                    Console.WriteLine("Active Session(s):");
                    foreach (var record in records)
                    {
                        Console.WriteLine(record);
                    }
                    break;
                case FrameworkCommand.Abort:
                    foreach (var sessionId in coreOptions.SessionIds)
                    {
                        int rowsAffected = DatabaseManager.UpdateSessionStatus
                        (
                            sessionId,
                            TestSessionStatus.Aborted
                        );

                        var abortMessage = rowsAffected != 0
                            ? $"SessionId '{sessionId}' will abort execution immediately."
                            : $"SessionId '{sessionId}' is not in a state which can be aborted.";
                        Console.WriteLine(abortMessage);
                    }
                    break;
                case FrameworkCommand.Stop:
                    foreach (var sessionId in coreOptions.SessionIds)
                    {
                        int rowsAffected = DatabaseManager.UpdateSessionStatus
                        (
                            sessionId,
                            TestSessionStatus.InterruptRequested
                        );

                        var stopMessage = rowsAffected != 0
                            ? $"SessionId '{sessionId}' is queued to stop execution after current test."
                            : $"SessionId '{sessionId}' is not in a state which can be interrupted.";
                        Console.WriteLine(stopMessage);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException
                    (
                        nameof(coreOptions.Command),
                        coreOptions.Command,
                        "Invalid command"
                    );
            }
        }

        private static void DisplayExampleMenu()
        {
            Console.WriteLine("Example Usage:\n" +
                              "\tTr.Feta.Executor.exe --command Run --files s1.json [s2.json ...] [--name NewSession] [--mode Web] [--runId 53]\n\n" +
                              "\tTr.Feta.Executor.exe --command List\n\n" +
                              "\tTr.Feta.Executor.exe --command Abort --sessionIds sid1 [sid2 ...]\n\n" +
                              "\tTr.Feta.Executor.exe --command Stop --sessionIds sid1 [sid2 ...]\n\n" +
                              "FetaExecutor documentation can be found out Doc\\FetaExecutor");
        }
    }
}
